<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOffenceStatusLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('offence_status_logs', function (Blueprint $table) {
            $table->increments('id');
            $table->date('status_date');

            $table->integer('offence_status_id')->unsigned();
            $table->foreign('offence_status_id')->references('id')->on('offence_statuses');

            $table->bigInteger('offence_id')->unsigned();
            $table->foreign('offence_id')->references('id')->on('offences');


            $table->text('detail')->nullable();
            $table->string('document_copy')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('offence_status_logs');
    }
}
