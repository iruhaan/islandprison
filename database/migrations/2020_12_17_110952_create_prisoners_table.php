<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePrisonersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('prisoners', function (Blueprint $table) {
          $table->bigIncrements('id');
          $table->string('file_number');
          $table->string('full_name');
          $table->string('address')->nullable();
          $table->date('DOB')->nullable();
          $table->string('nick_name')->nullable();
          $table->string('id_passportNo')->nullable();
          $table->string('profile_path')->nullable();

          $table->integer('country_id')->unsigned();
          $table->foreign('country_id')->references('id')->on('countries');

          $table->integer('atoll_id')->nullable()->unsigned();
          $table->foreign('atoll_id')->references('id')->on('atolls');

          $table->integer('island_id')->nullable()->unsigned();
          $table->foreign('island_id')->references('id')->on('islands');



          $table->integer('security_level_id')->nullable()->unsigned();
          $table->foreign('security_level_id')->references('id')->on('security_levels');


          $table->integer('status_id')->nullable()->unsigned();
          $table->foreign('status_id')->references('id')->on('statuses');


          $table->integer('gender_id')->nullable()->unsigned();
          $table->foreign('gender_id')->references('id')->on('genders');

          $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('prisoners');
    }
}
