<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOffenceIncrementsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('offence_increments', function (Blueprint $table) {
            $table->increments('id');


            $table->integer('increment_type_id')->unsigned();
            $table->foreign('increment_type_id')->references('id')->on('increment_types');

            $table->bigInteger('offence_id')->unsigned();
            $table->foreign('offence_id')->references('id')->on('offences');

            $table->date('increment_date');
            $table->date('start_date')->nullable();
            $table->date('end_date')->nullable();
            $table->integer('days');
            $table->text('additional_detail')->nullable();
            $table->string('document_copy')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('offence_increments');
    }
}
