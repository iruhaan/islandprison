<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOffenceDecrementsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('offence_decrements', function (Blueprint $table) {
            $table->increments('id');


            $table->integer('decrement_type_id')->unsigned();
            $table->foreign('decrement_type_id')->references('id')->on('decrement_types');

            $table->bigInteger('offence_id')->unsigned();
            $table->foreign('offence_id')->references('id')->on('offences');

            $table->date('decrement_date');
            $table->date('start_date')->nullable();
            $table->date('end_date')->nullable();
            $table->integer('percentage')->nullable();
            $table->integer('remain_days')->nullable();
            $table->integer('days');
            $table->text('additional_detail')->nullable();
            $table->string('document_copy')->nullable();


            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('offence_decrements');
    }
}
