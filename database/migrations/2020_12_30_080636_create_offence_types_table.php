<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOffenceTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('offence_types', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('offence_cat_id')->unsigned();
            $table->foreign('offence_cat_id')->references('id')->on('offence_cats');

            $table->string('offence')->nullable();


            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('offence_types');
    }
}
