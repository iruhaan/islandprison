<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOffencesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('offences', function (Blueprint $table) {

            $table->bigIncrements('id');

            $table->bigInteger('prisoner_id')->unsigned();
            $table->foreign('prisoner_id')->references('id')->on('prisoners');

            $table->integer('offence_cat_id')->unsigned();
            $table->foreign('offence_cat_id')->references('id')->on('offence_cats');

            $table->integer('offence_type_id')->unsigned();
            $table->foreign('offence_type_id')->references('id')->on('offence_types');

            $table->integer('court_name_id')->unsigned();
            $table->foreign('court_name_id')->references('id')->on('court_names');

            $table->integer('offence_status_id')->nullable()->unsigned();
            $table->foreign('offence_status_id')->references('id')->on('offence_statuses');


            $table->date('sentenced_date')->nullable()->default(null);
            $table->date('sentenced_implement_date')->nullable()->default(null);
            $table->integer('period_year')->default(0);
            $table->integer('period_month')->default(0);
            $table->integer('period_day')->default(0);
            $table->integer('total_period_days')->nullable()->default(0);
            $table->integer('total_increment_days')->nullable()->default(0);
            $table->integer('total_decrement_days')->nullable()->default(0);
            $table->string('court_order_no')->nullable()->default(0);
            $table->string('court_order_copy')->nullable();
            $table->text('warrant_detail')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('offences');
    }
}
