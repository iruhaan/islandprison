<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//login

//Auth::routes();
Route::get('audits', 'AuditController@index');


Route::get('/','WelcomeController@index')->name('home');

Route::get('/login',  'AuthController@loginForm')->name('login');
Route::get('/register',  'AuthController@register')->name('register');
Route::post('/login',  'AuthController@login');
Route::post('/save-register',  'AuthController@create')->name('register');

Route::post('/logout',  'AuthController@logout')->name('logout');




//Information
Route::get('/information','InformationController@index')->name('information');

//Country
Route::get('/country','InformationController@country')->name('country');
Route::post('/save-country','InformationController@saveCountry')->name('save.country');
Route::get('/detele-country/{id}','InformationController@deleteCountry')->name('delete.country');

//Atoll
Route::get('/atoll','InformationController@atoll')->name('atoll');
Route::post('/save-atoll','InformationController@saveAtoll')->name('save.atoll');
Route::get('/detele-atoll/{id}','InformationController@deleteAtoll')->name('delete.atoll');

//island
Route::get('/island','InformationController@island')->name('island');
Route::post('/save-island','InformationController@saveIsland')->name('save.island');
Route::get('/detele-island/{id}','InformationController@deleteIsland')->name('delete.island');

//Blood Group
Route::get('/blood-group','InformationController@Blood_group')->name('blood_group');
Route::post('/save-blood_group','InformationController@saveBlood_group')->name('save.blood_group');
Route::get('/detele-blood_group/{id}','InformationController@deleteBlood_group')->name('delete.blood_group');

//Gang Names
Route::get('/gang_name','InformationController@gang_name')->name('gang_name');
Route::post('/save-gang_name','InformationController@savegang_name')->name('save.gang_name');
Route::get('/detele-gang_name/{id}','InformationController@deletegang_name')->name('delete.gang_name');

//Unit
Route::get('/unit','InformationController@unit')->name('unit');
Route::post('/save-unit','InformationController@sunit')->name('save.unit');
Route::get('/detele-unit/{id}','InformationController@dunit')->name('delete.unit');

//Cells
Route::get('/cell','InformationController@cell')->name('cell');
Route::post('/save-cell','InformationController@scell')->name('save.cell');
Route::get('/detele-cell/{id}','InformationController@dcell')->name('delete.cell');

//wings
Route::get('/wing','InformationController@wing')->name('wing');
Route::post('/save-wing','InformationController@swing')->name('save.wing');
Route::get('/detele-wing/{id}','InformationController@dwing')->name('delete.wing');

//blocks
Route::get('/block','InformationController@block')->name('block');
Route::post('/save-block','InformationController@sblock')->name('save.block');
Route::get('/detele-block/{id}','InformationController@dblock')->name('delete.block');

//bed numbers
Route::get('/bed_number','InformationController@bed_number')->name('bed_number');
Route::post('/save-bed_number','InformationController@sbed_number')->name('save.bed_number');
Route::get('/detele-bed_number/{id}','InformationController@dbed_number')->name('delete.bed_number');

//Prison Names
Route::get('/prison_name','InformationController@prison_name')->name('prison_name');
Route::post('/save-prison_name','InformationController@sprison_name')->name('save.prison_name');
Route::get('/detele-prison_name/{id}','InformationController@dprison_name')->name('delete.prison_name');


//Security Level Names
Route::get('/security_level_name','InformationController@security_level_name')->name('security_level_name');
Route::post('/save-security_level_name','InformationController@ssecurity_level_name')->name('save.security_level_name');
Route::get('/detele-security_level_name/{id}','InformationController@dsecurity_level_name')->name('delete.security_level_name');

//Prisoner Types
Route::get('/prisoner_type','InformationController@prisoner_type')->name('prisoner_type');
Route::post('/save-prisoner_type','InformationController@sprisoner_type')->name('save.prisoner_type');
Route::get('/detele-prisoner_type/{id}','InformationController@dprisoner_type')->name('delete.prisoner_type');

//Gender
Route::get('/gender','InformationController@gender')->name('gender');
Route::post('/save-gender','InformationController@sgender')->name('save.gender');
Route::get('/detele-gender/{id}','InformationController@dgender')->name('delete.gender');

//Prisoner Status Name
Route::get('/status','InformationController@status')->name('status');
Route::post('/save-status','InformationController@sstatus')->name('save.status');
Route::get('/detele-status/{id}','InformationController@estatus')->name('delete.status');

//Prisoner Temporary
Route::get('/temp_status','InformationController@temp_status')->name('temp_status');
Route::post('/save-temp_status','InformationController@stemp_status')->name('save.temp_status');
Route::get('/detele-temp_status/{id}','InformationController@dtemp_status')->name('delete.temp_status');

//Prisoner Sentence Type
Route::get('/sentence_type','InformationController@sentence_type')->name('sentence_type');
Route::post('/save-sentence_type','InformationController@ssentence_type')->name('save.sentence_type');
Route::get('/detele-sentence_type/{id}','InformationController@dsentence_type')->name('delete.sentence_type');

//Prisoner Offence Category
Route::get('/offence_cat','InformationController@offence_cat')->name('offence_cat');
Route::post('/save-offence_cat','InformationController@soffence_cat')->name('save.offence_cat');
Route::get('/detele-offence_cat/{id}','InformationController@doffence_cat')->name('delete.offence_cat');
Route::post('/update-offence_cat/{id}','InformationController@uoffence_cat')->name('update.offence_cat');

//Prisoner Offence Types
Route::get('/offence_type','InformationController@offence_type')->name('offence_type');
Route::post('/save-offence_type','InformationController@soffence_type')->name('save.offence_type');
Route::get('/detele-offence_type/{id}','InformationController@doffence_type')->name('delete.offence_type');
Route::post('/update-offence_type/{id}','InformationController@uoffence_type')->name('update.offence_type');

//Prisoner Court Name
Route::get('/court_name','InformationController@court_name')->name('court_name');
Route::post('/save-court_name','InformationController@scourt_name')->name('save.court_name');
Route::get('/detele-court_name/{id}','InformationController@dcourt_name')->name('delete.court_name');
Route::post('/update-court_name/{id}','InformationController@ucourt_name')->name('update.court_name');

//Prisoner Offence Decrement Types
Route::get('/decrement_type','InformationController@decrement_type')->name('decrement_type');
Route::post('/save-decrement_type','InformationController@sdecrement_type')->name('save.decrement_type');
Route::get('/detele-decrement_type/{id}','InformationController@ddecrement_type')->name('delete.decrement_type');
Route::post('/update-decrement_type/{id}','InformationController@udecrement_type')->name('update.decrement_type');

//Prisoner Offence Increment Types
Route::get('/increment_type','InformationController@increment_type')->name('increment_type');
Route::post('/save-increment_type','InformationController@sincrement_type')->name('save.increment_type');
Route::get('/detele-increment_type/{id}','InformationController@dincrement_type')->name('delete.increment_type');
Route::post('/update-increment_type/{id}','InformationController@uincrement_type')->name('update.increment_type');

//Prisoner Offence Drug Offence Name
Route::get('/drug_offence_name','InformationController@drug_offence_name')->name('drug_offence_name');
Route::post('/save-drug_offence_name','InformationController@sdrug_offence_name')->name('save.drug_offence_name');
Route::get('/detele-drug_offence_name/{id}','InformationController@ddrug_offence_name')->name('delete.drug_offence_name');
Route::post('/update-drug_offence_name/{id}','InformationController@udrug_offence_name')->name('update.drug_offence_name');

//Prisoner Offence Offence status
Route::get('/offence_status','InformationController@offence_status')->name('offence_status');
Route::post('/save-offence_status','InformationController@soffence_status')->name('save.offence_status');
Route::get('/detele-offence_status/{id}','InformationController@doffence_status')->name('delete.offence_status');
Route::post('/update-offence_status/{id}','InformationController@uoffence_status')->name('update.offence_status');

//Prisoner Offence Present
Route::get('/sentence_present','InformationController@sentence_present')->name('sentence_present');
Route::post('/save-sentence_present','InformationController@ssentence_present')->name('save.sentence_present');
Route::get('/detele-sentence_present/{id}','InformationController@dsentence_present')->name('delete.sentence_present');
Route::post('/update-sentence_present/{id}','InformationController@usentence_present')->name('update.sentence_present');

//Prisoner Offence Approval
Route::get('/approval','InformationController@approval')->name('approval');
Route::post('/save-approval','InformationController@sapproval')->name('save.approval');




//offender
Route::get('/offender','OffenderController@offender')->name('offender');

//add prisoner
Route::get('/add-prisoner','OffenderController@addPrisoner')->name('add.prisoner');
Route::get('/islandList/{id}','OffenderController@getIslandName');
Route::get('/offencelist/{id}','OffenderController@getOffenceName');

//prisoner
Route::post('/saveprisoner','OffenderController@saveprisoner')->name("saveprisoner");
Route::get('/prisoner-info/{id}','OffenderController@prisonerInfo')->name('prisoner.info');


//hukum
Route::get('/hukum/{id}','HukumController@hukum')->name('hukum');
Route::get('/add-hukum/{id}','HukumController@addHukum')->name('add.hukum');
Route::post('/save-hukum/{id}','HukumController@saveHukum')->name('save.hukum');
Route::get('/open_hukum_detail/{id}','HukumController@openHukum')->name('open.hukum');
Route::get('/add_offence_increment/{id}','HukumController@addOffenceIncrement')->name('add.offence.increment');
Route::post('/save-offence_increment/{id}','HukumController@saveIncrement')->name('save.offence.increment');
Route::get('/add_offence_decrement/{id}','HukumController@addOffenceDecrement')->name('add.offence.decrement');
Route::post('/save-offence_decrement/{id}','HukumController@saveDecrement')->name('save.offence.decrement');
Route::get('/print-hukum/{id}','HukumController@printHukum')->name('print.hukum');

Route::get('/view-offence_increment/{id}','HukumController@viewIncrement')->name('view.offence.increment');
Route::post('/update-offence_increment/{id}','HukumController@updateIncrement')->name('update.offence.increment');
Route::get('/delete-offence_increment/{id}','HukumController@deleteOffenceIncrement')->name('delete.offence.increment');
Route::get('/print-increment/{id}','HukumController@printIncrement')->name('print.increment');
Route::get('/print-decrement/{id}','HukumController@printDecrement')->name('print.decrement');

Route::get('/view-offence_decrement/{id}','HukumController@viewDecrement')->name('view.offence.decrement');
Route::post('/update-offence_decrement/{id}','HukumController@updateDecrement')->name('update.offence.decrement');
Route::get('/delete-offence_decrement/{id}','HukumController@deleteOffenceDecrement')->name('delete.offence.decrement');

Route::get('/update-offence-status/{id}','HukumController@editOffenceStatus')->name('edit.offence.status');
Route::post('/update-offence-status/{id}','HukumController@updateOffenceStatus')->name('update.offence.status');
Route::get('/delete-offence-status/{id}','HukumController@deleteOffenceStatus')->name('delete.offence.status');


//offender supervisor
Route::get('/offender-supervisor','OffenderSupervisorController@index')->name('offender.supervisor');
Route::get('/approve-offence-increment/{id}','OffenderSupervisorController@approveIncrement')->name('approve.offender.increment');
Route::post('/save-increment-approval/{id}','OffenderSupervisorController@saveIncrementApproval')->name('save.increment.approval');
Route::get('/approve-offence-decrement/{id}','OffenderSupervisorController@approveDecrement')->name('approve.offender.decrement');
Route::post('/save-decrement-approval/{id}','OffenderSupervisorController@saveDecrementApproval')->name('save.decrement.approval');
Route::get('/approve_offence_status/{id}','OffenderSupervisorController@offenceStatus')->name('approve.offence.status');
Route::post('/save-approve_offence_status/{id}','OffenderSupervisorController@saveOffenceStatus')->name('save.approve.offence.status');

Route::get('/add-offence-status/{id}','HukumController@addOffenceStatus')->name('add.offence.status');
Route::post('/save-offence-status/{id}','HukumController@saveOffenceStatus')->name('save.offence.status');














//
