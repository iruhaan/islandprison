<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class court_name extends Model
{
    protected $fillable = [
      'court_name'
    ];
}
