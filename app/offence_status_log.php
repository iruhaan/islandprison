<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class offence_status_log extends Model implements Auditable
{
  use \OwenIt\Auditing\Auditable;
    protected $fillable = [
      'status_date',
      'offence_status_id',
      'offence_id',
      'detail',
      'document_copy'
    ];

    public function offence(){
      return $this->belongsTo(offence::class)->withDefault();
    }
    public function offence_status(){
      return $this->belongsTo(offence_status::class);
    }
}
