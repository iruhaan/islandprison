<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class decrement_type extends Model
{
    protected $fillable = [
      'decrement_type'
    ];
}
