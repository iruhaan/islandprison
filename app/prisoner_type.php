<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class prisoner_type extends Model
{
    protected $fillable = [
      'prisoner_type_dhi',
      'prisoner_type_eng'
    ];
}
