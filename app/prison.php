<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class prison extends Model
{
    protected $fillable = [
      'prison_dhi',
      'prison_eng'
    ];
}
