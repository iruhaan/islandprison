<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class wing extends Model
{
  protected $fillable = [
    'wing'
  ];
}
