<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class sentence_type extends Model
{
    protected $fillable = [
      'sentence_type_dhi',
      'sentence_type_eng'
    ];
}
