<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class gang_name extends Model
{
    protected $fillable = [
      'gang_name'
    ];
}
