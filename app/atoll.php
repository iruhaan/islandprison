<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class atoll extends Model
{
  protected $fillable = [
    'atoll_name',
  ];

  public function island(){
    return $this->hasMany(island::class);
  }

}
