<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class offence_type extends Model
{
    protected $fillable = [
      'offence',
      'offence_cat_id'
    ];

    public function offence_cat(){
      return $this->belongsTo(offence_cat::class);
    }
}
