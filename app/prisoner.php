<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use carbon\carbon;

class prisoner extends Model
{
  protected $fillable = [
    'file_number',
    'full_name',
    'address',
    'DOB',
    'id_passportNo',
    'profile_path',
    'country_id',
    'atoll_id',
    'island_id',
    'security_level_id',
    'status_id',
    'nick_name',
    'gender_id'
  ];

  protected $appends = ['age'];


  public function getAgeAttribute()
  {
     return Carbon::parse($this->attributes['DOB'])->age;
  }

  public function country(){
    return $this->belongsTo('App\country');
  }

  public function atoll(){
    return $this->belongsTo(atoll::class)->withDefault();
  }

  public function island(){
    return $this->belongsTo(island::class)->withDefault();
  }

  public function status(){
    return $this->belongsTo('App\status')->withDefault();
  }

  public function security_level(){
    return $this->belongsTo('App\security_level')->withDefault();
  }

  public function gender(){
    return $this->belongsTo('App\gender');
  }

  public function offence(){
    return $this->hasMany(offence::class);
  }





}
