<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class temp_status extends Model
{
    protected $fillable = [
      'temp_status'
    ];
}
