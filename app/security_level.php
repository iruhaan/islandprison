<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class security_level extends Model
{
    protected $fillable = [
      'security_level'
    ];
}
