<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class offence_cat extends Model
{
    protected $fillable = [
      'offence_cat',
      'offence_level'
    ];

    public function offence_type(){
      return $this->hasMany(offence_type::class);
    }


}
