<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class bed_number extends Model
{
    protected $fillable = [
      'bed_no'
    ];
}
