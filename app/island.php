<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class island extends Model
{
  protected $fillable = [
    'island_name',
    'atoll_id'
  ];

  public function atoll(){
    return $this->belongsTo(atoll::class);
  }
}
