<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class drug_offence_name extends Model
{
    protected $fillable = [
      'drug_offence_name_dhi',
      'drug_offence_name_eng'
    ];
}
