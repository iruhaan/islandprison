<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\country;
use App\atoll;
use App\island;
use App\prisoner_type;
use App\prison;
use App\gender;
use App\gang_name;
use App\security_level;
use App\prisoner;
use App\blood_group;
use App\offence_cat;
use Validator;

class OffenderController extends Controller
{
    // public function __construct(){
    //   return $this->middleware('offender');
    // }

    public function offender(){
      $prisoners = prisoner::all();
      return view('offender.offender',compact('prisoners'));
    }

    public function getIslandName($id){
      $atolls = atoll::Find($id);
      return $atolls->island()->select('id','island_name')->get();
    }
    public function getOffenceName($id){
      $offence_cats = offence_cat::Find($id);
      return $offence_cats->offence_type()->select('id','offence')->get();
    }

    public function addPrisoner(){
      $countries = country::all();
      $atolls = atoll::all();
      $islands = island::all();
      $security_levels = security_level::all();
      $genders = gender::all();
      return view('offender.addPrisoner0',compact('countries',
      'atolls',
      'islands',
      'prisons',
      'security_levels',
      'genders'
    ));
    }


    public function saveprisoner(request $request){

      $validator = Validator::make($request->all(), [
          'file_number' => 'unique:prisoners|max:255',
          'id_passportNo' => 'unique:prisoners|max:255',
            ]);

            if ($validator->fails()) {
              return redirect()->back()
                          ->withErrors($validator)
                          ->withInput();
            }
      //save prisone information

      prisoner::create($request->all());
      return redirect(route('offender'));
    }

    public function prisonerInfo($id){
      $prisoner = prisoner::Find($id);
      return view('offender.prisonerInfo',compact('prisoner'));
    }
}
