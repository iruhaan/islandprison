<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\prisoner;
use App\sentence_type;
use App\offence_cat;
use App\offence_type;
use App\court_name;
use App\offence_status;
use App\sentence_present;
use App\offence_status_log;
use App\increment_type;
use App\offence_increment;
use App\decrement_type;
use App\offence_decrement;
use App\offence;
use \carbon\carbon;
use Auth;

class HukumController extends Controller
{
    public function hukum($id){
      $prisoner = prisoner::Find($id);
      return view('hukum.hukum',compact('prisoner'));
    }

    public function addHukum($id){
      $prisoner = prisoner::Find($id);
      $offence_cats = offence_cat::all();
      $offence_types = offence_type::all();
      $court_names = court_name::all();
      $offence_statuses = offence_status::all();
      return view('hukum.add_hukum',compact(
        'prisoner',
        'offence_types',
        'offence_cats',
        'offence_statuses',
        'court_names'
      ));
    }

    public function printHukum($id){
        $prisoner = prisoner::Find($id);
        return view('hukum.printHukum',compact('prisoner'));
    }
















    public function saveHukum($id, request $request){
      $offence = offence::Find(11);
      //get total offence level
      $level = 0;

        offence::where('prisoner_id', $offence->prisoner_id)->each(function($p, $k) use (&$level) {
            $level += $p->offence_cat()->sum('offence_level');
        });

        $security_code = 0;

      // get prisoners Age
      $prisoner_age = prisoner::Find($id)->age;
      //High Security
      if ($level >= 3 && $level <= 8 && $prisoner_age <= 50 || $level >= 9 && $level <= 10 && $prisoner_age <= 55) {
        $security_code = 4;
      }
      //Medium Security
      if ($level == 2 && $prisoner_age <= 55 || $level >= 5 && $level <= 3 && $prisoner_age <= 55 && $prisoner_age >= 50 || $level >= 10 && $level <= 9 && $prisoner_age <= 70) {
        $security_code = 3;
      }
      //Low Security
      if($level == 1 || $level >=2 && $level <=5 && $prisoner_age >= 55 && $prisoner_age <= 50 || $level >= 8 && $level <= 6 && $prisoner_age >= 50 && $prisoner_age <= 60 || $level >=9 && $prisoner_age >= 55
    && $prisoner_age <= 70){
        $security_code = 2;
      }

      dd($security_code);






      if ($request->sentenced_implement_date == NULL) {
        $implDate = NULL;
      }else{
        $implDate = carbon::createFromFormat('Y-m-d',$request->sentenced_implement_date);
      }


      if($request->hasFile('court_order_copy'))
          {
          $gaziyya_copy = $request->court_order_copy;
          $extension = $gaziyya_copy->getClientOriginalExtension();
          $gaziyya_fileName = time().'.'.$extension;
          $request->court_order_copy->move(storage_path('app/public/gaziyya_chit'), $gaziyya_fileName);
      }
      else
      {
        $gaziyya_fileName = "no_file.png";
        //$fileName = "no_file.png";
      }

      $offence = offence::create([
        'prisoner_id'=> $request->prisoner_id,
        'offence_cat_id'=> $request->offence_cat_id,
        'offence_type_id'=> $request->offence_type_id,
        'court_name_id'=> $request->court_name_id,
        'offence_status_id'=> $request->offence_status_id,
        'sentenced_date'=> carbon::createFromFormat('Y-m-d',$request->sentenced_date),
        'sentenced_implement_date' => $implDate,
        'period_year'=> $request->period_year,
        'period_month'=> $request->period_month,
        'period_day'=> $request->period_day,
        'total_period_days'=> $request->total_period_days,
        'total_increment_days'=> 0,
        'court_order_copy'=> $gaziyya_fileName,
        'total_decrement_days' => 0,
        'court_order_no'=> $request->court_order_no,
        'prisoner_id'=> $id,
        'warrant_detail' => $request->warrant_detail
      ]);

      if($implDate == NULL){
        $status_date = carbon::createFromFormat('Y-m-d',$request->sentenced_date);
      }
      else{
        $status_date = $implDate;
      }


        offence_status_log::create([
          'status_date' => $status_date,
          'offence_status_id'=> $request->offence_status_id,
          'offence_id'=> $offence->id,
          'detail' => null,
          'document_copy' => "no_file.png"
        ]);



      //get total offence level
      $level = 0;

        offence::where('prisoner_id', $offence->prisoner_id)->each(function($p, $k) use (&$level) {
            $level += $p->offence_cat()->sum('offence_level');
        });

      // get prisoners Age
      $prisoner_age = prisoner::Find($id)->age;

      //Calculate Security Level

      //High Security
      if ($level >= 3 && $level <= 8 && $prisoner_age <= 50 || $level >= 9 && $level <= 10 && $prisoner_age <= 55) {
        $security_code = 4;
      }
      //Medium Security
      else if ($level == 2 && $prisoner_age <= 55 || $level >= 5 && $level <= 3 && $prisoner_age <= 55 && $prisoner_age >= 50 || $level >= 10 && $level <= 9 && $prisoner_age <= 70) {
        $security_code = 3;
      }
      //Low Security
      else if($level == 1 || $level >=2 && $level <=5 && $prisoner_age >= 55 && $prisoner_age <= 50 || $level >= 8 && $level <= 6 && $prisoner_age >= 50 && $prisoner_age <= 60 || $level >=9 && $prisoner_age >= 55
    && $prisoner_age <= 70){
        $security_code = 2;
      }

      return redirect(route('hukum',['id' => $id]));
      //return redirect('/hukum/'.($id));
    }


    public function openHukum($id){

      $hukum = offence::Find($id);

      $inc = $hukum->total_increment_days;
      $dec = $hukum->total_decrement_days;
      $balance = ($hukum->total_period_days + $inc) - $dec;

      if($hukum->sentenced_implement_date != NULL){
      $expire_date = carbon::createFromFormat('Y-m-d',$hukum->sentenced_implement_date)->addDays($balance);
    }else{
      $expire_date = carbon::now();
    }



      return view('hukum.open_hukum',compact('hukum','expire_date','balance'));
    }




    public function addOffenceIncrement($id){
      $increment_types = increment_type::all();
      return view('hukum.add_offence_increment', compact('id','increment_types'));
    }

    public function deleteOffenceIncrement($id){

      $offence_increment = offence_increment::Find($id);
      if ($offence_increment->approval_status_id != 2) {
        dd('record is approved cant delete');
      }
      else{
        $id = $offence_increment->offence_id;
        $offence_increment->delete();
        return redirect(route('open.hukum',['id' => $id]));
      }

    }
    public function deleteOffenceDecrement($id){
      $offence_decrement = offence_decrement::Find($id);
      if ($offence_decrement->approval_status_id != 2) {
          dd('record is approved cant delete');
      }
      else {
          $id = $offence_decrement->offence_id;
          $offence_decrement->delete();
          return redirect(route('open.hukum',['id' => $id]));
      }
    }

    public function saveIncrement($id, request $request){

      if($request->hasFile('document_copy'))
          {

          $extension = $request->document_copy->getClientOriginalExtension();
          $fileName = time().'.'.$extension;
          $request->document_copy->move(storage_path('app/public/offence_increment'), $fileName);
          }
          else
          {
            $fileName = "no_file.png";
          }

          if ($request->start_date == null) {
            $start = null;
          }
          else{
            $start = carbon::createFromFormat('Y-m-d',$request->start_date);
          }
          if ($request->end_date == null) {
            $end = null;
          }
          else{
            $end = carbon::createFromFormat('Y-m-d',$request->end_date);
          }

      offence_increment::create([
        'approval_status_id' => 2,
        'increment_type_id' => $request->increment_type_id ,
        'offence_id' => $id,
        'user_id' => Auth::user()->id,
        'increment_date' => carbon::createFromFormat('Y-m-d',$request->increment_date),
        'start_date' => $start,
        'end_date' => $end,
        'days' => $request->days,
        'additional_detail' => $request->additional_detail,
        'document_copy' => $fileName
      ]);


      //update increment to offence
      $offence = offence::Find($id);
      $total_increment_days = offence_increment::where('offence_id',$offence->id)->sum('days');
      $offence->update(['total_increment_days' => $total_increment_days]);

      return redirect(route('open.hukum',['id' => $id]));
    }





    //decrement
    public function addOffenceDecrement($id){
      $decrement_types = decrement_type::all();
      $offence = offence::Find($id);

      $inc = $offence->total_increment_days;
      $dec = $offence->total_decrement_days;
      $balance = ($offence->total_period_days + $inc) - $dec;

      if($offence->sentenced_implement_date != NULL){
      $expire_date = carbon::createFromFormat('Y-m-d',$offence->sentenced_implement_date)->addDays($balance);
    }else{
      $expire_date = carbon::now();
    }




      return view('hukum.add_offence_decrement', compact('id','decrement_types','expire_date'));
    }

    public function saveDecrement($id, request $request){

      if($request->hasFile('document_copy'))
          {

          $extension = $request->document_copy->getClientOriginalExtension();
          $fileName = time().'.'.$extension;
          $request->document_copy->move(storage_path('app/public/offence_decrement'), $fileName);
          }
          else
          {
            $fileName = "no_file.png";
          }

          if ($request->start_date == null) {
             $start = null;
          }
          else{
            $start =  carbon::createFromFormat('Y-m-d',$request->start_date);
          }
          if ($request->end_date == null) {
             $end = null;
          }
          else{
            $end =  carbon::createFromFormat('Y-m-d',$request->end_date);
          }

      offence_decrement::create([
        'approval_status_id' => 2,
        'decrement_type_id' => $request->decrement_type_id ,
        'offence_id' => $id,
        'user_id' => Auth::user()->id,
        'decrement_date' => carbon::createFromFormat('Y-m-d',$request->decrement_date),
        'start_date' => $start,
        'end_date' => $end,
        'remain_days' => $request->remain_days,
        'percentage' => $request->percentage,
        'days' => $request->days,
        'additional_detail' => $request->additional_detail,
        'document_copy' => $fileName
      ]);

      // update decrement days to offence
      $offence = offence::Find($id);
      $total_decrement_days = offence_decrement::where('offence_id',$offence->id)->sum('days');
      $offence->update(['total_decrement_days' => $total_decrement_days]);


      return redirect(route('open.hukum',['id' => $id]));
    }

    public function viewIncrement($id){

      $offence_increment = offence_increment::Find($id);

      if($offence_increment->approval_status_id == 2){
        $hukum = offence::Find($offence_increment->offence_id);
        $increment_types = increment_type::all();
        return view('hukum.view_offence_increment',compact('offence_increment','hukum','increment_types'));
      }
      elseif ($offence_increment->approval_status_id == 1) {
        return redirect()->back()->withErrors(['ތިޔަ ރެކޯޑް ވަނީ ޤަބޫލު ނުކޮށްފައި.']);
      }


    }

    public function viewDecrement($id){

      $offence_decrement = offence_decrement::Find($id);

      $offence = offence::Find($offence_decrement->offence_id);

      $inc = $offence->total_increment_day;
      $dec = $offence->total_increment_day;
      $balance = ($offence->total_period_days + $inc) - $dec;

      if($offence->sentenced_implement_date != NULL){
      $expire_date = carbon::createFromFormat('Y-m-d',$offence->sentenced_implement_date)->addDays($balance);
    }else{
      $expire_date = carbon::now();
    }

      if($offence_decrement->approval_status_id == 2){
        $hukum = offence::Find($offence_decrement->offence_id);
        $decrement_types = decrement_type::all();
        return view('hukum.view_offence_decrement',compact('offence_decrement','hukum','decrement_types','expire_date'));
      }
      elseif ($offence_decrement->approval_status_id == 1) {
        return redirect()->back()->withErrors(['ތިޔަ ރެކޯޑް ވަނީ ޤަބޫލު ނުކޮށްފައި.']);
      }


    }



    public function updateDecrement($id, request $request){
      $decrement = offence_decrement::Find($id);

      if($request->hasFile('document_copy'))
          {
          $extension = $request->document_copy->getClientOriginalExtension();
          $fileName = time().'.'.$extension;
          $request->document_copy->move(storage_path('app/public/offence_decrement'), $fileName);
          }
          else
          {
            $fileName = $decrement->document_copy;
          }
          if ($request->start_date == null) {
             $start = null;
          }
          else{
            $start =  carbon::createFromFormat('Y-m-d',$request->start_date);
          }
          if ($request->end_date == null) {
             $end = null;
          }
          else{
            $end =  carbon::createFromFormat('Y-m-d',$request->end_date);
          }

      $decrement->update([
        'approval_status_id' => 2,
        'decrement_type_id' => $request->decrement_type_id ,
        'user_id' => Auth::user()->id,
        'decrement_date' => carbon::createFromFormat('Y-m-d',$request->decrement_date),
        'start_date' => $start,
        'end_date' => $end,
        'remain_days' => $request->remain_days,
        'percentage' => $request->percentage,
        'days' => $request->days,
        'additional_detail' => $request->additional_detail,
        'document_copy' => $fileName
      ]);
      return redirect(route('open.hukum',['id' => $decrement->offence_id]));
    }



    public function updateIncrement($id, request $request){
      $decrement_set = offence_increment::Find($id);
      if($request->hasFile('document_copy'))
          {
          $extension = $request->document_copy->getClientOriginalExtension();
          $fileName = time().'.'.$extension;
          $request->document_copy->move(storage_path('app/public/offence_increment'), $fileName);
          }
          else
          {
            $fileName = $decrement_set->document_copy;
          }
          if ($request->start_date == null) {
            $start_date = null;
          }
          else{
            $start_date = carbon::createFromFormat('Y-m-d',$request->start_date);
          }
          if ($request->end_date == null) {

            $end_date = null;
          }
          else{
            $end_date = carbon::createFromFormat('Y-m-d',$request->end_date);
          }
      $decrement_set->update([
        'increment_type_id' => $request->increment_type_id ,
        'user_id' => Auth::user()->id,
        'increment_date' => carbon::createFromFormat('Y-m-d',$request->increment_date),
        'start_date' => $start_date,
        'end_date' => $end_date,
        'remain_days' => $request->remain_days,
        'percentage' => $request->percentage,
        'days' => $request->days,
        'additional_detail' => $request->additional_detail,
        'document_copy' => $fileName
      ]);
      return redirect(route('open.hukum',['id' => $decrement_set->offence_id]));
    }
    public function printIncrement($id){

      $increment = offence_increment::Find($id);
    return view('hukum.increment',compact('increment'));
    }
    public function printDecrement($id){

      $decrement = offence_decrement::Find($id);
    return view('hukum.decrement',compact('decrement'));
    }

    public function addOffenceStatus($id){

      $hukum = offence::Find($id);
      $offence_status = offence_status::all();
      return view('hukum.add_hukum_status',compact('hukum','offence_status'));
    }


    public function saveOffenceStatus($id, request $request){

      if($request->hasFile('document_copy'))
          {

          $extension = $request->document_copy->getClientOriginalExtension();
          $fileName = time().'.'.$extension;
          $request->document_copy->move(storage_path('app/public/status_log'), $fileName);
          }
          else
          {
            $fileName = "no_file.png";

          }

      $offence_status = offence_status_log::create([
        'status_date' => $request->status_date,
        'detail' => $request->detail ,
        'document_copy' => $fileName,
        'offence_id' => $id,
        'offence_status_id' => $request->offence_status_id
      ]);

      //updating offence status
      $offence = offence::Find($offence_status->offence_id);
      $status_logs = offence_status_log::where('offence_id',$offence_status->offence->id)->latest('status_date')->first();
      if ($status_logs == null) {
        $offence->update([
          'offence_status_id' => null
        ]);
      }
      $offence->update([
        'offence_status_id' => $status_logs->offence_status_id
      ]);


      //updating offence inplimented Date
      $status_log_thanfeez_on_going = offence_status_log::where('offence_id',$offence_status->offence->id)->oldest('status_date')->first();
      if ($status_log_thanfeez_on_going == null) {
        $offence->update([
          'sentenced_implement_date' => null
        ]);
      }
      $offence->update([
        'sentenced_implement_date' => $status_log_thanfeez_on_going->status_date
      ]);

      return redirect(route('open.hukum',['id' => $id]));
    }

    public function editOffenceStatus($id){
      $offence_status = offence_status_log::Find($id);
      $hukum = offence::Find($offence_status->offence_id);
      $status = offence_status::all();
      return view('hukum.edit_offence_status',compact('hukum','offence_status','status'));

    }


    public function updateOffenceStatus($id, request $request){
      $offence_status = offence_status_log::Find($id);
      if($request->hasFile('document_copy'))
          {

          $extension = $request->document_copy->getClientOriginalExtension();
          $fileName = time().'.'.$extension;
          $request->document_copy->move(storage_path('app/public/status_log'), $fileName);
          }
          else
          {
            $fileName = "no_file.png";

          }

          $offence_status->update([
            'status_date' => $request->status_date,
            'offence_status_id' => $request->offence_status_id,
            'user_id' => Auth::user()->id,
            'detail' => $request->detail,
            'document_copy' => $fileName
          ]);

          return redirect(route('open.hukum',['id' => $offence_status->offence_id]));

    }

    public function deleteOffenceStatus($id){
      $offence_status = offence_status_log::Find($id);
      $routeID = $offence_status->offence_id;
      $offence_status->delete();
      return redirect(route('open.hukum',['id' => $routeID]));
    }



}
