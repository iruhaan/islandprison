<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\offence_increment;
use App\offence_decrement;
use App\offence;
use App\approval_status;
use App\offence_status_log;
use Auth;

class OffenderSupervisorController extends Controller
{
    public function index(){
      $offence_increment = offence_increment::where('approval_status_id',2)->get();
      $offence_decrements = offence_decrement::where('approval_status_id',2)->get();
      $offence_status_logs = offence_status_log::where('approval_status_id',2)->get();
      return view('offender_supervisor.offender_supervisor',compact('offence_increment','offence_decrements','offence_status_logs'));
    }

    public function approveIncrement($id){
      $increment = offence_increment::Find($id);
      if ($increment->approval_status_id == 2) {
        $hukum = offence::Find($increment->offence_id);
        $approval_status = approval_status::all();
        return view('offender_supervisor.approve_increment',compact('increment','hukum','approval_status'));
      }
      else {
        return redirect(route('offender.supervisor'))->withErrors(['ތިޔައީ ގޯހެއް، ތިޔަހެން މި ސިސްޓަމް ބޭނުންކުރާނަމަ، އެކަން އެބަ ރެކޯޑްކުރަން']);
      }

    }
    public function approveDecrement($id){
      $decrement = offence_decrement::Find($id);
      if ($decrement->approval_status_id == 2) {
        $hukum = offence::Find($decrement->offence_id);
        $approval_status = approval_status::all();
        return view('offender_supervisor.approve_decrement',compact('decrement','hukum','approval_status'));
      }
      else {
        return redirect(route('offender.supervisor'))->withErrors(['ތިޔައީ ގޯހެއް، ތިޔަހެން މި ސިސްޓަމް ބޭނުންކުރާނަމަ، އެކަން އެބަ ރެކޯޑްކުރަން']);
      }

    }

    public function offenceStatus($id){
      $offence_status = offence_status_log::Find($id);
      if ($offence_status->approval_status_id == 2) {
        $hukum = offence::Find($offence_status->offence_id);
        $approval_status = approval_status::all();
        return view('offender_supervisor.approval_status_log',compact('offence_status','hukum','approval_status'));
      }
      else {
        return redirect(route('offender.supervisor'))->withErrors(['ތިޔައީ ގޯހެއް، ތިޔަހެން މި ސިސްޓަމް ބޭނުންކުރާނަމަ، އެކަން އެބަ ރެކޯޑްކުރަން']);
      }
    }





    public function saveOffenceStatus($id, request $request){

      $offence_status = offence_status_log::Find($id);
      $offence = offence::Find($offence_status->offence_id);
      $offence_status->update($request->all());

      //updating offence status
      $status_logs = offence_status_log::where('approval_status_id',3)
      ->where('offence_id',$offence_status->offence->id)->latest('status_date')->first();
      if ($status_logs == null) {
        $offence->update([
          'offence_status_id' => null
        ]);
      }
      $offence->update([
        'offence_status_id' => $status_logs->offence_status_id
      ]);



      //updating offence inplimented Date
      $status_log_thanfeez_on_going = offence_status_log::where('offence_status_id', 1)
      ->where('approval_status_id',3)
      ->where('offence_id',$offence_status->offence->id)->oldest('status_date')->first();
      if ($status_log_thanfeez_on_going == null) {
        $offence->update([
          'sentenced_implement_date' => null
        ]);
      }
      $offence->update([
        'sentenced_implement_date' => $status_log_thanfeez_on_going->status_date
      ]);

      return redirect(route('offender.supervisor'));
    }







    public function saveIncrementApproval(request $request, $id){
      $increment = offence_increment::Find($id);
      $increment->update([
        'approval_status_id' => $request->approval_status_id,
        'approved_user_id' => Auth::user()->id
      ]);
      $offence = offence::Find($increment->offence_id);
      $total_increment_days = offence_increment::where('offence_id',$offence->id)->where('approval_status_id',3)->sum('days');
      $offence->update(['total_increment_days' => $total_increment_days]);
      return redirect(route('offender.supervisor'));
      }








    public function saveDecrementApproval(request $request, $id){
      $decrement = offence_decrement::Find($id);
      $decrement->update([
        'approval_status_id' => $request->approval_status_id,
        'approved_user_id' => Auth::user()->id
      ]);
      $offence = offence::Find($decrement->offence_id);
      $total_decrement_days = offence_decrement::where('offence_id',$offence->id)->where('approval_status_id',3)->sum('days');
      $offence->update(['total_decrement_days' => $total_decrement_days]);
      return redirect(route('offender.supervisor'));
      }





}
