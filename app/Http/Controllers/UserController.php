<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\user;

class UserController extends Controller
{

  public function __construct()
  {
      $this->middleware('admin');
  }



  public function view(){
    $users = user::all();
    return view('users.user',compact('users'));
  }

  public function edit($id){
    $user = user::FindOrFail($id);
    return view('users.edituser',compact('user'));
  }

  public function update(request $request,$id){
    $user = user::Find($id);
    if ($request->admin == null) {
      $admin = 0;
    }
    else{
      $admin = 1;
    }
    if ($request->hr == null) {
      $hr = 0;
    }
    else{
      $hr = 1;
    }
    if ($request->supervisor == null) {
      $supervisor = 0;
    }
    else{
      $supervisor = 1;
    }
    if ($request->commandhead == null) {
      $commandhead = 0;
    }
    else{
      $commandhead = 1;
    }
    if ($request->officer == null) {
      $officer = 0;
    }
    else{
      $officer = 1;
    }
    if ($request->reset_password == null) {
      $reset_password = 0;
    }
    else{
      $reset_password = 1;
    }
    if ($request->security == null) {
      $security = 0;
    }
    else{
      $security = 1;
    }
    if ($request->roster == null) {
      $roster = 0;
    }
    else{
      $roster = 1;
    }
    if ($request->operation == null) {
      $operation = 0;
    }
    else{
      $operation = 1;
    }
    $updateData = [
      'admin' =>$admin,
      'hr' =>$hr,
      'commandhead' => $commandhead,
      'supervisor' => $supervisor,
      'officer' => $officer,
      'security' => $security,
      'roster' => $roster,
      'operation' => $operation
    ];

    //reset user password if forgot admin can reset when tick reset button
      $user->update($updateData);
      if ($reset_password == 1) {
        $resetpassword = bcrypt("welcome123");
        $user->update(['password' => $resetpassword]);
      }

  }

  public function index(){
    return view('home');
  }



  public function updateYaumiyya(request $request,$id){
    $user = user::Find($id);

    //Administration
    if ($request->y_admin == null) {
      $y_admin = 0;
    }
    else{
      $y_admin = 1;
    }

    if ($request->y_hr == null) {
      $y_hr = 0;
    }
    else{
      $y_hr = 1;
    }

    if ($request->y_reception == null) {
      $y_reception = 0;
    }
    else{
      $y_reception = 1;
    }

    if ($request->y_stock == null) {
      $y_stock = 0;
    }
    else{
      $y_stock = 1;
    }

    if ($request->y_welfare == null) {
      $y_welfare = 0;
    }
    else{
      $y_welfare = 1;
    }

    if ($request->y_it == null) {
      $y_it = 0;
    }
    else{
      $y_it = 1;
    }

    if ($request->y_maintenance == null) {
      $y_maintenance = 0;
    }
    else{
      $y_maintenance = 1;
    }

    if ($request->y_transport == null) {
      $y_transport = 0;
    }
    else{
      $y_transport = 1;
    }

    if ($request->y_offender_record == null) {
      $y_offender_record = 0;
    }
    else{
      $y_offender_record = 1;
    }

    if ($request->y_canteen == null) {
      $y_canteen = 0;
    }
    else{
      $y_canteen = 1;
    }

    if ($request->y_visit == null) {
      $y_visit = 0;
    }
    else{
      $y_visit = 1;
    }

    if ($request->y_labour == null) {
      $y_labour = 0;
    }
    else{
      $y_labour = 1;
    }

    if ($request->y_orientation == null) {
      $y_orientation = 0;
    }
    else{
      $y_orientation = 1;
    }

    if ($request->y_complain == null) {
      $y_complain = 0;
    }
    else{
      $y_complain = 1;
    }

    if ($request->y_procecution == null) {
      $y_procecution = 0;
    }
    else{
      $y_procecution = 1;
    }

    if ($request->y_unit == null) {
      $y_unit = 0;
    }
    else{
      $y_unit = 1;
    }

    if ($request->y_case_management == null) {
      $y_case_management = 0;
    }
    else{
      $y_case_management = 1;
    }

    if ($request->y_councelling == null) {
      $y_councelling = 0;
    }
    else{
      $y_councelling = 1;
    }

    if ($request->y_library == null) {
      $y_library = 0;
    }
    else{
      $y_library = 1;
    }

    if ($request->y_interview == null) {
      $y_interview = 0;
    }
    else{
      $y_interview = 1;
    }

    if ($request->y_assessment == null) {
      $y_assessment = 0;
    }
    else{
      $y_assessment = 1;
    }

    if ($request->y_medical_record == null) {
      $y_medical_record = 0;
    }
    else{
      $y_medical_record = 1;
    }

    if ($request->y_medical_center == null) {
      $y_medical_center = 0;
    }
    else{
      $y_medical_center = 1;
    }

    if ($request->y_pharmacy == null) {
      $y_pharmacy = 0;
    }
    else{
      $y_pharmacy = 1;
    }

    if ($request->y_escort == null) {
      $y_escort = 0;
    }
    else{
      $y_escort = 1;
    }

    if ($request->y_special_security == null) {
      $y_special_security = 0;
    }
    else{
      $y_special_security = 1;
    }

    if ($request->y_investigation == null) {
      $y_investigation = 0;
    }
    else{
      $y_investigation = 1;
    }

    if ($request->y_inspection == null) {
      $y_inspection = 0;
    }
    else{
      $y_inspection = 1;
    }

    if ($request->y_gatehouse == null) {
      $y_gatehouse = 0;
    }
    else{
      $y_gatehouse = 1;
    }
    if ($request->y_addmission == null) {
      $y_addmission = 0;
    }
    else{
      $y_addmission = 1;
    }
    if ($request->y_incharge == null) {
      $y_incharge = 0;
    }
    else{
      $y_incharge = 1;
    }




  $user->update([
    'y_admin' => $y_admin,
    'y_hr' => $y_hr,
    'y_reception' => $y_reception,
    'y_stock' => $y_stock,
    'y_welfare' => $y_welfare,
    'y_transport' => $y_transport,
    'y_it' => $y_it,
    'y_maintenance' => $y_maintenance,
    'y_offender_record' => $y_offender_record,
    'y_visit' => $y_visit,
    'y_canteen' => $y_canteen,
    'y_labour' => $y_labour,
    'y_orientation' => $y_orientation,
    'y_procecution' => $y_procecution,
    'y_complain' => $y_complain,
    'y_unit' => $y_unit,
    'y_case_management' => $y_case_management,
    'y_councelling' => $y_councelling,
    'y_library' => $y_library,
    'y_interview' => $y_interview,
    'y_assessment' => $y_assessment,
    'y_medical_center' => $y_medical_center,
    'y_medical_record' => $y_medical_record,
    'y_pharmacy' => $y_pharmacy,
    'y_escort' => $y_escort,
    'y_special_security' => $y_special_security,
    'y_investigation' => $y_investigation,
    'y_inspection' => $y_inspection,
    'y_gatehouse' => $y_gatehouse,
    'y_addmission' => $y_addmission,
    'y_incharge' => $y_incharge
  ]);

}












}
