<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\user;
use App\prisoner;
use Auth;
use Validator;


class WelcomeController extends Controller
{
  public function __construct(){
    return $this->middleware('auth');
  }
    public function index(){
      $prisoners = prisoner::all();
      return view('home',compact('prisoners'));
    }

    public function changePassword($id){
      $user = user::where('service_number',Auth::user()->service_number)->first();
      return view('changePassword',compact('user'));
    }

    public function updatePassword(request $request,$id){
      $validator = Validator::make($request->all(), [
        'repeatpassword' => 'min:6|required_with:password|same:password',
        'password' => 'min:6'
      ]);
      if ($validator->fails()) {
          return redirect()->back()
                      ->withErrors($validator)
                      ->withInput($request->all());
      }
      $user = user::Find($id);
      $user->password = bcrypt($request->password);
      $user->update();
      Alert::success('ޕާސްވޯޑް ބަދަލުވެއްޖެ')->persistent("ތިކަން އެގިއްޖެ");
      Session::flush();
      return redirect(route('/'));

    }


}
