<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Redirect;
use App\user;
use Hash;
use Illuminate\Support\MessageBag;


class AuthController extends Controller
{
  public function loginForm()
{
    return view('newLogin');
}
  public function register()
{
    return view('newRegister');
}

  public function login(Request $request){
  $this->validate($request, [
      'email' => 'required',
      'password' => 'required',
      ]);
  if (\Auth::attempt([
      'email' => $request->email,
      'password' => $request->password])
  ){
      return redirect('/');
  }
  //return redirect('/login')->with('error', 'Service Number or Password');
  $errors = new MessageBag(['password' => ['Email and/or password invalid.']]);
   return Redirect::back()->withErrors($errors);
}
/* GET
*/
public function logout(Request $request)
{
  if(\Auth::check())
  {
      \Auth::logout();
      $request->session()->invalidate();
  }
  return  redirect('/');
}


public function create(request $request)
{
    User::create([
        'name' => $request->name,
        'email' => $request->email,
        'password' => Hash::make($request->password)
    ]);
  return  redirect('/login');
}


}
