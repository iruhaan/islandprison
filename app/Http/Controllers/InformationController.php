<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\country;
use App\atoll;
use App\island;
use App\blood_group;
use App\gang_name;
use App\unit;
use App\wing;
use App\cell;
use App\block;
use App\bed_number;
use App\prison;
use App\security_level;
use App\prisoner_type;
use App\gender;
use App\status;
use App\temp_status;
use App\sentence_type;
use App\offence_cat;
use App\offence_type;
use App\approval_status;
use App\court_name;
use App\decrement_type;
use App\increment_type;
use App\drug_offence_name;
use App\offence_status;
use App\sentence_present;

class InformationController extends Controller
{
    public function index(){
      return view('information.offender-information');
    }

    //Add and Delete Countries
    public function country(){
      $countries = country::all();
      return view('information.country',compact('countries'));
    }

    public function saveCountry(request $request){
      country::create($request->all());
      return redirect()->back();
    }

    public function deleteCountry($id){
      country::Find($id)->delete();
      return redirect()->back();
    }

    //Add and Delete Atolls
    public function atoll(){
      $atolls = atoll::all();
      return view('information.atoll',compact('atolls'));
    }

    public function saveAtoll(request $request){
      atoll::create($request->all());
      return redirect()->back();
    }

    public function deleteAtoll($id){
      atoll::Find($id)->delete();
      return redirect()->back();
    }

    //Add and Delete Island
    public function island(){
      $islands = island::all();
      $atolls = atoll::all();
      return view('information.island',compact('islands','atolls'));
    }

    public function saveIsland(request $request){

      island::create($request->all());
      return redirect()->back();
    }

    public function deleteIsland($id){
      island::Find($id)->delete();
      return redirect()->back();
    }


    //Add and Delete Blood Group
    public function Blood_group(){
      $blood_groups = blood_group::all();
      return view('information.blood_group',compact('blood_groups'));
    }

    public function saveBlood_group(request $request){

      blood_group::create($request->all());
      return redirect()->back();
    }

    public function deleteBlood_group($id){
      blood_group::Find($id)->delete();
      return redirect()->back();
    }

    //Add and Delete Gang Names
    public function gang_name(){
      $gang_names = gang_name::all();
      return view('information.gang_name',compact('gang_names'));
    }

    public function savegang_name(request $request){

      gang_name::create($request->all());
      return redirect()->back();
    }

    public function deletegang_name($id){
      gang_name::Find($id)->delete();
      return redirect()->back();
    }

    //Add and Delete Unit Name
    public function unit(){
      $units = unit::all();
      return view('information.unit',compact('units'));
    }

    public function sunit(request $request){

      unit::create($request->all());
      return redirect()->back();
    }

    public function dunit($id){
      unit::Find($id)->delete();
      return redirect()->back();
    }

    //Add and Delete Cells
    public function cell(){
      $cells = cell::all();
      return view('information.cell',compact('cells'));
    }
    public function scell(request $request){
      cell::create($request->all());
      return redirect()->back();
    }
    public function dcell($id){
      cell::Find($id)->delete();
      return redirect()->back();
    }

    //Add and Delete Wings
    public function wing(){
      $wings = wing::all();
      return view('information.wing',compact('wings'));
    }
    public function swing(request $request){
      wing::create($request->all());
      return redirect()->back();
    }
    public function dwing($id){
      wing::Find($id)->delete();
      return redirect()->back();
    }

    //Add and Delete Blocks
    public function block(){
      $blocks = block::all();
      return view('information.block',compact('blocks'));
    }
    public function sblock(request $request){
      block::create($request->all());
      return redirect()->back();
    }
    public function dblock($id){
      block::Find($id)->delete();
      return redirect()->back();
    }

    //Add and Delete Bed Number
    public function bed_number(){
      $bed_numbers = bed_number::all();
      return view('information.bed_number',compact('bed_numbers'));
    }
    public function sbed_number(request $request){
      bed_number::create($request->all());
      return redirect()->back();
    }
    public function dbed_number($id){
      bed_number::Find($id)->delete();
      return redirect()->back();
    }

    //Add and Delete Prison Names
    public function prison_name(){
      $prison_names = prison::all();
      return view('information.prison_name',compact('prison_names'));
    }
    public function sprison_name(request $request){
      prison::create($request->all());
      return redirect()->back();
    }
    public function dprison_name($id){
      prison::Find($id)->delete();
      return redirect()->back();
    }

    //Add and Delete Security Level Name
    public function security_level_name(){
      $security_level_names = security_level::all();
      return view('information.security_level_name',compact('security_level_names'));
    }
    public function ssecurity_level_name(request $request){
      security_level::create($request->all());
      return redirect()->back();
    }
    public function dsecurity_level_name($id){
      security_level::Find($id)->delete();
      return redirect()->back();
    }

    //Add and Delete Prisoner Types
    public function prisoner_type(){
      $prisoner_types = prisoner_type::all();
      return view('information.prisoner_type',compact('prisoner_types'));
    }
    public function sprisoner_type(request $request){
      prisoner_type::create($request->all());
      return redirect()->back();
    }
    public function dprisoner_type($id){
      prisoner_type::Find($id)->delete();
      return redirect()->back();
    }

    //Add and Delete Gender
    public function gender(){
      $genders = gender::all();
      return view('information.gender',compact('genders'));
    }
    public function sgender(request $request){
      gender::create($request->all());
      return redirect()->back();
    }
    public function dgender($id){
      gender::Find($id)->delete();
      return redirect()->back();
    }

    //Add and Delete Prisoner status name
    public function status(){
      $statuses = status::all();
      return view('information.status',compact('statuses'));
    }
    public function sstatus(request $request){
      status::create($request->all());
      return redirect()->back();
    }
    public function dstatus($id){
      status::Find($id)->delete();
      return redirect()->back();
    }

    //Add and Delete Prisoner temp status name
    public function temp_status(){
      $temp_statuses = temp_status::all();
      return view('information.temp_status',compact('temp_statuses'));
    }
    public function stemp_status(request $request){
      temp_status::create($request->all());
      return redirect()->back();
    }

    public function dtemp_status($id){
      temp_status::Find($id)->delete();
      return redirect()->back();
    }


    //Add and Delete Sentence Status
    public function sentence_type(){
      $sentence_types = sentence_type::all();
      return view('information.sentence_type',compact('sentence_types'));
    }
    public function ssentence_type(request $request){
      sentence_type::create($request->all());
      return redirect()->back();
    }
    public function dsentence_type($id){
      sentence_type::Find($id)->delete();
      return redirect()->back();
    }

    //Add and Delete Sentence Status
    public function offence_cat(){
      $offence_cats = offence_cat::all();
      return view('information.offence_cat',compact('offence_cats'));
    }
    public function soffence_cat(request $request){
      offence_cat::create($request->all());
      return redirect()->back();
    }
    public function doffence_cat($id){
      offence_cat::Find($id)->delete();
      return redirect()->back();
    }
    public function uoffence_cat($id, request $request){
      offence_cat::Find($id)->update($request->all());
      return redirect()->back();
    }

    //Add and Delete Sentence Types
    public function offence_type(){
      $offence_types = offence_type::all();
      $offence_cats = offence_cat::all();
      return view('information.offence_type',compact('offence_types','offence_cats'));
    }
    public function soffence_type(request $request){

      offence_type::create($request->all());
      return redirect()->back();
    }
    public function doffence_type($id){
      offence_type::Find($id)->delete();
      return redirect()->back();
    }
    public function uoffence_type($id, request $request){
      offence_type::Find($id)->update($request->all());
      return redirect()->back();
    }

    //Add and Delete Approval
    public function approval(){
      $approvals = approval_status::all();

      return view('information.approval',compact('approvals'));
    }
    public function sapproval(request $request){

      approval_status::create($request->all());
      return redirect()->back();
    }



        //Add and Delete Court Name
        public function court_name(){
          $court_names = court_name::all();
          return view('information.court_name',compact('court_names'));
        }
        public function scourt_name(request $request){

          court_name::create($request->all());
          return redirect()->back();
        }
        public function dcourt_name($id){
          court_name::Find($id)->delete();
          return redirect()->back();
        }
        public function ucourt_name($id, request $request){
          court_name::Find($id)->update($request->all());
          return redirect()->back();
        }

        //Add and Delete Offence Decrement
        public function decrement_type(){
          $decrement_types = decrement_type::all();
          return view('information.decrement_type',compact('decrement_types'));
        }
        public function sdecrement_type(request $request){

          decrement_type::create($request->all());
          return redirect()->back();
        }
        public function ddecrement_type($id){
          decrement_type::Find($id)->delete();
          return redirect()->back();
        }
        public function udecrement_type($id, request $request){
          decrement_type::Find($id)->update($request->all());
          return redirect()->back();
        }

        //Add and Delete Offence Decrement
        public function increment_type(){
          $increment_types = increment_type::all();
          return view('information.increment_type',compact('increment_types'));
        }
        public function sincrement_type(request $request){

          increment_type::create($request->all());
          return redirect()->back();
        }

        public function dincrement_type($id){
          increment_type::Find($id)->delete();
          return redirect()->back();
        }

        public function uincrement_type($id, request $request){
          increment_type::Find($id)->update($request->all());
          return redirect()->back();
        }

        //Add and Delete Drug Offence Name
        public function drug_offence_name(){
          $drug_offence_names = drug_offence_name::all();
          return view('information.drug_offence_name',compact('drug_offence_names'));
        }
        public function sdrug_offence_name(request $request){

          drug_offence_name::create($request->all());
          return redirect()->back();
        }

        public function ddrug_offence_name($id){
          drug_offence_name::Find($id)->delete();
          return redirect()->back();
        }

        public function udrug_offence_name($id, request $request){
          drug_offence_name::Find($id)->update($request->all());
          return redirect()->back();
        }

        //Add and Delete Offence Status
        public function offence_status(){
          $offence_statuses = offence_status::all();
          return view('information.offence_status',compact('offence_statuses'));
        }
        public function soffence_status(request $request){

          offence_status::create($request->all());
          return redirect()->back();
        }

        public function doffence_status($id){
          offence_status::Find($id)->delete();
          return redirect()->back();
        }

        public function uoffence_status($id, request $request){
          offence_status::Find($id)->update($request->all());
          return redirect()->back();
        }

        //Add and Delete Offence Status
        public function sentence_present(){
          $sentence_presents = sentence_present::all();
          return view('information.sentence_present',compact('sentence_presents'));
        }
        public function ssentence_present(request $request){

          sentence_present::create($request->all());
          return redirect()->back();
        }

        public function dsentence_present($id){
          sentence_present::Find($id)->delete();
          return redirect()->back();
        }

        public function usentence_present($id, request $request){
          sentence_present::Find($id)->update($request->all());
          return redirect()->back();
        }




}
