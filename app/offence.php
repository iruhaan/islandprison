<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class offence extends Model implements Auditable
{
  use \OwenIt\Auditing\Auditable;

    protected $fillable = [
      'prisoner_id',
      'offence_cat_id',
      'offence_type_id',
      'court_name_id',
      'offence_status_id',
      'sentenced_date',
      'sentenced_implement_date',
      'period_year',
      'period_month',
      'period_day',
      'total_period_days',
      'total_increment_days',
      'total_decrement_days',
      'court_order_no',
      'court_order_copy',
      'warrant_detail'
    ];



    public function prisoner(){
      return $this->belongsTo(prisoner::class);
    }

    public function sentence_present(){
      return $this->belongsTo(sentence_present::class);
    }

    public function court_name(){
      return $this->belongsTo(court_name::class);
    }

    public function offence_status(){
      return $this->belongsTo(offence_status::class)->withDefault();
    }

    public function offence_type(){
      return $this->belongsTo(offence_type::class)->withDefault();
    }

    public function offence_cat(){
      return $this->belongsTo(offence_cat::class);
    }

    public function sentence_type(){
      return $this->belongsTo(sentence_type::class);
    }

    public function drug_offence_name(){
      return $this->belongsTo(drug_offence_name::class)->withDefault();
    }

    public function offence_status_log(){
      return $this->hasMany(offence_status_log::class)->orderBy('status_date','asc');;
    }

    public function status_log(){
      return $this->hasMany(status_log::class);
    }

    public function offence_decrement(){
      return $this->hasMany(offence_decrement::class);
    }

    public function offence_increment(){
      return $this->hasMany(offence_increment::class);
    }



        public function getExpiredDate()
            {
              $totalDurationDays = $this->total_period_days;
              $totalIncementDays = $this->total_increment_days;
              $totalDecementDays = $this->total_Decrement_days;
              $implimentedDate = $this->sentenced_implement_date;

              $inc = $totalIncementDays;
              $dec = $totalDecementDays;
              $balance = ($totalDurationDays + $inc) - $dec;

              if($implimentedDate != NULL){
                $expiryDate = \carbon\carbon::createFromFormat('Y-m-d',$implimentedDate)
                ->addDays($balance)
                ->subDays(1)
                ->format('Y-m-d');
              }

              else{
                $expiryDate = null;
              }


              return "{$expiryDate}";
            }

}
