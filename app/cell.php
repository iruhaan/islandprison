<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class cell extends Model
{
  protected $fillable = [
    'cell'
  ];
}
