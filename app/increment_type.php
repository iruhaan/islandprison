<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class increment_type extends Model
{
    protected $fillable = [
      'increment_type'
     ];
}
