<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class offence_decrement extends Model implements Auditable
{
  use \OwenIt\Auditing\Auditable;
  protected $fillable = [
    'decrement_type_id',
    'offence_id',
    'decrement_date',
    'start_date',
    'end_date',
    'percentage',
    'remain_days',
    'days',
    'additional_detail',
    'document_copy'
  ];


  public function decrement_type(){
    return $this->belongsTo(decrement_type::class);
  }
  public function approval_status(){
    return $this->belongsTo(approval_status::class);
  }
  public function offence(){
    return $this->belongsTo(offence::class);
  }
}
