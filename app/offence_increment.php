<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class offence_increment extends Model implements Auditable
{
  use \OwenIt\Auditing\Auditable;
  protected $fillable = [
    'increment_type_id',
    'offence_id',
    'increment_date',
    'start_date',
    'end_date',
    'days',
    'additional_detail',
    'document_copy'
  ];



  public function increment_type(){
    return $this->belongsTo(increment_type::class);
  }
  public function approval_status(){
    return $this->belongsTo(approval_status::class);
  }
  public function offence(){
    return $this->belongsTo(offence::class);
  }
}
