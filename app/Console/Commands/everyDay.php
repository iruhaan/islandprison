<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Process\Process;
use App\officer;
use App\attendence;
use App\asses;
use App\occ_date;
use carbon\Carbon;


class everyDay extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'everyDay:attendence_date_update';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This will add everyday date to attendences table - this will add after 10 days from current date';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
      $officers = officer::whereNotIn('status_id', [2,7,3,8])->get();
      foreach ($officers as $officer) {
        $today_date = date('m-d', strtotime(Carbon::now()->addDays(10)));
        $officer_job_date = date('m-d', strtotime($officer->join_date));
        if ($today_date < $officer_job_date) {
          $last_year = date('Y', strtotime(Carbon::now()->addDays(10))) - 1;
        }
        else {
          $last_year = date('Y', strtotime(Carbon::now()->addDays(10)));
        }
        attendence::create([
            'officer_id' => $officer->id,
            'attn_date' => Carbon::now()->addDays(10),
            'attn_year' => date('Y', strtotime(Carbon::now()->addDays(10))),
            'alaam_year' => $last_year,
            'attendencetype_id' => 1,
            'attn_month' => date('m', strtotime(Carbon::now()->addDays(10)))
            ]);
        asses::create([
          'daily_date' =>Carbon::now(),
          'officer_id' => $officer->id
        ]);
      }

      $date = occ_date::create(['daily_date' => Carbon::now()]);


      \App\y_ad::create(['occ_date_id' => $date->id]);
      \App\y_ss::create(['occ_date_id' => $date->id]);
      \App\y_off::create(['occ_date_id' => $date->id]);
      \App\y_unit::create(['occ_date_id' => $date->id]);
      \App\y_program::create(['occ_date_id' => $date->id]);
      \App\y_parole::create(['occ_date_id' => $date->id]);
      \App\y_health::create(['occ_date_id' => $date->id]);
      \App\y_movement::create(['occ_date_id' => $date->id]);
      \App\y_security::create(['occ_date_id' => $date->id]);
      \App\y_officer_incharge::create(['occ_date_id' => $date->id]);



      $ds = DIRECTORY_SEPARATOR;
      //
      // $host = env('DB_HOST');
      // $username = env('DB_USERNAME');
      // $password = env('DB_PASSWORD');
      // $database = env('DB_DATABASE');
      // $ts = time();
      // $path = database_path() . $ds . 'backups' . $ds . date('Y', $ts) . $ds . date('m', $ts) . $ds . date('d', $ts) . $ds;
      // $file = date('Y-m-d-His', $ts) . '-dump-' . $database . '.sql';
      // $command = sprintf('mysqldump -h %s -u %s -p\'%s\' %s > %s', $host, $username, $password, $database, $path . $file);
      // if (!is_dir($path)) {
      //     mkdir($path, 0755, true);
      // }
      // exec($command);


      // $tempLocation     = database_path().$ds .env('DB_DATABASE') . '_' . date("Y-m-d_Hi") . '.sql';
      // $process = new Process('mysqldump -u' .env('DB_USERNAME'). ' -p' .env('DB_PASSWORD'). ' ' .env('DB_DATABASE'). ' > ' .$tempLocation);
      // $process->run();
      // var_dump($process);


      $ds = DIRECTORY_SEPARATOR;
$host = env('DB_HOST');
$username = env('DB_USERNAME');
$password = env('DB_PASSWORD');
$database = env('DB_DATABASE');

//$mysqlpath = 'C:\xampp\mysql\bin\mysqldump';
$mysqlpath = 'mysqldump';

$ts = time();
//$path = 'C:\salesandinventory\Backups' . $ds . date('Y', $ts) . $ds . date('m', $ts) . $ds . date('d', $ts) . $ds;
$path = database_path() . $ds . date('Y', $ts) . $ds . date('m', $ts) . $ds . date('d', $ts) . $ds;
$file = date('Y-m-d-His', $ts) . '-dump-' . $database . '.sql';
$command = sprintf($mysqlpath . ' -u' . $username . ' -p' . $password . ' -h' . $host . ' ' . $database . ' > ' . $path . $file);

if (!is_dir($path))
{
    mkdir($path, 0755, true);
}

exec($command);



    }


}

/// To run cron tap run this code ::   php artisan eveyDay:attendence_date_update
