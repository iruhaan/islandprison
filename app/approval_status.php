<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class approval_status extends Model
{
    protected $fillable = [
      'approval_status'
    ];
}
