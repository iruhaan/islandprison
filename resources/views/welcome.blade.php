@extends('layouts.myapp')
@section('content')

@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<div class="container">
  <div class="verticle-align-middle">

      <a href="{{route('create.officer')}}" target="_blank" style="font-size:20px;color:#fff;" class="faruma">
        <button type="button" name="button"style=";width:200px; float:left; font-size:20px; margin-top:20px;background-color:rgb(0,0,0,60%); border:none;" class="btn btn-primary">އައު މުވައްޒަފެއް އެޅުމަށް</button>
      </a>
      <input type="text" id="search" placeholder=" ހޯދާ" class="thaanaKeyboardInput faruma direction-right">
      <h2 class="waheedh" style="float:center; padding-top:20px;">ޕްރިޒަން / ސިވިލް އޮފިސަރުން</h2>
  </div>

<div class="officers">
    <table class="table direction-right faruma" id="table">
      <tbody>
        @foreach($staffs as $staff)
        <tr>
          <td style="text-align:center;" class="officerList" ><a href="{{action('StaffController@info',$staff->id)}}" target="_blank" style="color:#fff;

@if($staff->status_id > 1)
color:#E74C3C;
@endif

            ">{{ $staff->service_number }}</a></td>
          <td style="text-align:right;color:#fff;

@if($staff->status_id > 1)
color:#E74C3C;
@endif

          " class="faruma">{{ $staff->rank->rank_name }}</td>
          <td style="text-align:right;color:#fff;

@if($staff->status_id > 1)
color:#E74C3C;
@endif

          " class="faruma">{{ $staff->full_name }}</td>
          <td style="text-align:right;color:#fff;

@if($staff->status_id > 1)
color:#E74C3C;
@endif

          " class="faruma">{{ $staff->p_address }}</td>
          <td style="text-align:right;color:#fff;


@if($staff->status_id > 1)
color:#E74C3C;
@endif


          "class="faruma">{{ $staff->status->status_name }}</td>
          <td style="text-align:center;color:#fff;

@if($staff->status_id > 1)
color:#E74C3C;
@endif

          " >{{ $staff->contact_number }}</td>
          <td style="text-align:center;color:#fff;

@if($staff->status_id > 1)
color:#E74C3C;
@endif


          " >{{ $staff->nid_number }}</td>
          <td style="text-align:center;" >
              @if($staff->profile_picture_path == "unknown.jpg")

                    <img class="avatar-list" src="/images/unknown.jpg" alt="avatar"></td>

                  @else
                    <img class="avatar-list" src="/officerphoto/{{$staff->profile_picture_path}}" alt="avatar"></td>
                  @endif



        </tr>
        @endforeach
      </tbody>
    </table>
  </div>
  <br>
  <br>

  <div class="row">
    <div class="col-md-3">
      <a href="{{route('reports')}}" ><button style="margin: auto;" class="unit-btn"><span>Reports</span></button></a>
    </div>
    <div class="col-md-3">
      <a href="{{route('mandates')}}" ><button style="margin: auto;" class="unit-btn"><span>Officers Mendates</span></button></a>
    </div>
    <div class="col-md-3">
      <a href="{{route('smslog.all')}}" ><button style="margin: auto;" class="unit-btn"><span>SMS History</span></button></a>
    </div>
    <div class="col-md-3">
      <a href="{{route('sms')}}" ><button style="margin: auto;" class="unit-btn"><span>Send SMS</span></button></a>
    </div>
  </div>
  <br>
  <div class="row">
    <div class="col-md-3">
      <a href="{{route('yaumiyya.date')}}" ><button style="margin: auto;" class="unit-btn"><span>Yaumiyya</span></button></a>
    </div>
  </div>


  <br>
  <br>
  <br>
  <br>

  <!-- Requested Annual leave -->
  <h3 class="waheedh" style="color:#fff; text-align:center">ރިކުއެސްޓް ކޮށްފައިވާ އަހަރީ ޗުއްޓީ</h3>
  <table class="table faruma" style=";box-shadow:3px 4px 30px #000!important;border:none;background-color:#000;">
    <thead style="border:none;">
      <tr style="background-color:rgb(0,0,0,70%)!important; color:#fff;border:none;">
        <th style="text-align:center;">ސާރވިސް.ނ</th>
        <th style="text-align:right;">ނަން</th>
        <th style="text-align:center;">ޗުއްޓީ ފަށާތާރީޚް</th>
        <th style="text-align:center;">ޗުއްޓީ ނިމޭ ތާރީޚް</th>
        <th style="text-align:center;">މުއްދަތު</th>
        <th style="text-align:center;">އަނބުރާ ނިކުންނަށްޖެހޭ ދުވަސް</th>
        <th style="text-align:center;">ޗުއްޓީ ހޭދަކުރާ ތަން</th>
        <th style="text-align:center;">އެދުނު ސަބަބު</th>
        <th style="text-align:center;">ސުޕަވައިޒަ</th>
        <th style="text-align:center;">ކޮމާންޑަރ</th>
        <th style="text-align:center;">އޮފީސް</th>
      </tr>
    </thead>
    <tbody>
        @foreach($requestedLeave as  $leave);
        <tr>
        <td style="text-align:center;color:#fff;font-family:'avenir'"><a href="{{ route('hr.officer.portal',['id' => $leave->id]) }}">{{ $leave->officer->service_number }}</a></td>
        <td class="faruma" style="text-align:right;color:#fff;">{{ $leave->officer->full_name }}</td>
        <td style="text-align:center;color:#fff;font-family:'avenir'">{{ $leave->requested_leave_start_date }}</td>
        <td style="text-align:center;color:#fff;font-family:'avenir'">{{ $leave->requested_leave_end_date}}</td>
        <td style="text-align:center;color:#fff;font-family:'avenir'">{{ $leave->request_days }}</td>
        <td style="text-align:center;color:#fff;font-family:'avenir'">{{ $leave->return_date }}</td>
        <td class="faruma" style="text-align:center;color:#fff;">{{ $leave->leave_location }}</td>
        <td class="faruma" style="text-align:center;color:#fff;">{{ $leave->leave_reason }}</td>
        <td style="text-align:center;color:#fff;font-family:'avenir'">
          @if($leave->supervisor->approval_code == 1)
          <label style="background-color:red; color:#fff; border-radius:6px;">Declined</label>
          @elseif($leave->supervisor->approval_code ==2)
          <label style="background-color:green; color:#fff; border-radius:6px;">Approved</label>
          @else
          <label style="background-color:Orange; color:#000; border-radius:6px;">Pending</label>
          @endif
        </td>
        <td style="text-align:center;color:#fff;font-family:'avenir'">
          @if($leave->commander->approval_code == 1)
          <label style="background-color:red; color:#fff; border-radius:6px;">Declined</label>
          @elseif($leave->supervisor->approval_code ==2)
          <label style="background-color:green; color:#fff; border-radius:6px;">Approved</label>
          @else
          <label style="background-color:Orange; color:#000; border-radius:6px;">Pending</label>
          @endif
        </td>
        <td style="text-align:center;color:#fff;font-family:'avenir'">
          @if($leave->dp->approval_code == 1)
          <label style="background-color:red; color:#fff; border-radius:6px;">Declined</label>
          @elseif($leave->dp->approval_code ==2)
          <label style="background-color:green; color:#fff; border-radius:6px;">Approved</label>
          @else
          <label style="background-color:Orange; color:#000; border-radius:6px;">Pending</label>
          @endif
        </td>
      </tr>
      @endforeach
    </tbody>
  </table>


<br>
<br>


<!-- Expiring Contract  staff -->
<h3 class="waheedh" style="color:#fff; text-align:center">ކޮންޓްރެކްޓް މުއްޒަފުން | މުއްދަތު ހަމަވާން 20 ދުވަހަށްވުރެ މުއްދަތު ކުޑަވެފައިވާ</h3>
<br>
<table class="table faruma" style=";box-shadow:3px 4px 30px #000!important;border:none;background-color:#E74C3C;">
  <thead style="border:none;">
    <tr style="background-color:rgb(0,0,0,70%)!important; color:#fff;border:none;">
      <th style="text-align:center;">ސާރވިސް.ނ</th>
      <th style="text-align:right;">މަޤާމް</th>
      <th style="text-align:right;">ނަން</th>
      <th style="text-align:center;">ކޮންޓްރެކްޓް ހެދި ތާރީޚް</th>
      <th style="text-align:center;">މުއްދަތު | މަސް</th>
      <th style="text-align:center;">ހަމަވާ ތާރީޚް</th>
      <th style="text-align:center;">މުއްދަތު ހަވާނީ</th>
    </tr>
  </thead>
  <tbody>
    @foreach($contracts as $contract)
      <tr>
      <td style="text-align:center;color:#fff;font-family:'avenir'"><a style="color:#2ECC71;" target="_blank" href="{{route('index.contract',['id'=>$contract->officer_id])}}">{{ $contract->officer->service_number }}</a></td>
      <td style="text-align:right;color:#fff;" class="faruma">{{ $contract->officer->rank->rank_name }}</td>
      <td style="text-align:right;color:#fff;" class="faruma">{{ $contract->officer->full_name }}</td>
      <td style="text-align:center;color:#fff;font-family:'avenir'">{{ $contract->join_date}}</td>
      <td style="text-align:center;color:#fff;font-family:'avenir'">{{ $contract->duration }}</td>
      <td style="text-align:center;color:#fff;font-family:'avenir'">{{ $contract->expired_date }}</td>
      <td class="direction-left"style="text-align:center;color:#fff;font-family:'avenir'">{{ \Carbon\Carbon::parse($contract->expired_date)->diffForhumans() }}</td>
    </tr>
    @endforeach
  </tbody>
</table>


<br>

<!-- javaaabu dhaari vaan jehey  staff -->
<h3 class="waheedh" style="color:#fff; text-align:center">ގޮތް ނިންމަންޖެހޭ ލިޔެކިޔުން | މުވައްޒަފުން ހުށަހަޅާފައިވާ</h3>
<br>
<table class="table faruma" style=";box-shadow:3px 4px 30px #000!important;border:none;background-color:#E74C3C;">
  <thead style="border:none;">
    <tr style="background-color:rgb(0,0,0,70%)!important; color:#fff;border:none;">
      <th style="text-align:center;">ސާރވިސް.ނ</th>
      <th style="text-align:right;">މަޤާމް</th>
      <th style="text-align:right;">ނަން</th>
      <th style="text-align:center;">ހުށަހެޅި ތާރީޚް</th>
      <th style="text-align:right;">ބާވަތް</th>
      <th style="text-align:right;">ހުށަހެޅި ފަރާތް</th>
    </tr>
  </thead>
  <tbody>
    @foreach($documents as $document)
      <tr>
      <td style="text-align:center;color:#fff;font-family:'avenir'"><a style="color:#2ECC71;" target="_blank" href="{{route('document',['id'=>$document->officer_id])}}">{{ $document->officer->service_number }}</a></td>
      <td style="text-align:right;color:#fff;" class="faruma">{{ $document->officer->rank->rank_name }}</td>
      <td style="text-align:right;color:#fff;" class="faruma">{{ $document->officer->full_name }}</td>
      <td style="text-align:center;color:#fff;font-family:'avenir'">{{ $document->received_date}}</td>
      <td style="text-align:right;color:#fff;" class="faruma">{{ $document->letter_type->document_type}}</td>
      <td style="text-align:right;color:#fff;" class="faruma">{{ $document->to }}</td>
    </tr>
    @endforeach
  </tbody>
</table>
<br>

<!-- javaaabu dhaari vaan jehey  staff -->
<h3 class="waheedh" style="color:#fff; text-align:center">އޮފީހާ ޙަވާލުކުރަންޖެހޭ ޔުނިފޯމް އައިޓަމް</h3>
<br>
<table class="table faruma" style=";box-shadow:3px 4px 30px #000!important;border:none;background-color:#E74C3C;">
  <thead style="border:none;">
    <tr style="background-color:rgb(0,0,0,70%)!important; color:#fff;border:none;">
      <th style="text-align:center;">ސާރވިސް.ނ</th>
      <th style="text-align:right;">މަޤާމް</th>
      <th style="text-align:right;">ނަން</th>
      <th style="text-align:center;">ދިން ތާރީޚް</th>
      <th style="text-align:center;">އަދަދު</th>
      <th style="text-align:right;">އައިޓަމް</th>
    </tr>
  </thead>
  <tbody>
    @foreach($uniforms as $uniform)
      <tr>
      <td style="text-align:center;color:#fff;font-family:'avenir'"><a style="color:#2ECC71;" target="_blank" href="{{route('uniform',['id'=>$uniform->officer->id])}}">{{ $uniform->officer->service_number }}</a></td>
      <td style="text-align:right;color:#fff;" class="faruma">{{ $uniform->officer->rank->rank_name }}</td>
      <td style="text-align:right;color:#fff;" class="faruma">{{ $uniform->officer->full_name }}</td>
      <td style="text-align:center;color:#fff;font-family:'avenir'">{{ $uniform->given_date}}</td>
      <td style="text-align:center;color:#fff;font-family:'avenir'">{{ $uniform->qty}}</td>
      <td style="text-align:right;color:#fff;" class="faruma">{{ $uniform->item }}</td>
    </tr>
    @endforeach
  </tbody>
</table>



</div>
@stop
