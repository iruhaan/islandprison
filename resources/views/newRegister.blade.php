
<!--- Include the above in your HEAD tag ---------->

<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}" dir="ltr">
  <head>
    <meta charset="utf-8">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Male Prison HR | Login</title>

    <!--Bootsrap 4 CDN-->

  <!--Fontawesome CDN-->


    <!--Style Sheets -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Raleway" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="/css/login.css">

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}"></script>

  </head>
  <body>

    <div class="container1" id="container">
    	<div class="form-container sign-in-container1">
    		<form action="{{ route('register') }}" method="POST">
            {{ csrf_field() }}
    			<h1 style="color:#fff;">Register</h1>
    			<div class="social-container">
    				<img class="logo" src="/images/logo.png" alt="">
    			</div>

          <div class="{{ $errors->has('name') ? ' has-error' : '' }} inpu">

            <input  id="name" type="text" name="name" value="{{ old('name') }}" required autofocus placeholder="Full Name"/>
            @if ($errors->has('name'))
                    <strong>{{ $errors->first('name') }}</strong>

            @endif
          </div>

          <div class="{{ $errors->has('email') ? ' has-error' : '' }} inpu">

            <input  id="email" type="email" name="email" value="{{ old('email') }}" required autofocus placeholder="Email Address"/>
            @if ($errors->has('email'))
                    <strong>{{ $errors->first('email') }}</strong>

            @endif
          </div>

          <div class="{{ $errors->has('password') ? ' has-error' : '' }} inpu">

            <input  id="password" type="password" name="password" required placeholder="Password"/>
            @if ($errors->has('password'))
                <span class="help-block">
                    <strong>{{ $errors->first('password') }}</strong>
                </span>
            @endif
          </div>
          <div class="{{ $errors->has('password') ? ' has-error' : '' }} inpu">

            <input  id="password-confirm" type="password" name="password_confirmation" required placeholder="Confirm Password"/>
          </div>

    			<br>
    			<button class="btn-reg">Register</button>
    		</form>
    	</div>
    	<div class="overlay-container">
    		<div class="overlay">
    			<div class="overlay-panel overlay-right">
    				<h1>Hello, Officer!</h1>
    				<p style="line-height: 1.6; font-size:20px"><b>
              Your job gives you authority.<br>
              Your behavior gives you respect.
            </b></p><h3>Good Day, Officer!</h3>
    			</div>
    		</div>
    	</div>
    </div>


  </body>

</html>
