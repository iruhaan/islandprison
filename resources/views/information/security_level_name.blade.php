@extends('layouts.myapp')
@section('content')



<div class="container">
<h2 style="text-align:center">Security Level Names<h2>
<hr class="style-one">

<form class="" action="{{route('save.security_level_name')}}" method="post">
  {{CSRF_field()}}

<div class="row">
<div class="col-md-6">
  <label class="faruma lbl" for="security_level_eng">Level Name</label>
  <input required style="text-align:center;"type="text" name="security_level" value="">

</div>
</div>

<button type="submit" name=""><i style="color:#14CEBD;"class="fa fa-plus-circle fa-2x" aria-hidden="true"></i></button>

</form>


<br>
<table class="faruma" style="background-color:rgb(0,0,0,50%);">
  <thead>
    <tr style="background-color:rgb(229, 49, 10,20%);">
      <th style="text-align:center; font-size:17px;">Security Level Name</th>
      <th style="text-align:center; font-size:17px;">Delete</th>
    </tr>
  </thead>
  <tbody>
    @foreach($security_level_names as $security_level_name)
    <tr >
      <td style="text-align:center;" class="faruma">{{$security_level_name->security_level}}</td>
      <td style="text-align:center;"><a href={{route('delete.security_level_name',['id'=> $security_level_name->id])}}><i style="color:red;" class="fa fa-trash fa-2x" aria-hidden="true"></i></a></td>
    </tr>
    @endforeach
  </tbody>
</table>






</div>
@endsection
