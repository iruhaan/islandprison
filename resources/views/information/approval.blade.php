@extends('layouts.myapp')
@section('content')



<div class="container">
<h2 style="text-align:center">Approval Status<h2>
<hr class="style-one">

<ul>
  <li>1---Declined</li>
  <li>2---Pending</li>
  <li>3---Approved</li>
</ul>

<form class="" action="{{route('save.approval')}}" method="post">
  {{CSRF_field()}}

<div class="row">
<div class="col-md-2">
  <label class="faruma lbl" for="dummy" style="color:transparent">dummy</label>
<button type="submit" name=""><i style="color:#14CEBD;"class="fa fa-plus-circle fa-2x" aria-hidden="true"></i></button>
</div>

  <div class="col-md-5">
    <label class="faruma lbl" for="approval_status">Approval</label>
    <input type="text" name="approval_status" style="text-align:left;" class="">
  </div>


<div class="col-md-5">
</div>
</div>



</form>


<br>
<table style="background-color:rgb(0,0,0,50%);" class="faruma">
  <thead>
    <tr style="background-color:rgb(229, 49, 10,20%);">
      <th style="text-align:center; font-size:17px;">Approval</th>
    </tr>
  </thead>
  <tbody>
    @foreach($approvals as $approval)
    <tr >
      <td style="text-align:center;">{{$approval->approval_status}}</td>
    </tr>
    @endforeach
  </tbody>
</table>




</div>
@endsection
