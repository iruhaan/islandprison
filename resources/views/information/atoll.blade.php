@extends('layouts.myapp')
@section('content')



<div class="container">
<h2 style="text-align:center">Atolls<h2>
<hr class="style-one">

<form class="" action="{{route('save.atoll')}}" method="post">
  {{CSRF_field()}}

<div class="row">
<div class="col-md-3">
  <label class="faruma lbl" for="atoll_name_dhi">Atoll Name</label>
  <input required type="text" name="atoll_name" class="">
</div>
</div>

<button type="submit" name=""><i style="color:#14CEBD;"class="fa fa-plus-circle fa-2x" aria-hidden="true"></i></button>

</form>


<br>
<table class="faruma" style="background-color:rgb(0,0,0,50%);">
  <thead>
    <tr style="background-color:rgb(229, 49, 10,20%);">
      <th style="text-align:center; font-size:17px;">Name</th>
      <th style="text-align:center; font-size:17px;">Delete</th>
    </tr>
  </thead>
  <tbody>
    @foreach($atolls as $atoll)
    <tr >
      <td style="text-align:center;">{{$atoll->atoll_name}}</td>
      <td style="text-align:center;"><a href={{route('delete.atoll',['id'=> $atoll->id])}}><i style="color:red;" class="fa fa-trash fa-2x" aria-hidden="true"></i></a></td>
    </tr>
    @endforeach
  </tbody>
</table>






</div>
@endsection
