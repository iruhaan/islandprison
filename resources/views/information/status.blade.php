@extends('layouts.myapp')
@section('content')



<div class="container">
<h2 style="text-align:center">Prisomer status Name<h2>
<hr class="style-one">

<form class="" action="{{route('save.status')}}" method="post">
  {{CSRF_field()}}

<div class="row">
<div class="col-md-3">
  <label class="faruma lbl" for="status">Status</label>
  <input required type="text" name="status" style="text-align:center;">
</div>
</div>

<button type="submit" name=""><i style="color:#14CEBD;"class="fa fa-plus-circle fa-2x" aria-hidden="true"></i></button>

</form>


<br>
<table style="background-color:rgb(0,0,0,50%);" class="faruma">
  <thead>
    <tr style="background-color:rgb(229, 49, 10,20%);">
      <th style="text-align:center; font-size:17px;">prisoner status</th>
      <th style="text-align:center; font-size:17px;">delete</th>
    </tr>
  </thead>
  <tbody>
    @foreach($statuses as $status)
    <tr >
      <td style="text-align:center;">{{$status->status}}</td>
      <td style="text-align:center;"><a href={{route('delete.status',['id'=> $status->id])}}><i style="color:red;" class="fa fa-trash fa-2x" aria-hidden="true"></i></a></td>
    </tr>
    @endforeach
  </tbody>
</table>






</div>
@endsection
