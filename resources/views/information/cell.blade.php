@extends('layouts.myapp')
@section('content')



<div class="container">
<h2 style="text-align:center">Cell Names<h2>
<hr class="style-one">

<form class="" action="{{route('save.cell')}}" method="post">
  {{CSRF_field()}}

<div class="row">
<div class="col-md-2">

</div>
<div class="col-md-5">
</div>

<div class="col-md-5">
  <label class="faruma lbl" for="cell">ގޮޅީގެ ނަން</label>
  <input required type="text" name="cell" style="text-align:right;" class="faruma thaanaKeyboardInput">
</div>
</div>

<button type="submit" name=""><i style="color:#14CEBD;"class="fa fa-plus-circle fa-2x" aria-hidden="true"></i></button>

</form>


<br>
<table style="background-color:rgb(0,0,0,50%);" class="faruma">
  <thead>
    <tr style="background-color:rgb(229, 49, 10,20%);">
      <th style="text-align:center; font-size:17px;">unit name</th>
      <th style="text-align:center; font-size:17px;">delete</th>
    </tr>
  </thead>
  <tbody>
    @foreach($cells as $cell)
    <tr >
      <td style="text-align:center;">{{$cell->cell}}</td>
      <td style="text-align:center;"><a href={{route('delete.cell',['id'=> $cell->id])}}><i style="color:red;" class="fa fa-trash fa-2x" aria-hidden="true"></i></a></td>
    </tr>
    @endforeach
  </tbody>
</table>






</div>
@endsection
