@extends('layouts.myapp')
@section('content')



<div class="container">
<h2 style="text-align:center">Offence Present<h2>
<hr class="style-one">

<form class="" action="{{route('save.sentence_present')}}" method="post">
  {{CSRF_field()}}

<div class="row">
<div class="col-md-2">
  <label class="faruma lbl" for="dummy" style="color:transparent">dummy</label>
<button type="submit" name=""><i style="color:#14CEBD;"class="fa fa-plus-circle fa-2x" aria-hidden="true"></i></button>
</div>

  <div class="col-md-5">
  </div>


<div class="col-md-5">
  <label class="faruma lbl" for="sentence_present">ޙުކުމް ކުރި ގޮތް</label>
  <input required type="text" name="sentence_present" style="text-align:right;" class="faruma thaanaKeyboardInput">
</div>
</div>



</form>


<br>
<table style="background-color:rgb(0,0,0,50%);" class="faruma">
  <thead>
    <tr style="background-color:rgb(229, 49, 10,20%);">
      <th style="text-align:center; font-size:17px;">Offence Present | Dhivehi</th>
      <th style="text-align:center; font-size:17px;">delete</th>
      <th style="text-align:center; font-size:17px;">Edit</th>
    </tr>
  </thead>
  <tbody>
    @foreach($sentence_presents as $sentence_present)
    <tr >
      <td style="text-align:center;">{{$sentence_present->sentence_present}}</td>
      <td style="text-align:center;"><a href={{route('delete.sentence_present',['id'=> $sentence_present->id])}}><i style="color:red;" class="fa fa-trash fa-2x" aria-hidden="true"></i></a></td>
      <td style="text-align:center;"><button data-toggle="modal" data-target="#{{$sentence_present->id}}"><i style="color:Orange;" class="fa fa-pencil-square fa-2x" aria-hidden="true"></i></button></td>
      <!-- Modal -->
      <div id="{{$sentence_present->id}}" class="modal fade" role="dialog" aria-labelledby="exampleModalCenterTitle">
        <div class="modal-dialog modal-dialog-centered" role="document">

          <!-- Modal content-->
          <div class="modal-content modalback">
            <form method="post" action="{{route('update.sentence_present',['id'=> $sentence_present->id])}}">
                {{CSRF_field()}}
            <div class="modal-header">
              <button style="background:red;"type="button" class="close" data-dismiss="modal">&times;</button>
              <h4 class="faruma modal-title">މަޢުލޫމާތު ބަދަލުކުރުން</h4>
            </div>
            <div class="modal-body">
              <label class="faruma lbl" for="increment_type">ޙުކުމް ކުރި ގޮތް</label>
              <input value="{{$sentence_present->sentence_present}}" type="text" name="sentence_present" style="text-align:right;" class="faruma thaanaKeyboardInput">
            </div>
            <div class="modal-footer">
              <button class="btn" style="background:#E74C3C;border:none:color:#fff;"type="button"  data-dismiss="modal">Close</button>
              <input class="btn" style="background:#27AE60;border:none:color:#fff;" type="submit" name="" value="Save">
            </div>
          </form>
          </div>

        </div>
      </div>
    </tr>

    @endforeach
  </tbody>
</table>




</div>
@endsection
