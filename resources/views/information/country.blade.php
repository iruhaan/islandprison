@extends('layouts.myapp')
@section('content')



<div class="container">
<h2 style="text-align:center">Countries<h2>
<hr class="style-one">

<form class="" action="{{route('save.country')}}" method="post">
  {{CSRF_field()}}

<div class="row">
<div class="col-md-6">
  <label class="lbl" for="country_name">Country Name</label>
  <input required style="text-align:center;"type="text" name="country_name" value="">
</div>
</div>

<button type="submit" name=""><i style="color:#14CEBD;"class="fa fa-plus-circle fa-2x" aria-hidden="true"></i></button>

</form>


<br>
<table class="" style="background-color:rgb(0,0,0,50%);">
  <thead>
    <tr style="background-color:rgb(229, 49, 10,20%);">
      <th style="text-align:center; font-size:17px;">Name</th>
      <th style="text-align:center; font-size:17px;">Delete</th>
    </tr>
  </thead>
  <tbody>
    @foreach($countries as $country)
    <tr >
      <td style="text-align:center;" class="faruma">{{$country->country_name}}</td>
      <td style="text-align:center;"><a href={{route('delete.country',['id'=> $country->id])}}><i style="color:red;" class="fa fa-trash fa-2x" aria-hidden="true"></i></a></td>
    </tr>
    @endforeach
  </tbody>
</table>






</div>
@endsection
