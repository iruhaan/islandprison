@extends('layouts.myapp')
@section('content')



<div class="container">
<h2 style="text-align:center">Temporaty Out Status <h2>
<hr class="style-one">

<form class="" action="{{route('save.temp_status')}}" method="post">
  {{CSRF_field()}}

<div class="row">
<div class="col-md-2">

</div>
<div class="col-md-5">
</div>

<div class="col-md-5">
  <label class="faruma lbl" for="temp_status">ވަގުތީ ގޮތުން ބޭރަށް ނެރުން</label>
  <input required type="text" name="temp_status" style="text-align:right;" class="faruma thaanaKeyboardInput">
</div>
</div>

<button type="submit" name=""><i style="color:#14CEBD;"class="fa fa-plus-circle fa-2x" aria-hidden="true"></i></button>

</form>


<br>
<table style="background-color:rgb(0,0,0,50%);" class="faruma">
  <thead>
    <tr style="background-color:rgb(229, 49, 10,20%);">
      <th style="text-align:center; font-size:17px;">Temp out status</th>
      <th style="text-align:center; font-size:17px;">delete</th>
    </tr>
  </thead>
  <tbody>
    @foreach($temp_statuses as $temp_status)
    <tr >
      <td style="text-align:center;">{{$temp_status->temp_status}}</td>
      <td style="text-align:center;"><a href={{route('delete.temp_status',['id'=> $temp_status->id])}}><i style="color:red;" class="fa fa-trash fa-2x" aria-hidden="true"></i></a></td>
    </tr>
    @endforeach
  </tbody>
</table>






</div>
@endsection
