@extends('layouts.myapp')
@section('content')



<div class="container">
<h2 style="text-align:center">Bed Numbers<h2>
<hr class="style-one">

<form class="" action="{{route('save.bed_number')}}" method="post">
  {{CSRF_field()}}

<div class="row">
<div class="col-md-2">

</div>
<div class="col-md-5">
</div>

<div class="col-md-5">
  <label class="faruma lbl" for="bed_no">އެނދު ނަންބަރ</label>
  <input required type="text" name="bed_no" style="text-align:right;" class="faruma thaanaKeyboardInput">
</div>
</div>

<button type="submit" name=""><i style="color:#14CEBD;"class="fa fa-plus-circle fa-2x" aria-hidden="true"></i></button>

</form>


<br>
<table style="background-color:rgb(0,0,0,50%);" class="faruma">
  <thead>
    <tr style="background-color:rgb(229, 49, 10,20%);">
      <th style="text-align:center; font-size:17px;">Bed Numbers</th>
      <th style="text-align:center; font-size:17px;">delete</th>
    </tr>
  </thead>
  <tbody>
    @foreach($bed_numbers as $bed_number)
    <tr >
      <td style="text-align:center;">{{$bed_number->bed_no}}</td>
      <td style="text-align:center;"><a href={{route('delete.bed_number',['id'=> $bed_number->id])}}><i style="color:red;" class="fa fa-trash fa-2x" aria-hidden="true"></i></a></td>
    </tr>
    @endforeach
  </tbody>
</table>






</div>
@endsection
