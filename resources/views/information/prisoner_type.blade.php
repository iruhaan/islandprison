@extends('layouts.myapp')
@section('content')



<div class="container">
<h2 style="text-align:center">Prisoner Types<h2>
<hr class="style-one">

<form class="" action="{{route('save.prisoner_type')}}" method="post">
  {{CSRF_field()}}

<div class="row">
<div class="col-md-6">
  <label class="faruma lbl" for="prisoner_type_eng">ކުއްވެރިންގެ ބާވަތް | އިނގިރޭސިން</label>
  <input required style="text-align:center;"type="text" name="prisoner_type_eng" value="">

</div>
<div class="col-md-6">
  <label class="faruma lbl" for="prisoner_type_dhi">ކުއްވެރިންގެ ބާވަތް | ދިވެހިން</label>
  <input required type="text" name="prisoner_type_dhi" class=" faruma thaanaKeyboardInput">
</div>
</div>

<button type="submit" name=""><i style="color:#14CEBD;"class="fa fa-plus-circle fa-2x" aria-hidden="true"></i></button>

</form>


<br>
<table class="faruma" style="background-color:rgb(0,0,0,50%);">
  <thead>
    <tr style="background-color:rgb(229, 49, 10,20%);">
      <th style="text-align:center; font-size:17px;">Prisoner Type | English</th>
      <th style="text-align:center; font-size:17px;">Prisoner Type | dhivehi</th>
      <th style="text-align:center; font-size:17px;">Delete</th>
    </tr>
  </thead>
  <tbody>
    @foreach($prisoner_types as $prisoner_type)
    <tr >
      <td style="text-align:center;" class="faruma">{{$prisoner_type->prisoner_type_eng}}</td>
      <td style="text-align:center;">{{$prisoner_type->prisoner_type_dhi}}</td>
      <td style="text-align:center;"><a href={{route('delete.prisoner_type',['id'=> $prisoner_type->id])}}><i style="color:red;" class="fa fa-trash fa-2x" aria-hidden="true"></i></a></td>
    </tr>
    @endforeach
  </tbody>
</table>






</div>
@endsection
