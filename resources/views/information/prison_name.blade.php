@extends('layouts.myapp')
@section('content')



<div class="container">
<h2 style="text-align:center">Prison Names<h2>
<hr class="style-one">

<form class="" action="{{route('save.prison_name')}}" method="post">
  {{CSRF_field()}}

<div class="row">
<div class="col-md-6">
  <label class="faruma lbl" for="prison_eng">ޖަލުގެ ނަން | އިނގިރޭސިން</label>
  <input required style="text-align:center;"type="text" name="prison_eng" value="">

</div>
<div class="col-md-6">
  <label class="faruma lbl" for="prison_dhi">ޖަލުގެ ނަން | ދިވެހިނ</label>
  <input required type="text" name="prison_dhi" class=" faruma thaanaKeyboardInput">
</div>
</div>

<button type="submit" name=""><i style="color:#14CEBD;"class="fa fa-plus-circle fa-2x" aria-hidden="true"></i></button>

</form>


<br>
<table class="faruma" style="background-color:rgb(0,0,0,50%);">
  <thead>
    <tr style="background-color:rgb(229, 49, 10,20%);">
      <th style="text-align:center; font-size:17px;">Prison Name | English</th>
      <th style="text-align:center; font-size:17px;">Prison Name | dhivehi</th>
      <th style="text-align:center; font-size:17px;">Delete</th>
    </tr>
  </thead>
  <tbody>
    @foreach($prison_names as $prison_name)
    <tr >
      <td style="text-align:center;" class="faruma">{{$prison_name->prison_dhi}}</td>
      <td style="text-align:center;">{{$prison_name->prison_eng}}</td>
      <td style="text-align:center;"><a href={{route('delete.prison_name',['id'=> $prison_name->id])}}><i style="color:red;" class="fa fa-trash fa-2x" aria-hidden="true"></i></a></td>
    </tr>
    @endforeach
  </tbody>
</table>






</div>
@endsection
