@extends('layouts.myapp')
@section('content')



<div class="container">
<h2 style="text-align:center">Gender<h2>
<hr class="style-one">

<form class="" action="{{route('save.gender')}}" method="post">
  {{CSRF_field()}}

<div class="row">
<div class="col-md-6">
  <label class="faruma lbl" for="gender">Gender</label>
  <input required style="text-align:center;"type="text" name="gender" value="">
</div>
</div>

<button type="submit" name=""><i style="color:#14CEBD;"class="fa fa-plus-circle fa-2x" aria-hidden="true"></i></button>

</form>


<br>
<table class="faruma" style="background-color:rgb(0,0,0,50%);">
  <thead>
    <tr style="background-color:rgb(229, 49, 10,20%);">
      <th style="text-align:center; font-size:17px;">Gender</th>
      <th style="text-align:center; font-size:17px;">Delete</th>
    </tr>
  </thead>
  <tbody>
    @foreach($genders as $gender)
    <tr >
      <td style="text-align:center;" class="faruma">{{$gender->gender}}</td>
      <td style="text-align:center;"><a href={{route('delete.gender',['id'=> $gender->id])}}><i style="color:red;" class="fa fa-trash fa-2x" aria-hidden="true"></i></a></td>
    </tr>
    @endforeach
  </tbody>
</table>






</div>
@endsection
