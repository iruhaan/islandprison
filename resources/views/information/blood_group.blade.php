@extends('layouts.myapp')
@section('content')



<div class="container">
<h2 style="text-align:center">Blood Groups<h2>
<hr class="style-one">

<form class="" action="{{route('save.blood_group')}}" method="post">
  {{CSRF_field()}}

<div class="row">
<div class="col-md-2">

</div>
<div class="col-md-5">
</div>

<div class="col-md-5">
  <label class="faruma lbl" for="blood_group">ލޭގެ ގްރޫޕް</label>
  <input required type="text" name="blood_group" style="text-align:center;">
</div>
</div>

<button type="submit" name=""><i style="color:#14CEBD;"class="fa fa-plus-circle fa-2x" aria-hidden="true"></i></button>

</form>


<br>
<table style="background-color:rgb(0,0,0,50%);">
  <thead>
    <tr style="background-color:rgb(229, 49, 10,20%);">
      <th style="text-align:center; font-size:17px;">Delete</th>
      <th style="text-align:center; font-size:17px;">Blood Group</th>
    </tr>
  </thead>
  <tbody>
    @foreach($blood_groups as $blood_group)
    <tr >
      <td style="text-align:center;"><a href={{route('delete.blood_group',['id'=> $blood_group->id])}}><i style="color:red;" class="fa fa-trash fa-2x" aria-hidden="true"></i></a></td>
      <td style="text-align:center;">{{$blood_group->blood_group}}</td>
    </tr>
    @endforeach
  </tbody>
</table>






</div>
@endsection
