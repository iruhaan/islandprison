@extends('layouts.myapp')
@section('content')



<div class="container">
<h2 style="text-align:center">Offence Category<h2>
<hr class="style-one">

<form class="" action="{{route('save.offence_type')}}" method="post">
  {{CSRF_field()}}

<div class="row">


  <div class="col-md-3">
    <label class="faruma lbl" for="offence_type_eng">Select Gategory</label>
    <select required name="offence_cat_id" style="text-align-last:center;" class="faruma">
      <option disabled class="faruma" selected value="">Offence Category</option>
      @foreach($offence_cats as $offence_cat)
      <option class="faruma "value="{{$offence_cat->id}}">{{$offence_cat->offence_cat}}</option>
      @endforeach
    </select>
  </div>
  <div class="col-md-4">
    <label class="" for="offence">Offence Type</label>
    <input type="text" name="offence" style="text-align:center;" class="" required>
  </div>

  <div class="col-md-1">
    <label class="faruma lbl" for="dummy" style="color:transparent">dummy</label>
  <button type="submit" name=""><i style="color:#14CEBD;"class="fa fa-plus-circle fa-2x" aria-hidden="true"></i></button>
  </div>
</div>



</form>


<br>
<table style="background-color:rgb(0,0,0,50%);" class="faruma">
  <thead>
    <tr style="background-color:rgb(229, 49, 10,20%);">
      <th style="text-align:center; font-size:17px;">Sentence Type</th>
      <th style="text-align:center; font-size:17px;">Sentence Gategory</th>
      <th style="text-align:center; font-size:17px;">delete</th>
      <th style="text-align:center; font-size:17px;">Edit</th>
    </tr>
  </thead>
  <tbody>
    @foreach($offence_types as $offence_type)
    <tr >
      <td style="text-align:center;">{{$offence_type->offence}}</td>
      <td style="text-align:center;">{{$offence_type->offence_cat->offence_cat}}</td>
      <td style="text-align:center;"><a href={{route('delete.offence_type',['id'=> $offence_type->id])}}><i style="color:red;" class="fa fa-trash fa-2x" aria-hidden="true"></i></a></td>
      <td style="text-align:center;"><button data-toggle="modal" data-target="#{{$offence_type->id}}"><i style="color:Orange;" class="fa fa-pencil-square fa-2x" aria-hidden="true"></i></button></td>
      <!-- Modal -->
      <div id="{{$offence_type->id}}" class="modal fade" role="dialog" aria-labelledby="exampleModalCenterTitle">
        <div class="modal-dialog modal-dialog-centered" role="document">

          <!-- Modal content-->
          <div class="modal-content modalback">
            <form method="post" action="{{route('update.offence_type',['id'=> $offence_type->id])}}">
                {{CSRF_field()}}
            <div class="modal-header">
              <button style="background:red;"type="button" class="close" data-dismiss="modal">&times;</button>
              <h4 class="faruma modal-title">Change Info</h4>
            </div>
            <div class="modal-body">



              <label class="lbl" for="offence_type">Offence Type</label>
              <input value ="{{$offence_type->offence}}" type="text" name="offence" style="text-align:left;" class="">





              <label class="faruma lbl" for="offence_type_eng">Category</label>
              <select required name="offence_cat_id" style="text-align:left;" class="faruma">

                @foreach($offence_cats as $offence_cat)
                @if($offence_cat->id === $offence_type->offence_cat_id)
                <option selected class="faruma "value="{{$offence_cat->id}}">{{$offence_cat->offence_cat}}</option>
                @else
                <option class="faruma "value="{{$offence_cat->id}}">{{$offence_cat->offence_cat}}</option>
                @endif
                @endforeach

              </select>




            </div>
            <div class="modal-footer">
              <button class="btn" style="background:#E74C3C;border:none:color:#fff;"type="button"  data-dismiss="modal">Close</button>
              <input class="btn" style="background:#27AE60;border:none:color:#fff;" type="submit" name="" value="Save">
            </div>

          </form>
          </div>

        </div>
      </div>


    </tr>



    @endforeach
  </tbody>
</table>




</div>
@endsection
