@extends('layouts.myapp')
@section('content')



<div class="container">
<h2 style="text-align:center">Island<h2>
<hr class="style-one">

<form class="" action="{{route('save.island')}}" method="post">
  {{CSRF_field()}}

<div class="row">
<div class="col-md-2">
  <label class="faruma lbl" for="atoll_name">Select Atoll</label>
  <select  class="faruma" type="text" name="atoll_id">
    @foreach($atolls as $atoll)
    <option class="faruma" value="{{$atoll->id}}">{{$atoll->atoll_name}}</option>
    @endforeach
  </select>

</div>
<div class="col-md-5">
  <label class="faruma lbl" for="island_name">Island Name</label>
  <input required type="text" name="island_name" style="text-align:center;">
</div>
</div>

<button type="submit" name=""><i style="color:#14CEBD;"class="fa fa-plus-circle fa-2x" aria-hidden="true"></i></button>

</form>


<br>
<table class="faruma" style="background-color:rgb(0,0,0,50%);">
  <thead>
    <tr style="background-color:rgb(229, 49, 10,20%);">
      <th style="text-align:center; font-size:17px;">Name in English</th>
      <th style="text-align:center; font-size:17px;">Atoll</th>
      <th style="text-align:center; font-size:17px;">Delete</th>
    </tr>
  </thead>
  <tbody>
    @foreach($islands as $island)
    <tr >
      <td style="text-align:center;">{{$island->island_name}}</td>
      <td class="faruma" style="text-align:center;">{{$island->atoll->atoll_name}}</td>
      <td style="text-align:center;"><a href="{{route('delete.island',['id'=> $island->id])}}"><i style="color:red;" class="fa fa-trash fa-2x" aria-hidden="true"></i></a></td>
    </tr>
    @endforeach
  </tbody>
</table>






</div>
@endsection
