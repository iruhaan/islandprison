@extends('layouts.myapp')
@section('content')



<div class="container">
<h2 style="text-align:center">Sentence Type<h2>
<hr class="style-one">

<form class="" action="{{route('save.sentence_type')}}" method="post">
  {{CSRF_field()}}

<div class="row">
<div class="col-md-2">

</div>

  <div class="col-md-5">
    <label class="faruma lbl" for="sentence_type_eng">Sentence Type in English</label>
    <input type="text" name="sentence_type_eng" style="text-align:left;" class="">
  </div>


<div class="col-md-5">
  <label class="faruma lbl" for="sentence_type_dhi">ޙުކުމުގެ ނިޔާ</label>
  <input required type="text" name="sentence_type_dhi" style="text-align:right;" class="faruma thaanaKeyboardInput">
</div>
</div>

<button type="submit" name=""><i style="color:#14CEBD;"class="fa fa-plus-circle fa-2x" aria-hidden="true"></i></button>

</form>


<br>
<table style="background-color:rgb(0,0,0,50%);" class="faruma">
  <thead>
    <tr style="background-color:rgb(229, 49, 10,20%);">
      <th style="text-align:center; font-size:17px;">Sentence Type | Dhivehi</th>
      <th style="text-align:center; font-size:17px;">Sentence Type | English</th>
      <th style="text-align:center; font-size:17px;">delete</th>
    </tr>
  </thead>
  <tbody>
    @foreach($sentence_types as $sentence_type)
    <tr >
      <td style="text-align:center;">{{$sentence_type->sentence_type_dhi}}</td>
      <td style="text-align:center;">{{$sentence_type->sentence_type_eng}}</td>
      <td style="text-align:center;"><a href={{route('delete.sentence_type',['id'=> $sentence_type->id])}}><i style="color:red;" class="fa fa-trash fa-2x" aria-hidden="true"></i></a></td>
    </tr>
    @endforeach
  </tbody>
</table>






</div>
@endsection
