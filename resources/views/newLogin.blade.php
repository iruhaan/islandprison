
<!--- Include the above in your HEAD tag ---------->

<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}" dir="ltr">
  <head>
    <meta charset="utf-8">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Male Prison HR | Login</title>

    <!--Bootsrap 4 CDN-->

  <!--Fontawesome CDN-->


    <!--Style Sheets -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Raleway" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="/css/login.css">

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}"></script>

  </head>
  <body>

    <div class="container" id="container">
    	<div class="form-container sign-in-container">
    		<form action="{{ route('login') }}" method="POST">
            {{ csrf_field() }}
    			<h1 style="color:#fff;">Login</h1>
    			<div class="social-container">
    				<img class="logo" src="/images/logo.png" alt="">
    			</div>

          <div class="{{ $errors->has('service_number') ? ' has-error' : '' }} inpu">

            <input  id="email" type="email" name="email" value="{{ old('email') }}" required autofocus / placeholder="E Mail Address">
            @if ($errors->has('password'))
                <span class="help-block">
                    <strong>{{ $errors->first('password') }}</strong>
                </span>
            @endif
          </div>

          <div class="{{ $errors->has('password') ? ' has-error' : '' }} inpu">
            <input  id="password" type="password" name="password" required placeholder="Password" />

          </div>

    			<br>
    			<button>Sign In</button>
    		</form>
    	</div>
    	<div class="overlay-container">
    		<div class="overlay">
    			<div class="overlay-panel overlay-right">
    				<h1>Hello, Officer!</h1>
    				<p style="line-height: 1.6; font-size:20px">We are the officers that are rarely regarded as being part of law enforcement, we are the ones who have to control ruthless, mean, and violent crime offenders without any weapons.
            </p><h3>Good Day, Officer!</h3>

    			</div>
    		</div>
    	</div>
    </div>



  </body>

</html>
