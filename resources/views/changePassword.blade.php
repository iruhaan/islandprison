
<!--- Include the above in your HEAD tag ---------->

<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}" dir="ltr">
  <head>
    <meta charset="utf-8">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Male Prison HR | Reset Password</title>

    <!--Bootsrap 4 CDN-->

  <!--Fontawesome CDN-->


    <!--Style Sheets -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Raleway" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="/fonts/font.css">
    <link rel="stylesheet" type="text/css" href="/css/font-awesome.css">

    <style media="screen">
.background
{
  background-image: url('/images/coverphoto.jpg');
  background-position: bottom;
  background-repeat: no-repeat;
  background-size: cover;
  height: 100vh;
  width: 100%;
  display: block;
  background-color: rgb(202, 207, 210,10%);
background-blend-mode: screen;

  /* -webkit-filter: blur(5px);
-moz-filter: blur(5px);
-o-filter: blur(5px);
-ms-filter: blur(5px);
filter: blur(5px); */
}


.container
{
  position: absolute;
  left: 50%;
  top: 50%;
  transform: translate(-50%, -50%);
  width: 800px;
  height: 530px;
  background: rgb(0,0,0,75%);
  border-radius: 25px;
  box-shadow: 0 2px 0px #003B3F;
  display: block;
  overflow: hidden;

}

.box-left
{
  width: 40%;
  padding-top: 60px;

  float: left;
  text-align: center;
  background: transparent;

}

.box-right
{

  width: 60%;
  float: left;
  height: 100%;
  background: transparent;

}

.in_right
{
  color: #fff;
  text-align: center;
  margin-top: 40px;
}

.in_right header, .box-left header
{
font-size: 32px;
font-weight: 400;
margin: 10px auto;
}

.in_right button
{
  background: transparent;
  margin: 10px auto;
  border-radius: 6px;
  cursor: pointer;
  padding: 6px;
}

.in_right button:hover
{
  background: linear-gradient(90deg,#000, #000);
}

.in_right p
{
  margin-right: 30px;
  font-size: 14px;

}

.in_left
{
  text-align: center;
  margin: 10px auto;

}

.logo
{
  text-align: center;
  color:#07D3C0
}

input
{
  display: block;
  margin: auto;
  margin-bottom:20px;
  height:40px;
  border-radius:50px;
  text-align:center;
  font-size:18px;
  background-color:rgb(231, 76, 60,40%);
  color:#fff;
  border:none;
}



.btnsave
{
  width: 50%;
  border-radius: 6px;
  background: linear-gradient(90deg,#049285, #003B3F);
  padding: 6px;
  cursor: pointer;
  color:#fff;
  margin: 25px auto;
  /* border: 2px solid gray; */
  border: none;
  box-shadow: 0 2px 4px #505050;
  font-size:19px;

}

.btnsave:hover
{
  background: linear-gradient(90deg,#48035A, #000);
  font-size:19px;
}
.fg
{
  text-align: center;
  font-size: 14px;
  color: #505050;
  cursor: pointer;
}
    </style>


    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}"></script>

  </head>
  <body>
<div class="background">
</div>

    <form method="post" action="{{route('update.password',['id'=>$user->id])}}" style="margin:0;">{{CSRF_field()}}
    <div class="container">

           <div class="box-left">
               <div class="logo">
                   <img src="/images/logo.png" style="width:90px;" alt="">
                   <header class="waheedh">ޕާސްވޯޑް ބަދަލުކުރުން</header>
               </div>
               <div class="in_left">
                  <label for="service_number" class="faruma" style="text-align:right;color:#fff;">ސާރވިސް ނަންބަރ</label>
                   <input type="text" name="service_number" value="{{$user->service_number}}" placeholder="ސާރވިސް ނަންބަރ" class="faruma" disabled>

                   <label for="password" class="faruma" style="text-align:right;color:#fff;">އިޚްތިޔާރުކުރާ ޕާސްވޯޑް ޖައްސަވާ</label>
                   <input  autocomplete="new-password" type="password" name="password" value="" placeholder="ޕާސްވޯޑް" class="faruma" required>

                   <label for="repeatpassword" class="faruma" style="text-align:right;color:#fff;">އިޚްތިޔާރުކުރި ޕާސްވޯޑް ކަށަވަރުކުރުމަށް އަދި ޖައްސަވާ</label>
                   <input  autocomplete="new-password" type="password" name="repeatpassword" value="" placeholder="ކަށަވަރުކުރާ ޕާސްވޯޑް" class="faruma" required>
               </div>
           </div>
           <div class="box-right">
               <div class="in_right">
                   <header class="waheedh" style="margin-right:40px;">އެއްބަސްވުން</header>
                   <p class="faruma" style="line-height:40px;font-size:18px;text-align:justify;">
                       މި ސިސްޓަމަކީ މާލެޖަލުގައި މަސައްކަތްކުރާ އޮފިސަރުންނާއި މުވައްޒަފުންގެ މަޢުލޫމާތާއި ޖަލުގެ އެކި މަޢުލޫމާތު ރައްކާކުރުމާއި,
                       ފަސޭހަކަމާއި އެކު މަޢުލޫމާތު ހޯދުމަށާއި އަދިވެސް އެހެނިހެން މުހިންމު ކަންކަމަށް ބޭނުންކުރާ ސިސްޓަމެކެވެ. އެގޮތުން
                       މި ސިސްޓަމްގައި ސިއްރު އެތަށް މަޢުލޫމާތެއް އެކުލެވިގެންވާނެއެވެ. މި ސިސްޓަމް ބޭނުން ކުރުމުގައި ސައްޙަކަން ކަށަވަރުކޮށް
                       ފަރުވާތެރިކަމާއެކު މަޢުލޫމާތު ރައްކާކުރުމަށް އެދެމެވެ. އަދި މި ސިސްޓަމްގައިވާ އެއްވެސް މަޢުލޫމާތެއް
                       ކަމާއި ނުގުޅޭ ފަރާތަކާއި ހިއްސާކުރުމަކީ ކުށެކެވެ. މަތީގައި ބަޔާންވެދިޔަ ގޮތަށް ޢަމަލުކުރުމަށް އެއްބަސްވަމެވެ.
               </p>
                   <input class="waheedh btnsave"type="submit" value="މަޢުލޫމާތު ރައްކާކުރައްވާ"/>
               </div>
           </div>

       </div>
     </form>
     @if ($errors->any())
         <div class="alert alert-danger" style="position: fixed;left:20%;right:20%">
             <ul>
                 @foreach ($errors->all() as $error)
                     <ol class="faruma" style="font-size:17px; text-align:right;"> <i class="fa fa-exclamation-triangle" aria-hidden="true"></i> &nbsp;{{ $error }} </ol>
                 @endforeach
             </ul>
         </div>
     @endif




  </body>

</html>
