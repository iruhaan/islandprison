@extends('layouts.myapp')
@section('content')
<div class="container">


<h2 style="text-align:center;"class="waheedh">އެޕްރޫވް ކުރަންޖެހޭ ޙުކުމްތައް</h2>
<hr class="style-one">

<h2 style="text-align:center;"class="waheedh">އެޕްރޫވް ކުރަންޖެހޭ | ޙުކުމް ތަންފީޛް ކުރާގޮތުގެ ތަފްސީލް</h2>
<hr class="style-one">
<table class="faruma" style="margin-bottom:150px;">
  <thead>
    <tr style="background-color:transparent;">
      <th class="faruma" style="width:15%;font-size:18px;text-align:center;padding-right:20px;">ޤަޟިއްޔާ ނަންބަރު</th>
      <th class="faruma" style="width:15%;font-size:18px;text-align:center;padding-right:20px;">ޢަމަލުކުރާ ތާރީޚް</th>
      <th class="faruma" style="width:30%;font-size:18px;text-align:right;">ސްޓޭޓަސް</th>
      <th class="faruma" style="width:50%;font-size:18px;text-align:right;right;padding-right:20px;">ތަފްސީލް</th>
      <th class="faruma" style="font-size:18px;text-align:center;"></th>
    </tr>
  </thead>
  <tbody style="background:rgb(0,0,0,54%);text-decoration: none!important;">
    @foreach($offence_status_logs as $log)
    <tr>
      <td style="border-top: 1px solid #48C9B0;border-bottom: 1px solid #48C9B0;text-align:center;" >{{$log->offence->gaziyya_no}}</td>
      <td style="border-top: 1px solid #48C9B0;border-bottom: 1px solid #48C9B0;text-align:center;" >{{$log->status_date}}</td>
      <td class="faruma" style=" border-top: 1px solid #48C9B0;border-bottom: 1px solid #48C9B0;text-align:right;" >{{$log->offence_status->offence_status}}</td>
      <td class="faruma" style="line-height: 30px;padding :20px; border-top: 1px solid #48C9B0;border-bottom: 1px solid #48C9B0;text-align:justify;" >{{$log->detail}}</td>
      <td class="faruma" style="border-top: 1px solid #48C9B0;border-bottom: 1px solid #48C9B0;text-align:center;" ><a href="{{route('approve.offence.status',['id' => $log->id])}}" style="color:#F1C40F;"><i class="fa fa-pencil-square-o fa-2x" aria-hidden="true"></i></a></td>
    </tr>
    @endforeach
  </tbody>
</table>


<h2 style="text-align:center;"class="waheedh">އެޕްރޫވް ކުރަންޖެހޭ | ޙުކުމަށް ދުވަސް އިތުރުކުރުން (+)</h2>
<hr class="style-one">
<table class="faruma" style="margin-bottom:150px;">
  <thead>
    <tr style="background-color:transparent;">
      <th class="faruma" style="font-size:18px;text-align:right;padding-right:20px;">ސާބިތުވި މައްސަލަ</th>
      <th class="faruma" style="font-size:18px;text-align:center;">ޙުކުމުގެ މުއްދަތު</th>
      <th class="faruma" style="font-size:18px;text-align:right;right;padding-right:20px;">އިތުރުކުރާ ތަފްސީލް</th>
      <th class="faruma" style="font-size:18px;text-align:center;"  >އިތުރުކުރާ ދުވަސް</th>
      <th class="faruma" style="font-size:18px;text-align:center;"  ></th>
    </tr>
  </thead>
  <tbody style="background:rgb(0,0,0,54%);text-decoration: none!important;">
    @foreach($offence_increment as $offence)
    <tr>
      <td class="faruma" style="padding-right:20px; border-top: 1px solid #48C9B0;border-bottom: 1px solid #48C9B0;text-align:right;" >{{$offence->offence->offence_type->offence_type_dhi}}</td>
      <td class="faruma" style=" border-top: 1px solid #48C9B0;border-bottom: 1px solid #48C9B0;text-align:center;" >{{$offence->offence->period_year." އަހަރާއި"}} {{$offence->offence->period_month." މަހާއި"}} {{$offence->offence->period_day." ދުވަސް"}}</td>
      <td class="faruma" style=" padding-right:20px;border-top: 1px solid #48C9B0;border-bottom: 1px solid #48C9B0;text-align:right;" >{{$offence->increment_type->increment_type}}</td>
      <td class="faruma" style="border-top: 1px solid #48C9B0;border-bottom: 1px solid #48C9B0;text-align:center;" >{{$offence->days}}</td>
      <td class="faruma" style="border-top: 1px solid #48C9B0;border-bottom: 1px solid #48C9B0;text-align:center;" ><a href="{{route('approve.offender.increment',['id' => $offence->id])}}" style="color:#F1C40F;"><i class="fa fa-pencil-square-o fa-2x" aria-hidden="true"></i></a></td>
    </tr>
    @endforeach
  </tbody>
</table>


<h2 style="text-align:center;"class="waheedh">އެޕްރޫވް ކުރަންޖެހޭ | ޙުކުމުން ދުވަސް އުނިކުރުން (-)</h2>
<hr class="style-one">
<table class="faruma" style="margin-bottom:150px;">
  <thead>
    <tr style="background-color:transparent;">
      <th class="faruma" style="font-size:18px;text-align:right;padding-right:20px;">ސާބިތުވި މައްސަލަ</th>
      <th class="faruma" style="font-size:18px;text-align:center;">ޙުކުމުގެ މުއްދަތު</th>
      <th class="faruma" style="font-size:18px;text-align:right;right;padding-right:20px;">އުނިކުރާ ތަފްސީލް</th>
      <th class="faruma" style="font-size:18px;text-align:center;">އުނިކުރާ ދުވަސް</th>
      <th class="faruma" style="font-size:18px;text-align:center;"></th>
    </tr>
  </thead>
  <tbody style="background:rgb(0,0,0,54%);text-decoration: none!important;">
    @foreach($offence_decrements as $offence)
    <tr>
      <td class="faruma" style="padding-right:20px; border-top: 1px solid #48C9B0;border-bottom: 1px solid #48C9B0;text-align:right;" >{{$offence->offence->offence_type->offence_type_dhi}}</td>
      <td class="faruma" style=" border-top: 1px solid #48C9B0;border-bottom: 1px solid #48C9B0;text-align:center;" >{{$offence->offence->period_year." އަހަރާއި"}} {{$offence->offence->period_month." މަހާއި"}} {{$offence->offence->period_day." ދުވަސް"}}</td>
      <td class="faruma" style=" padding-right:20px;border-top: 1px solid #48C9B0;border-bottom: 1px solid #48C9B0;text-align:right;" >{{$offence->decrement_type->decrement_type}}</td>
      <td class="faruma" style="border-top: 1px solid #48C9B0;border-bottom: 1px solid #48C9B0;text-align:center;" >{{$offence->days}}</td>
      <td class="faruma" style="border-top: 1px solid #48C9B0;border-bottom: 1px solid #48C9B0;text-align:center;" ><a href="{{route('approve.offender.decrement',$offence->id)}}" style="color:#F1C40F;"><i class="fa fa-pencil-square-o fa-2x" aria-hidden="true"></i></a></td>
    </tr>
    @endforeach
  </tbody>
</table>
















</div>



@endsection
