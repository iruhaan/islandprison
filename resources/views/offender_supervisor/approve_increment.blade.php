@extends('layouts.myapp')
@section('content')
<div class="container">



<div class="row">
  <div class="col-md-3">
    <img class="info-profile-photo" src="{{ asset('storage/prisoner/'.$hukum->prisoner->profile_path) }}">
  </div>
  <div class="col-md-5" style="top:40px;">
    <li style="margin:6px 2px 6px 2px; font-size:30px; font-family: 'raleway'; text-align:left;">{{$hukum->prisoner->full_name_eng}}</li>
    <li style="margin:6px 2px 6px 2px; font-size:20px; font-family: 'raleway'; text-align:left;">{{$hukum->prisoner->country->country_name_eng}}</li>
    <li style="margin:6px 2px 6px 2px; font-size:20px; font-family: 'raleway'; text-align:left;">{{$hukum->prisoner->atoll->atoll_name_eng}} . {{$hukum->prisoner->island->island_name_eng}}</li>
    <li style="margin:6px 2px 6px 2px; font-size:20px; font-family: 'raleway'; text-align:left;">{{$hukum->prisoner->address_eng}}</li>
  </div>
  <div class="col-md-4" style="top:40px;">
    <li style="margin:6px 2px 6px 2px; font-size:30px; text-align:right;" class="waheedh">{{$hukum->prisoner->full_name_dhi}}</li>
    <li style="margin:6px 2px 6px 2px; font-size:20px; text-align:right;" class="waheedh">{{$hukum->prisoner->country->country_name_dhi}}</li>
    <li style="margin:6px 2px 6px 2px; font-size:20px; text-align:right;" class="waheedh">{{$hukum->prisoner->atoll->atoll_name_dhi}} . {{$hukum->prisoner->island->island_name_dhi}}</li>
    <li style="margin:6px 2px 6px 2px; font-size:20px; text-align:right;" class="waheedh">{{$hukum->prisoner->address_dhi}}</li>
  </div>
</div>



<br><br>
<h2 style="text-align:center;"class="waheedh">ޙުކުމުގެ މަޢުލޫމާތު</h2>
<hr class="style-one">

<h2 style="text-align:center" class="waheedh">{{$hukum->offence_type->offence_type_dhi}} | {{$hukum->sentence_type->sentence_type_dhi}}<h2>
  <hr class="style-one">

  <div class="row">
    <div class="col-md-3">
      <label class="lbl">ކަނޑައެޅި ނިޔާ</label>
      <label class="text-info faruma">{{$hukum->sentence_type->sentence_type_dhi}}</label>
    </div>
    <div class="col-md-3">
      <label class="lbl">ސާބިތުވި ކުށް</label>
      <label class="text-info faruma">{{$hukum->offence_type->offence_type_dhi}}</label>
    </div>
    <div class="col-md-3">
      <label class="lbl">ކުށުގެ ކެޓަގަރީ</label>
      <label id="offence_cat" class="text-info faruma">{{$hukum->offence_cat->offence_cat_dhi}}</label>
    </div>
    <div class="col-md-3">
      <label class="lbl">ޤަޟިއްޔާ ނަންބަރ</label>
      <label class="text-info faruma">{{$hukum->gaziyya_no}}</label>
    </div>
  </div>

  <div class="row">
    <div class="col-md-12">
      <label class="lbl">ޤަޒިއްޔާ ކޮޕީ</label>

        @if($hukum->gaziyya_copy == "no_file.png")
        <img style="margin-top:20px;float:right;width:150px;" src="{{ asset('storage/gaziyya_chit/'.$hukum->gaziyya_copy) }}" alt="">
        @else
        <a target="_blank" href="{{ asset('storage/gaziyya_chit/'.$hukum->document_copy) }}" ><img style="margin-top:20px;float:right;width:150px;" src="{{ asset('storage/gaziyya_chit/file.png') }}" alt=""></a>
        @endif
    </div>
  </div>


<br>
<br>
<br>

  <h3 style="text-align:center;"class="waheedh">ޙުކުމަށް މުއްދަތު އިތުރުކުރާ މަޢުލޫމާތު</h3>
  <hr class="style-one">

  <div class="row">
    <div class="col-md-2">
      <label class="lbl">އިތުރުކުރާ ދުވަސް</label>
      <label  style="text-align:center;" class="text-info faruma">{{$increment->days}}</label>
    </div>
    <div class="col-md-2">
      <label class="lbl">ތާރީޚް</label>
      <label style="text-align:center;" id="offence_cat" class="text-info faruma">{{$increment->increment_date}}</label>
    </div>
    <div class="col-md-8">
      <label class="lbl">އިތުރުކުރާ ސަބަބު</label>
      <label class="text-info faruma">{{$increment->increment_type->increment_type}}</label>
    </div>
  </div>
  <div class="row">
    <div class="col-md-12">
      <label class="lbl">އިތުރު ތަފްސީލް</label>
      <p style="font-size:19px; text-align:justify;margin-bottom:50px;"class="faruma">{{$increment->additional_detail}}</p>
    </div>
  </div>
  <div class="row">
    <div class="col-md-12">
      <label class="lbl">މުއްދަތު އިތުރުކުރާ ރިފަރެންސް ކޮޕީ / ލިޔުން</label>
      @if($hukum->document_copy == "no_file.png")
      <img style="margin-top:20px;float:right;width:150px;" src="{{ asset('storage/offence_increment/'.$increment->document_copy) }}" alt="">
      @else
      <a target="_blank" href="{{ asset('storage/offence_increment/'.$increment->document_copy) }}" ><img style="margin-top:20px;float:right;width:150px;" src="{{ asset('storage/offence_increment/file.png') }}" alt=""></a>
      @endif
    </div>
  </div>

<br><br>

  <form action="{{route('save.increment.approval',['id' => $increment->id])}}" method="post">{{ csrf_field() }}
  <div class="row">
    <div class="col-md-6">

    </div>
    <div class="col-md-4">
    </div>
    <div class="col-md-2">
      <label class="lbl">އެޕްރޫވަލް ސްޓޭޓަސް ނަންގަވާ</label>
      <select style="font-family: 'Raleway';text-align-last:center;" type="text" name="approval_status_id" value="">
        @foreach($approval_status as $status)
        @if($status->id == $increment->approval_status_id)
        <option style="line-height: 40px;font-family: 'Raleway';" selected value="{{$status->id}}">{{$status->approval_status}}</option>
        @else
        <option style="line-height: 40px;font-family: 'Raleway';" value="{{$status->id}}">{{$status->approval_status  }}</option>
        @endif
        @endforeach
      </select>
    </div>
  </div>
  <div class="row">
      <div class="col-md-3">
        <a href="{{route('offender.supervisor')}}"><button type="button" name="" value="" class="faruma btn" style="color:#fff;height:50px; width:100%; background:#E74C3C;font-size:19px;">
          ކެންސަލް
        </button></a>
      </div>
      <div class="col-md-3">
        <input type="submit" name="" value="މަޢުލޫމާތު ރައްކާކުރުމަށް" class="faruma btn" style="color:#fff;height:50px; width:100%; background:#28B463;font-size:19px;">
      </div>
  </div>
</form>
















</div>



@endsection
