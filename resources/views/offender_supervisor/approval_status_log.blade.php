@extends('layouts.myapp')
@section('content')
<div class="container">

  <div class="row">
    <div class="col-md-3">
      <img class="info-profile-photo" src="{{ asset('storage/prisoner/'.$hukum->prisoner->profile_path) }}">
    </div>
    <div class="col-md-5" style="top:40px;">
      <li style="margin:6px 2px 6px 2px; font-size:30px; font-family: 'raleway'; text-align:left;">{{$hukum->prisoner->full_name_eng}}</li>
      <li style="margin:6px 2px 6px 2px; font-size:20px; font-family: 'raleway'; text-align:left;">{{$hukum->prisoner->country->country_name_eng}}</li>
      <li style="margin:6px 2px 6px 2px; font-size:20px; font-family: 'raleway'; text-align:left;">{{$hukum->prisoner->atoll->atoll_name_eng}} . {{$hukum->prisoner->island->island_name_eng}}</li>
      <li style="margin:6px 2px 6px 2px; font-size:20px; font-family: 'raleway'; text-align:left;">{{$hukum->prisoner->address_eng}}</li>
    </div>
    <div class="col-md-4" style="top:40px;">
      <li style="margin:6px 2px 6px 2px; font-size:30px; text-align:right;" class="waheedh">{{$hukum->prisoner->full_name_dhi}}</li>
      <li style="margin:6px 2px 6px 2px; font-size:20px; text-align:right;" class="waheedh">{{$hukum->prisoner->country->country_name_dhi}}</li>
      <li style="margin:6px 2px 6px 2px; font-size:20px; text-align:right;" class="waheedh">{{$hukum->prisoner->atoll->atoll_name_dhi}} . {{$hukum->prisoner->island->island_name_dhi}}</li>
      <li style="margin:6px 2px 6px 2px; font-size:20px; text-align:right;" class="waheedh">{{$hukum->prisoner->address_dhi}}</li>
    </div>
  </div>



  <br><br>
  <h2 style="text-align:center;"class="waheedh">ޙުކުމުގެ މަޢުލޫމާތު</h2>
  <hr class="style-one">

  <h2 style="text-align:center" class="waheedh">{{$hukum->offence_type->offence_type_dhi}} | {{$hukum->sentence_type->sentence_type_dhi}}<h2>
    <hr class="style-one">

    <div class="row">
      <div class="col-md-3">
        <label class="lbl">ކަނޑައެޅި ނިޔާ</label>
        <label class="text-info faruma">{{$hukum->sentence_type->sentence_type_dhi}}</label>
      </div>
      <div class="col-md-3">
        <label class="lbl">ސާބިތުވި ކުށް</label>
        <label class="text-info faruma">{{$hukum->offence_type->offence_type_dhi}}</label>
      </div>
      <div class="col-md-3">
        <label class="lbl">ކުށުގެ ކެޓަގަރީ</label>
        <label id="offence_cat" class="text-info faruma">{{$hukum->offence_cat->offence_cat_dhi}}</label>
      </div>
      <div class="col-md-3">
        <label class="lbl">ޤަޟިއްޔާ ނަންބަރ</label>
        <label class="text-info faruma">{{$hukum->gaziyya_no}}</label>
      </div>
    </div>

    <br><br>
    <h2 style="text-align:center;"class="waheedh">އެޕްރޫވް ކުރާ މަޢުލޫމާތު</h2>
    <hr class="style-one">


    <form class="" action="{{route('save.approve.offence.status',['id' => $offence_status->id])}}" method="post">
      {{CSRF_field()}}

<div class="row">
  <div class="col-md-6">
    <label class="lbl">ލިޔުމުގެ ކޮޕީ</label>
    @if($offence_status->document_copy == "no_file.png")
    <img style="margin-top:7px;float:right;width:150px;" src="{{ asset('storage/status_log/'.$offence_status->document_copy) }}" alt="">
    @else
    <a target="_blank" href="{{ asset('storage/status_log/'.$offence_status->document_copy) }}" ><img style="margin-top:20px;float:right;width:150px;" src="{{ asset('storage/status_log/file.png') }}" alt=""></a>
    @endif

  </div>
    <div class="col-md-4">
      <label class="lbl">ސްޓޭޓަސް</label>
      <input class="faruma" style="text-align:right;" type="text" name="" value="{{$offence_status->offence_status->offence_status}}" readonly>
    </div>
    <div class="col-md-2">
      <label class="lbl">ޢަމަލުކުރާ ތާރީޚް</label>
      <input style="text-align:center;"type="text"  value="{{$offence_status->status_date}}" readonly>
    </div>
  </div>

  <div class="row">

      <label class="lbl" style="margin-bottom:25px;">ތަފްސީލް</label>
      <p class="faruma" style="margin-right:20px;line-height: 40px;text-align:justify;font-size:21px;"type="text" readonly>{{$offence_status->detail}}"</p>
  </div>

  <div class="row" style="margin-top:50px;">
    <div class="col-md-2" style="float:right;">
      <label class="lbl">އެޕްރޫވަލް</label>
      <select style="font-family: 'Raleway';text-align-last:center;" type="text" name="approval_status_id" value="">
        @foreach($approval_status as $status)
        @if($status->id == $offence_status->approval_status_id)
        <option style="line-height: 40px;font-family: 'Raleway';" selected value="{{$status->id}}">{{$status->approval_status}}</option>
        @else
        <option style="line-height: 40px;font-family: 'Raleway';" value="{{$status->id}}">{{$status->approval_status  }}</option>
        @endif
        @endforeach
      </select>
    </div>
  </div>



  <div class="row" style="margin-top:30px;">
      <div class="col-md-3">
        <a href="{{route('offender.supervisor')}}"><button type="button" name="" value="" class="faruma btn" style="color:#fff;height:50px; width:100%; background:#E74C3C;font-size:19px;">
          ކެންސަލް
        </button></a>
      </div>
      <div class="col-md-3">
        <input type="submit" name="" value="މަޢުލޫމާތު ރައްކާކުރުމަށް" class="faruma btn" style="color:#fff;height:50px; width:100%; background:#28B463;font-size:19px;">
      </div>
  </div>
</form>
















</div>



@endsection
