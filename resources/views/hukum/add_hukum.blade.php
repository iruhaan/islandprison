@extends('layouts.myapp')
@section('content')



<div class="container">

  <div class="row">
    <div class="col-md-3">
      <img class="info-profile-photo" src="{{ asset('storage/prisoner/'.$prisoner->profile_path) }}">
    </div>
    <div class="col-md-5" style="top:40px;">
      <li style="margin:6px 2px 6px 2px; font-size:30px; font-family: 'raleway'; text-align:left;">{{$prisoner->full_name}}</li>
      <li style="margin:6px 2px 6px 2px; font-size:20px; font-family: 'raleway'; text-align:left;">{{$prisoner->country->country_name}}</li>
      <li style="margin:6px 2px 6px 2px; font-size:20px; font-family: 'raleway'; text-align:left;">{{$prisoner->atoll->atoll_name}} . {{$prisoner->island->island_name}}</li>
      <li style="margin:6px 2px 6px 2px; font-size:20px; font-family: 'raleway'; text-align:left;">{{$prisoner->address}}</li>
    </div>

  </div>
<br>

  <h2 style="text-align:center" class="">Add Sentence Details<h2>
    <hr class="style-one">
<form method="post" action="{{route('save.hukum',['id' => $prisoner->id])}}" enctype="multipart/form-data">
  {{CSRF_field()}}
    <div class="row">

      <div class="col-md-3">
        <label for="" class="lbl faruma">Gategory</label>
        <select style="text-align-last:center;" id="offence_cat" class="" name="offence_cat_id" required>
          <option value="" selected disabled class="">Please Select</option>
          @foreach($offence_cats as $offence_cat)
            <option value="{{$offence_cat->id}}"  class="faruma">{{$offence_cat->offence_cat}}</option>
          @endforeach
        </select>
      </div>

      <div class="col-md-4">
        <label for="" class="lbl faruma">Sentence Title</label>

        <select style="text-align-last:center;" id="offence_type" class="faruma" name="offence_type_id" required>
          <option value="" selected disabled class="">Please Select</option>
          @foreach($offence_types as $offence_type)
            <option value="{{$offence_type->id}}"  class="">{{$offence_type->offence}}</option>
          @endforeach
        </select>
      </div>

      <div class="col-md-3">
        <label for="" class="lbl faruma">Court Order Number</label>
        <input style="text-transform: uppercase;text-align:center;"type="text" name="court_order_no" required value="" autocomplete="off">
      </div>

      <div class="col-md-2">
        <label for="" class="lbl faruma">Ordered Court</label>
        <select style="text-align-last:center;" class="faruma" name="court_name_id" required>
          <option value="" selected disabled class="faruma">Please Select</option>
          @foreach($court_names as $court_name)
            <option value="{{$court_name->id}}"  class="faruma">{{$court_name->court_name}}</option>
          @endforeach
        </select>
      </div>
    </div>

    <div class="row">
    <div class="col-md-3" id="implemented_date">
      <label for="" class="lbl faruma">Implimentation Start Date</label>
      <input style="text-align:center;"type="text" name="sentenced_implement_date" value="" class="datepicker" autocomplete="off">
    </div>

      <div class="col-md-3">
        <label for="" class="lbl faruma">Current Status</label>
        <select style="text-align-last:center;" id="offence_status_id" class="faruma" name="offence_status_id" required>
          <option value="" selected disabled class="faruma">Please Select</option>
          @foreach($offence_statuses as $offence_statuse)
            <option value="{{$offence_statuse->id}}"  class="faruma">{{$offence_statuse->offence_status}}</option>
          @endforeach
        </select>
      </div>

      <div class="col-md-1">
        <label for="" class="lbl faruma">Year(s)</label>
        <input id="year" style="text-align:center;"type="text" name="period_year" value="" class="generate_total_days" autocomplete="off">
      </div>

      <div class="col-md-1">
        <label for="" class="lbl faruma">Month(s)</label>
        <input id="month" style="text-align:center;"type="text" name="period_month" value="" class="generate_total_days" autocomplete="off">
      </div>

      <div class="col-md-1">
        <label for="" class="lbl faruma">Day(s)</label>
        <input id="day" style="text-align:center;"type="text" name="period_day" value="" class="generate_total_days" autocomplete="off">
      </div>

      <div class="col-md-3">
        <label for="" class="lbl faruma">Total Days in Duration</label>
        <input id="total_in_days" style="text-align:center;"type="text" name="total_period_days" value="" readonly>
      </div>

  </div>



  <div class="row">

  <div id="period">










  </div>




    <div class="row" style="margin-bottom:30px;">
      <div class="col-md-12">
        <label for="" class="lbl">Summery of Sentence</label>
        <textarea type="text" name="additional_detail" class="autosize"></textarea>
      </div>
    </div>

    <div class="row">
      <div class="col-md-4">
        <label for="" class="lbl">Court Order Copy</label>
        <input type="file" name="court_order_copy" value="" style="text-align:center;">
      </div>


      <div class="col-md-2">
        <label for="" class="lbl faruma">Sentence Date</label>
        <input type="text" name="sentenced_date" value="" class="datepicker" style="text-align:center;" required autocomplete="off">
      </div>
    </div>


    <div class="row">
      <div class="col-md-3">
        <label style="color:transparent;" for="save">save</label>
        <input style="font-size:18px;background:#239B56; height:40px; width:100%;"class="btn btn-primary" type="submit" name="save" value="Save">
      </div>
      <div class="col-md-3">
        <label style="color:transparent;" for="save">save</label>
        <input style="font-size:18px;background:#E74C3C; height:40px; width:100%;"class="btn btn-primary" type="reset" name="save" value="Cancel">
      </div>
    </div>



</form>







<script>
$('.generate_total_days').keyup(function (){
  //var d = document.getElementById("day").value
  //var m = document.getElementById("month").value
  //var y = document.getElementById("year").value
  var day = Number(document.getElementById("day").value);
  var month = Number(document.getElementById("month").value);
  var year = Number(document.getElementById("year").value);

  var total_days_in_month = month * 30;
  var total_days_in_year = year * 365;
  var total_in_days = day + total_days_in_month + total_days_in_year;

  document.getElementById("total_in_days").value = total_in_days;

})
</script>












<script type="text/javascript">
autosize();
function autosize(){
    var text = $('.autosize');

    text.each(function(){
        $(this).attr('rows',5);
        resize($(this));
    });

    text.on('input', function(){
        resize($(this));
    });

    function resize ($text) {
        $text.css('height', 'auto');
      $text.css('width', '100%');
        $text.css('height', $text[0].scrollHeight+'px');
    }
}
</script>
  <!-- <script>

  tinymce.init({
    selector: 'textarea#editor',
    width: '100%',
    directionality: 'rtl',
    class : "faruma",
    skin: 'oxide-dark',
    branding: false,

    link_assume_external_targets: false,
    link_context_toolbar: false,
    link_default_protocol: '',
    link_title: false,
    link_quicklink: false,
    rel_list: false,
    target_list: false,
    relative_urls : false,
    remove_script_host : true,
    allow_script_urls: false,
    convert_urls: false,
    anchor_bottom: false,
    anchor_top: false,

    /* plugin */
      plugins: [
        "link code advlist autolink link image lists print preview hr anchor pagebreak autoresize",
        "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime nonbreaking",
        "save table contextmenu directionality emoticons template paste textcolor mention'",
      ],

      /* toolbar */
      toolbar: "link code | insertfile undo redo | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | forecolor backcolor emoticons",
      /* style */
      style_formats: [
        {title: "Headers", items: [
          {title: "Header 1", format: "h1"},
          {title: "Header 2", format: "h2"},
          {title: "Header 3", format: "h3"},
          {title: "Header 4", format: "h4"},
          {title: "Header 5", format: "h5"},
          {title: "Header 6", format: "h6"}
        ]},
        {title: "Inline", items: [
          {title: "Bold", icon: "bold", format: "bold", classes: 'faruma'},
          {title: "Italic", icon: "italic", format: "italic"},
          {title: "Underline", icon: "underline", format: "underline"},
          {title: "Strikethrough", icon: "strikethrough", format: "strikethrough"},
          {title: "Superscript", icon: "superscript", format: "superscript"},
          {title: "Subscript", icon: "subscript", format: "subscript"},
          {title: "Code", icon: "code", format: "p",remove: 'all'}
        ]},
        {title: "Blocks", items: [
          {title: "Paragraph", format: "p"},
          {title: "Blockquote", format: "blockquote"},
          {title: "Div", format: "div"},
          {title: "Pre", format: "pre",remove: 'all'}
        ]},
        {title: "Alignment", items: [
          {title: "Left", icon: "alignleft", format: "alignleft"},
          {title: "Center", icon: "aligncenter", format: "aligncenter"},
          {title: "Right", icon: "alignright", format: "alignright"},
          {title: "Justify", icon: "alignjustify", format: "alignjustify"}
        ]}],

    setup: function (editor) {
      editor.on('keypress', function (e) {
      thaanaKeyboard.value = '';
      thaanaKeyboard.handleKey(e);
      editor.insertContent(thaanaKeyboard.value);

  });


  },
       content_css : '/css/editor.css',
       menubar: true,
  });
  </script> -->



    <script>
        $(function() {
            $('select[id=offence_cat]').change(function() {
                var url = '/offencelist/' + $(this).val();
                $.get(url, function(data) {
                    var select = $('select[id=offence_type]');
                    $('select[id="offence_type"]').empty();

                            $('select[id="offence_type"]').append(
                                        '<option value="" disabled selected> Please Select </option>'
                                        )
                    $.each(data,function(key, value) {
                        select.append('<option value=' + value.id + '>' + value.offence + '</option>');
                    });
                });
            });
        });
    </script>
    <script>
    $(document).ready(function(){
        $('select[id=offence_cat]').on('change', function() {
          if ( this.value == '1')
          //.....................^.......
          {
            $("#drug_offence_div").fadeTo(1000, 1.0);
            $('select[id="drug_offence"]').append(
                        '<option value="" selected> މައްސަލަ ނަންގަވާ </option>'
                        )
          }
          else
          {
            $("#drug_offence_div").fadeTo(1000, 0);
            $('select[id="drug_offence"]').append(
                        '<option value="" selected> މައްސަލަ ނަންގަވާ </option>'
                        )
          }
        });
    });
    </script>
    <script>
    $(document).ready(function(){
        $('select[id=offence_status_id]').on('change', function() {
          if ( this.value == '2')
          //.....................^.......
          {
            $("#implemented_date").fadeTo(1000, 0);
            $("#implemented_time").fadeTo(1000, 0);
            $('input[name="sentenced_implement_date"]').val("");
            $('input[name="sentenced_implement_time"]').val("");
          }
          else
          {
            $("#implemented_date").fadeTo(1000, 1.0);
            $("#implemented_time").fadeTo(1000, 1.0);
          }
        });
    });
    </script>
    <script>
    $(document).ready(function(){
        $('select[id=sentence_type]').on('change', function() {
          if ( this.value == '2')
          //.....................^.......
          {
            $("#period").fadeTo(1000, 0);
            $("#year").val("0");
            $("#month").val("0");
            $("#day").val("0");
            $("#total_in_days").val("0");
          }
          else
          {
            $("#year").val("0");
            $("#month").val("0");
            $("#day").val("0");
            $("#total_in_days").val("0");
            $("#period").fadeTo(1000, 1.0);
          }
        });
    });
    </script>

    <script type="text/javascript">
    function loadpage() {
      $("#drug_offence_div").fadeTo(1000, 0);
      $('select[id="drug_offence"]').append(
                  '<option value="" selected> މައްސަލަ ނަންގަވާ </option>'
                )
              };

              onload.window = loadpage();
    </script>


</div>
@endsection
