@extends('layouts.myapp')
@section('content')



<div class="container">
  <div class="row">
    <div class="col-md-3">
      <img class="info-profile-photo" src="{{ asset('storage/prisoner/'.$hukum->prisoner->profile_path) }}">
    </div>
    <div class="col-md-5" style="top:40px;">
      <li style="margin:6px 2px 6px 2px; font-size:30px; font-family: 'raleway'; text-align:left;">{{$hukum->prisoner->full_name}}</li>
      <li style="margin:6px 2px 6px 2px; font-size:20px; font-family: 'raleway'; text-align:left;">{{$hukum->prisoner->country->country_name}}</li>
      <li style="margin:6px 2px 6px 2px; font-size:20px; font-family: 'raleway'; text-align:left;">{{$hukum->prisoner->atoll->atoll_name}} . {{$hukum->prisoner->island->island_name}}</li>
      <li style="margin:6px 2px 6px 2px; font-size:20px; font-family: 'raleway'; text-align:left;">{{$hukum->prisoner->address}}</li>
    </div>
  </div>

  <br><br>
  <h2 style="text-align:center" class="">{{$hukum->offence_type->offence}}<h2>
    <hr class="style-one">

    <div class="row">
      <div class="col-md-4">
        <label class="lbl">Sentence Title</label>
        <label class="text-info faruma">{{$hukum->offence_type->offence}}</label>
      </div>
      <div class="col-md-3">
        <label class="lbl">Category</label>
        <label id="offence_cat" class="text-info faruma">{{$hukum->offence_cat->offence_cat}}</label>
      </div>
      <div class="col-md-3">
        <label class="lbl">Court Order No.</label>
        <label class="text-info faruma">{{$hukum->court_order_no}}</label>
      </div>
      <div class="col-md-2">
        <label class="lbl">Ordered Court</label>
        <label class="text-info faruma">{{$hukum->court_name->court_name}}</label>
      </div>
    </div>

<br>
<br>
  <h2 style="text-align:center" class="waheedh">Enter Log Detail<h2>
    <hr class="style-one">

    <form class="" action="{{route('save.offence.status',['id' => $hukum->id])}}" method="post" enctype="multipart/form-data">
      {{CSRF_field()}}


    <!-- auto calculator -->

      <div class="row">


        <div class="col-md-7">
          <label class="lbl">Status</label>
          <select class="faruma" name="offence_status_id" required>
            <option disabled selected value="">Please Select</option>
            @foreach($offence_status as $status)
            <option value="{{$status->id}}">{{$status->offence_status}}</option>
            @endforeach
          </select>
        </div>

        <div class="col-md-2">
          <label class="lbl">Action Date</label>
          <input type="text" value="" name="status_date" style="text-align:center" class="datepicker" autocomplete="off">
        </div>
      </div>

    <div class="row">
      <label class="lbl">Additional Detail</label>
      <textarea type="text" name="detail" class="faruma thaanaKeyboardInput autosize"></textarea>
    </div>

    <div class="row">
      <div class="col-md-3">
        <input type="submit" name="" value="Save" class="faruma btn" style="color:#fff;height:50px; width:100%; background:#28B463;font-size:19px;">
      </div>
      <div class="col-md-3">
        <a href="{{route('open.hukum',['id' => $hukum->id])}}"><button type="button" name="" value="އެޕްރޫވަލަށް ފޮނުވުމަށް" class="faruma btn" style="color:#fff;height:50px; width:100%; background:#E74C3C;font-size:19px;">
        Cancel
        </button></a>
      </div>
      <div class="col-md-3">
      </div>
      <!-- <div class="col-md-3">
        <a href="">
          <button type="button" name="" value="" class="faruma btn" style="color:#fff;height:50px; width:100%; background:#BF1200;font-size:19px;">
          އުނިކުރުމަށް
        </button></a>
      </div> -->

    </div>

</form>







    <script type="text/javascript">
    autosize();
    function autosize(){
        var text = $('.autosize');

        text.each(function(){
            $(this).attr('rows',5);
            resize($(this));
        });

        text.on('input', function(){
            resize($(this));
        });

        function resize ($text) {
            $text.css('height', 'auto');
          $text.css('width', '100%');
            $text.css('height', $text[0].scrollHeight+'px');
        }
    }
    </script>


</div>
@endsection
