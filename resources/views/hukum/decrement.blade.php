@extends('layouts.report-header')
@section('content')

 <style media="print">


 @page {
   size: A5 landscape;
   margin: 0.6mm 0.6mm 0.6mm 0.6mm;
 }
 #fill{
   zoom: 75%;
 }

 /* html, body {
    background: #FFF;
    overflow:visible;
    transform: scale(0.9);
    float:top;
} */

 }

 </style>



<div id="fill" class="container-fluid" style="background-color:#fff;">
<button onclick="window.print();" style="position:fixed;float:right;"class="print-button"><span class="print-icon"></span></button>
<img style="display:block;margin-top:20px;margin-right:auto; margin-left:auto;width:9%;" src="/images/brand.png" alt="letter-head">



<h2 class="" style="color:#000; text-align:center;">Sentence Decrement</h2>
<hr class="hr-print">

<!-- prisoner information -->
<div class="row">
  <div class="col-print-3">
    <div style="margin-top:50px; margin-left:20px;">
      <!-- {{QrCode::size(60)->generate($decrement->offence->prisoner->file_number)}} -->
      <img src="data:image/png;base64,{{DNS1D::getBarcodePNG($decrement->offence->prisoner->file_number, 'C39')}}" alt="barcode" />

    </div>
  </div>
  <div class="col-print-7 faruma" >
    <ul class="faruma">
      <li>File Number : <b>{{$decrement->offence->prisoner->file_number}}</b></li>
      <li>NID : <b>{{$decrement->offence->prisoner->id_passportNo}}</b></li>
      <li>Full Name : <b>{{$decrement->offence->prisoner->full_name}}</b></li>
      <li>Address : <b>{{$decrement->offence->prisoner->atoll_name}} . {{$decrement->offence->prisoner->island_name}} / {{$decrement->offence->prisoner->address}} , {{$decrement->offence->prisoner->country->country_name}}</b></li>
    </ul>
  </div>
  <div class="col-print-2">
    <img src="{{ asset('storage/prisoner/'.$decrement->offence->prisoner->profile_path) }}" class="image--cover" style="float: center;">
  </div>
</div>
<hr class="hr-print">


<!-- hukum detail -->
<div class="row">

<!-- Increment detail -->

  <div class="col-print-6 " >
    <ul class="">
      <li>Date : <b>{{$decrement->decrement_date}}</b></li>
      <li>Reason : <b>{{$decrement->decrement_type->increment_type}}</b></li>
      <li>Deducted Days : <b>{{$decrement->days}} Days </b></li>
    </ul>
  </div>

  <div class="col-print-6" >
    <ul class="">
      <li>Court Order No : <b>{{$decrement->offence->court_order_no}}</b></li>
      <li>Duration : <b> {{$decrement->offence->period_year}} Y {{$decrement->offence->period_month }} M {{$decrement->offence->period_day}} D </b></li>
      <li>Offence Title : <b>{{$decrement->offence->offence_type->offence}}</b></li>
      <li>Offence Category : <b>{{$decrement->offence->offence_cat->offence_cat}}</b></li>
    </ul>
  </div>
</div>

<div class="row">
  <div class="col-print-12" style="padding-right:50px;padding-left:50px;">
    <p>{{$decrement->additional_detail}}</p>
  </div>
</div>
<!--
<script type="text/javascript">
let perpx = 0.7/1300;
let ratio = perpx * window.screen.width;
document.getElementById('fill').style.transform = "scale(" + ratio + ")";
</script> -->




</div>

@stop
