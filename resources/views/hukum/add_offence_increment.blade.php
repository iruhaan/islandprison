@extends('layouts.myapp')
@section('content')

<div class="container">
<br>
<br>
  <h2 style="text-align:center" class="waheedh">Sentence Increment<h2>
    <hr class="style-one">
    <form class="" action="{{route('save.offence.increment',['id' => $id])}}" method="post" enctype="multipart/form-data">
      {{CSRF_field()}}

    <!-- auto calculator -->
      <div class="row">
        <div class="col-md-3">
          <label class="lbl">Start Date</label>
          <input  autocomplete="off" id="s_date" type="text" name="start_date" style="text-align:center;" class="datepicker increment">
        </div>
        <div class="col-md-3">
          <label class="lbl">Till Date</label>
          <input  autocomplete="off" id="e_date" type="text" name="end_date" style="text-align:center;" class="datepicker increment">
        </div>
      </div>
      <hr class="style-one">

    <div class="row">
      <div class="col-md-2">
        <label class="lbl">Action Date</label>
        <input class="datepicker" style="text-align:center; "type="text" name="increment_date" required autocomplete="off">
      </div>

      <div class="col-md-5">
        <label class="lbl">Induct Reason</label>
        <select class="faruma" name="increment_type_id" required>
          <option disabled selected value="">Please Select</option>
          @foreach($increment_types as $type)
          <option value="{{$type->id}}">{{$type->increment_type}}</option>
          @endforeach
        </select>
      </div>

      <div class="col-md-2">
        <label class="lbl">Inducted Days</label>
        <input id="tdays" style="text-align:center;" type="text" name="days" value="" required required autocomplete="off">
      </div>

      <div class="col-md-3">
        <label class="lbl">Reference Document Copy</label>
        <input type="file" name="document_copy" value="">
      </div>
    </div>
    <div class="row">
      <label class="lbl">Additional Info</label>
      <textarea type="text" name="additional_detail" class="faruma thaanaKeyboardInput autosize"></textarea>
    </div>

    <div class="row">
      <div class="col-md-3">
        <input type="submit" name="" value="Save" class="faruma btn" style="color:#fff;height:50px; width:100%; background:#28B463;font-size:19px;">
      </div>
      <div class="col-md-3">
        <button type="button" name="" value="އެޕްރޫވަލަށް ފޮނުވުމަށް" class="faruma btn" style="color:#fff;height:50px; width:100%; background:#E74C3C;font-size:19px;">
        Cancel
        </button>
      </div>
    </div>
</form>

    <script type="text/javascript">
    $('.increment').on('change', function() {
      var edate = document.getElementById('e_date').value;
      var sdate = document.getElementById('s_date').value;
      var start = new Date(sdate);
      var end = new Date(edate);
      var diff =  Math.floor(( Date.parse(end) - Date.parse(start) ) / 86400000);
      document.getElementById("tdays").value = diff + 1;
      });
    </script>

    <script type="text/javascript">
    autosize();
    function autosize(){
        var text = $('.autosize');

        text.each(function(){
            $(this).attr('rows',5);
            resize($(this));
        });

        text.on('input', function(){
            resize($(this));
        });

        function resize ($text) {
            $text.css('height', 'auto');
          $text.css('width', '100%');
            $text.css('height', $text[0].scrollHeight+'px');
        }
    }
    </script>

</div>
@endsection
