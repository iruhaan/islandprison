@extends('layouts.myapp')
@section('content')



<div class="container">
  <div class="row">
    <div class="col-md-3">
      <img class="info-profile-photo" src="{{ asset('storage/prisoner/'.$hukum->prisoner->profile_path) }}">
    </div>
    <div class="col-md-5" style="top:40px;">
      <li style="margin:6px 2px 6px 2px; font-size:30px; font-family: 'raleway'; text-align:left;">{{$hukum->prisoner->full_name}}</li>
      <li style="margin:6px 2px 6px 2px; font-size:20px; font-family: 'raleway'; text-align:left;">{{$hukum->prisoner->country->country_name}}</li>
      <li style="margin:6px 2px 6px 2px; font-size:20px; font-family: 'raleway'; text-align:left;">{{$hukum->prisoner->atoll->atoll_name}} . {{$hukum->prisoner->island->island_name}}</li>
      <li style="margin:6px 2px 6px 2px; font-size:20px; font-family: 'raleway'; text-align:left;">{{$hukum->prisoner->address}}</li>
    </div>
  </div>

  <section class="outline">
  <h2 style="text-align:center" class="waheedh">{{$hukum->offence_type->offence}}<h2>
    <hr class="style-one">

    <div class="row">
      <div class="col-md-5">
        <label class="lbl">Offence Type</label>
        <label class="text-info faruma">{{$hukum->offence_type->offence}}</label>
      </div>
      <div class="col-md-4">
        <label class="lbl">Offence Category</label>
        <label id="offence_cat" class="text-info faruma">{{$hukum->offence_cat->offence_cat}}</label>
      </div>
      <div class="col-md-3">
        <label class="lbl">Court Order No.</label>
        <label class="text-info faruma">{{$hukum->court_order_no}}</label>
      </div>
    </div>
  </section>

  <section class="outline">
  <h2 style="text-align:center" class="waheedh">Edit Log<h2>
    <hr class="style-one">

    <form class="" action="{{route('update.offence.status',['id' => $offence_status->id])}}" method="post" enctype="multipart/form-data">
      {{CSRF_field()}}
      <div class="row">
        <div class="col-md-10">
          <label class="lbl">Status</label>
          <select class="faruma" name="offence_status_id" required>
            @foreach($status as $status)
            @if($status->id == $offence_status->offence_status_id)
            <option selected value="{{$status->id}}">{{$status->offence_status}}</option>
            @else
            <option value="{{$status->id}}">{{$status->offence_status}}</option>
            @endif
            @endforeach
          </select>
        </div>

        <div class="col-md-2">
          <label class="lbl">Date</label>
          <input type="text" value="{{$offence_status->status_date}}" name="status_date" style="text-align:center" class="datepicker" autocomplete="off">
        </div>
      </div>

      <div class="row">
      <label class="lbl">Additional Detail</label>
      <textarea type="text" name="detail" class="faruma thaanaKeyboardInput autosize">{{$offence_status->detail}}</textarea>
      </div>

      <div class="row">
        <div class="col-md-3">
          <input type="submit" name="" value="Update" class="faruma btn" style="color:#fff;height:50px; width:100%; background:#28B463;font-size:19px;">
        </div>
        <div class="col-md-3">
          <a href="{{route('open.hukum',['id' => $offence_status->offence_id])}}"><button type="button" name="" value="" class="faruma btn" style="color:#fff;height:50px; width:100%; background:#E74C3C;font-size:19px;">
          Cancel
          </button></a>
        </div>
        <div class="col-md-3">
        </div>
        <div class="col-md-3">
          <a href="{{route('delete.offence.status',['id' => $offence_status->id])}}"><button type="button" name="" value="" class="faruma btn" style="color:#fff;height:50px; width:100%; background:#BF1200;font-size:19px;">
            Remove Record
          </button></a>
        </div>
      </div>
      </form>
    </section>












    <script type="text/javascript">
    autosize();
    function autosize(){
        var text = $('.autosize');

        text.each(function(){
            $(this).attr('rows',5);
            resize($(this));
        });

        text.on('input', function(){
            resize($(this));
        });

        function resize ($text) {
            $text.css('height', 'auto');
          $text.css('width', '100%');
            $text.css('height', $text[0].scrollHeight+'px');
        }
    }
    </script>


</div>
@endsection
