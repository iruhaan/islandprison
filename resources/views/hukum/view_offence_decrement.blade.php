@extends('layouts.myapp')
@section('content')



<div class="container">
  <div class="row">
    <div class="col-md-3">
      <img class="info-profile-photo" src="{{ asset('storage/prisoner/'.$hukum->prisoner->profile_path) }}">
    </div>
    <div class="col-md-5" style="top:40px;">
      <li style="margin:6px 2px 6px 2px; font-size:30px; font-family: 'raleway'; text-align:left;">{{$hukum->prisoner->full_name_eng}}</li>
      <li style="margin:6px 2px 6px 2px; font-size:20px; font-family: 'raleway'; text-align:left;">{{$hukum->prisoner->country->country_name_eng}}</li>
      <li style="margin:6px 2px 6px 2px; font-size:20px; font-family: 'raleway'; text-align:left;">{{$hukum->prisoner->atoll->atoll_name_eng}} . {{$hukum->prisoner->island->island_name_eng}}</li>
      <li style="margin:6px 2px 6px 2px; font-size:20px; font-family: 'raleway'; text-align:left;">{{$hukum->prisoner->address_eng}}</li>
    </div>
    <div class="col-md-4" style="top:40px;">
      <li style="margin:6px 2px 6px 2px; font-size:30px; text-align:right;" class="waheedh">{{$hukum->prisoner->full_name_dhi}}</li>
      <li style="margin:6px 2px 6px 2px; font-size:20px; text-align:right;" class="waheedh">{{$hukum->prisoner->country->country_name_dhi}}</li>
      <li style="margin:6px 2px 6px 2px; font-size:20px; text-align:right;" class="waheedh">{{$hukum->prisoner->atoll->atoll_name_dhi}} . {{$hukum->prisoner->island->island_name_dhi}}</li>
      <li style="margin:6px 2px 6px 2px; font-size:20px; text-align:right;" class="waheedh">{{$hukum->prisoner->address_dhi}}</li>
    </div>
  </div>


  <br><br>
  <h2 style="text-align:center" class="waheedh">{{$hukum->offence_type->offence_type_dhi}} | {{$hukum->sentence_type->sentence_type_dhi}}<h2>
    <hr class="style-one">

    <div class="row">
      <div class="col-md-3">
        <label class="lbl">ކަނޑައެޅި ނިޔާ</label>
        <label class="text-info faruma">{{$hukum->sentence_type->sentence_type_dhi}}</label>
      </div>
      <div class="col-md-3">
        <label class="lbl">ސާބިތުވި ކުށް</label>
        <label class="text-info faruma">{{$hukum->offence_type->offence_type_dhi}}</label>
      </div>
      <div class="col-md-3">
        <label class="lbl">ކުށުގެ ކެޓަގަރީ</label>
        <label id="offence_cat" class="text-info faruma">{{$hukum->offence_cat->offence_cat_dhi}}</label>
      </div>
      <div class="col-md-3">
        <label class="lbl">ޤަޟިއްޔާ ނަންބަރ</label>
        <label class="text-info faruma">{{$hukum->gaziyya_no}}</label>
      </div>
    </div>

<br>
<br>
  <h2 style="text-align:center" class="waheedh">ޙުކުމް މުއްދަތު އުނިކުރުން<h2>
    <hr class="style-one">

    <form class="" action="{{route('update.offence.decrement',['id' => $offence_decrement->id])}}" method="post" enctype="multipart/form-data">
      {{CSRF_field()}}


    <!-- auto calculator -->

      <div class="row">


        <div class="col-md-2">
          <label class="lbl">ޕާރސަންޓޭޖް</label>
          <input value="{{$offence_decrement->percentage}}"autocomplete="off" id="percentage_num"  type="text" name="percentage" style="text-align:center;" class="percentage">
        </div>

        <div class="col-md-4">
          <label class="lbl">މި ރެކޯޑް އެންޓަރ ކުރި ތާރީޚާ ހަމައަށް ޙުކުމުގެ ބާކީ އޮތް ދުވަސް</label>
          <input value="{{$offence_decrement->remain_days}}" autocomplete="off" id="remain_days"  type="text" name="remain_days" style="width:150px;text-align:center;" class="percentage">
        </div>
        <div class="col-md-2">

        </div>
        <div class="col-md-2">
          <label class="lbl">ނިންމާ ތާރީޚް</label>
          <input value="{{$offence_decrement->end_date}}" autocomplete="off" id="e_date"  type="text" name="end_date" style="text-align:center;" class="datepicker increment">
        </div>
        <div class="col-md-2">
          <label class="lbl">ދުވަސް ގުނަން ފަށާ ތާރީޚް</label>
          <input value="{{$offence_decrement->start_date}}" autocomplete="off" id="s_date" type="text" name="start_date" style="text-align:center;" class="datepicker increment">
        </div>
      </div>
      <label class="faruma" style="color:#E74C3C;">މިޙުކުމުގެ ތަންފީޚް ކުރަންޖެހޭ ބާކީ ދުވަހުގެ އަދަދު އަންނާނީ މި ރެކޯޑް އެންޓަރ ކުރާ ތާރީޚް ޖެއްސެވުމުންނެވެ. އެގޮތުން އަންނަ ޖުމްލަ ދުވަހަކީ އެ ތާރީޚާހަމައަށް ޖުމްލަ ތަންފީޛް ނުކޮށް ހުރި ދުވަހެވެ.</label>
      <hr class="style-one">




    <div class="row">
      <div class="col-md-3">
        <label class="lbl">ރިފަރެންސް ކަރުދާހުގެ ކޮޕީ</label>
        <input type="file" name="document_copy" value="{{$offence_decrement->document_copy}}">
      </div>
      <div class="col-md-2">
        <label class="lbl">އިތުރު ކުރި ދުވަސް</label>
        <input value="{{$offence_decrement->days}}" id="tdays" style="text-align:center;" type="text" name="days" value="" required required autocomplete="off">
      </div>
      <div class="col-md-5">
        <label class="lbl">މުއްދަތު އިތުރުކުރާ ބާވަތް</label>
        <select class="faruma" name="decrement_type_id" required>
          <option disabled selected value="">ނަންގަވާ</option>
          @foreach($decrement_types as $type)
          @if($offence_decrement->decrement_type_id == $type->id)
          <option selected value="{{$type->id}}">{{$type->decrement_type}}</option>
          @else
          <option value="{{$type->id}}">{{$type->decrement_type}}</option>
          @endif
          @endforeach
        </select>
      </div>
      <div class="col-md-2">
        <label class="lbl">ތާރީޚް</label>
        <input value="{{$offence_decrement->decrement_date}}" id="decrement_date" class="percentage datepicker" style="text-align:center; "type="text" name="decrement_date" required autocomplete="off">
      </div>
    </div>
    <div class="row">
      <label class="lbl">އިތުރު ތަފްސީލް</label>
      <textarea type="text" name="additional_detail" class="faruma thaanaKeyboardInput autosize">{{$offence_decrement->additional_detail}}</textarea>
    </div>

    <div class="row">
      <div class="col-md-3">
        <input type="submit" name="" value="އެޕްރޫވަލަށް ފޮނުވުމަށް" class="faruma btn" style="color:#fff;height:50px; width:100%; background:#28B463;font-size:19px;">
      </div>
      <div class="col-md-3">
        <a href="{{route('open.hukum',['id' => $offence_decrement->offence_id])}}"><button type="button" name="" value="އެޕްރޫވަލަށް ފޮނުވުމަށް" class="faruma btn" style="color:#fff;height:50px; width:100%; background:#E74C3C;font-size:19px;">
          ކެންސަލް
        </button></a>
      </div>
      <div class="col-md-3">
      </div>
      <div class="col-md-3">
        <a href="{{route('delete.offence.decrement',['id' => $offence_decrement->id])}}">
          <button type="button" name="" value="" class="faruma btn" style="color:#fff;height:50px; width:100%; background:#BF1200;font-size:19px;">
          އުނިކުރުމަށް
        </button></a>
      </div>

    </div>

</form>











    <script type="text/javascript">
    $('.increment').on('change', function() {

      var edate = document.getElementById('e_date').value;
      var sdate = document.getElementById('s_date').value;

      var diff =  Math.floor(( Date.parse(edate) - Date.parse(sdate) ) / 86400000);
      document.getElementById("tdays").value = diff + 1;

      });
    </script>

    <script type="text/javascript">
      $('.percentage').on('change', function() {
        var sdate = document.getElementById('s_date').value;
        if (sdate == "") {
        var decrement_date = document.getElementById('decrement_date').value;
        var expire_date = "<?php echo $expire_date ?>";

        var date1, date2;
        date1 = new Date(decrement_date);
        date2 = new Date(expire_date);

        // get total seconds between two dates
        var res = Math.abs(date1 - date2) / 1000;
        var days = Math.floor(res / 86400);

        var per = document.getElementById('percentage_num').value;
        var day = days * per /100;
        document.getElementById('remain_days').value = days;
        document.getElementById("tdays").value = Math.round(day);
      }else{
        $('.increment').on('change', function() {
          document.getElementById("percentage").value = "";
          document.getElementById("remain_days").value = "";
          var edate = document.getElementById('e_date').value;
          var sdate = document.getElementById('s_date').value;


          var start = new Date(sdate);
          var end = new Date(edate);


          var diff =  Math.floor(( Date.parse(end) - Date.parse(start) ) / 86400000);
          document.getElementById("tdays").value = diff + 1;


          });
      }
      });


    </script>





    <script type="text/javascript">
    autosize();
    function autosize(){
        var text = $('.autosize');

        text.each(function(){
            $(this).attr('rows',5);
            resize($(this));
        });

        text.on('input', function(){
            resize($(this));
        });

        function resize ($text) {
            $text.css('height', 'auto');
          $text.css('width', '100%');
            $text.css('height', $text[0].scrollHeight+'px');
        }
    }
    </script>


</div>
@endsection
