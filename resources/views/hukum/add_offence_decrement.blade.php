@extends('layouts.myapp')
@section('content')



<div class="container">
<br>
<br>
  <h2 style="text-align:center" class="waheedh">Sentence Decrement<h2>
    <hr class="style-one">

    <form class="" action="{{route('save.offence.decrement',['id' => $id])}}" method="post" enctype="multipart/form-data">
      {{CSRF_field()}}


    <!-- auto calculator -->

      <div class="row">

        <div class="col-md-2">
          <label class="lbl">Day Start<br>Date</label>
          <input autocomplete="off" id="s_date" type="text" name="start_date" style="text-align:center;" class="datepicker increment">
        </div>
        <div class="col-md-2">
          <label class="lbl">Till<br>Date</label>
          <input autocomplete="off" id="e_date"  type="text" name="end_date" style="text-align:center;" class="datepicker increment">
        </div>

        <div class="col-md-2">
          <label class="lbl">Pardon Percentage</label>
          <input autocomplete="off" id="percentage_num"  type="text" name="percentage" style="text-align:center;" class="percentage">
        </div>

        <div class="col-md-4">
          <label class="lbl">Remaining Days to be<br>serve till today</label>
          <input autocomplete="off" id="remain_days"  type="text" name="remain_days" style="width:150px;text-align:center;" class="percentage">
        </div>
        <div class="col-md-2">

        </div>

      </div>
      <hr class="style-one">


    <div class="row">
      <div class="col-md-2">
        <label class="lbl">Date</label>
        <input id="decrement_date" class="percentage datepicker" style="text-align:center; "type="text" name="decrement_date" required autocomplete="off">
      </div>

      <div class="col-md-5">
        <label class="lbl">Decrement Reason</label>
        <select class="faruma" name="decrement_type_id" required>
          <option disabled selected value="">Please Select</option>
          @foreach($decrement_types as $type)
          <option value="{{$type->id}}">{{$type->decrement_type}}</option>
          @endforeach
        </select>
      </div>

      <div class="col-md-2">
        <label class="lbl">Deduction Days</label>
        <input id="tdays" style="text-align:center;" type="text" name="days" value="" required required autocomplete="off">
      </div>

      <div class="col-md-3">
        <label class="lbl">Reference Document | Optional</label>
        <input type="file" name="document_copy" value="">
      </div>

    </div>

    <div class="row">
      <label class="lbl">Additional Detail</label>
      <textarea type="text" name="additional_detail" class="faruma thaanaKeyboardInput autosize"></textarea>
    </div>

    <div class="row">
      <div class="col-md-3">
        <input type="submit" name="" value="Save" class="faruma btn" style="color:#fff;height:50px; width:100%; background:#28B463;font-size:19px;">
      </div>
      <div class="col-md-3">
        <button type="button" name="" value="އެޕްރޫވަލަށް ފޮނުވުމަށް" class="faruma btn" style="color:#fff;height:50px; width:100%; background:#E74C3C;font-size:19px;">
          Cancel
        </button>
      </div>
    </div>

</form>











    <script type="text/javascript">
    $('.increment').on('change', function() {

      var edate = document.getElementById('e_date').value;
      var sdate = document.getElementById('s_date').value;

      var s = sdate.split("-");
      var e = edate.split("-");

      var start = new Date(s[2], s[1] - 1, s[0]);
      var end = new Date(e[2], e[1] - 1, e[0]);


      var diff =  Math.floor(( Date.parse(end) - Date.parse(start) ) / 86400000);
      document.getElementById("tdays").value = diff + 1;



      });
    </script>

    <script type="text/javascript">
      $('.percentage').on('change', function() {
        var sdate = document.getElementById('s_date').value;
        if (sdate === "") {
        var decrement_date = document.getElementById('decrement_date').value;
        var expire_date = "<?php echo $expire_date ?>";
        var dec = new Date(expire_date);


        var date1, date2;
        date1 = new Date(Date.parse(dec));
        date2 = new Date(expire_date);

        // get total seconds between two dates
        var res = Math.abs(date1 - date2) / 1000;
        var days = Math.floor(res / 86400);

        var per = document.getElementById('percentage_num').value;
        var day = days * per /100;
        document.getElementById('remain_days').value = days;
        document.getElementById("tdays").value = Math.round(day);
      }else{
        $('.increment').on('change', function() {
          document.getElementById("percentage").value = "";
          document.getElementById("remain_days").value = "";
          var edate = document.getElementById('e_date').value;
          var sdate = document.getElementById('s_date').value;

          var s = sdate.split("-");
          var e = edate.split("-");

          var start = new Date(s[2], s[1] - 1, s[0]);
          var end = new Date(e[2], e[1] - 1, e[0]);


          var diff =  Math.floor(( Date.parse(end) - Date.parse(start) ) / 86400000);
          document.getElementById("tdays").value = diff + 1;


          });
      }
      });


    </script>





    <script type="text/javascript">
    autosize();
    function autosize(){
        var text = $('.autosize');

        text.each(function(){
            $(this).attr('rows',5);
            resize($(this));
        });

        text.on('input', function(){
            resize($(this));
        });

        function resize ($text) {
            $text.css('height', 'auto');
          $text.css('width', '100%');
            $text.css('height', $text[0].scrollHeight+'px');
        }
    }
    </script>


</div>
@endsection
