@extends('layouts.myapp')
@section('content')



<div class="container">


<div class="row">
  <div class="col-md-3">
    <img class="info-profile-photo" src="{{ asset('storage/prisoner/'.$prisoner->profile_path) }}">
  </div>
  <div class="col-md-5" style="top:40px;">
    <li style="margin:6px 2px 6px 2px; font-size:30px; font-family: 'raleway'; text-align:left;">{{$prisoner->full_name}}</li>
    <li style="margin:6px 2px 6px 2px; font-size:20px; font-family: 'raleway'; text-align:left;">{{$prisoner->country->country_name}}</li>
    <li style="margin:6px 2px 6px 2px; font-size:20px; font-family: 'raleway'; text-align:left;">{{$prisoner->atoll->atoll_name}} . {{$prisoner->island->island_name}}</li>
    <li style="margin:6px 2px 6px 2px; font-size:20px; font-family: 'raleway'; text-align:left;">{{$prisoner->address}}</li>
  </div>

</div>



<section class="outline">

<div class="row">
  <div class="col-md-2">
    <a target="_blank" href="{{route('print.hukum',['id' => $prisoner->id])}}"><i  style="color:#28B463;"class="fa fa-file-text fa-3x" aria-hidden="true"></i></a>
  </div>
  <div class="col-md-8">
<h2 style="text-align:center" class="">Engaged Sentences<h2>
  </div>
  <div class="col-md-2">
    <a href="{{route('add.hukum',['id' => $prisoner->id])}}"><button style="background:#28B463;height:40px; color:#fff;border-radius:10px;font-size:17px;width:100%;"class=""type="button" name="button">Add New Sentence</button></a>
  </div>
</div>

<hr class="style-one">

<table class="">
  <thead style="">
    <tr style="background-color:transparent;">
      <th><label style="text-align:center;" class="lbl">#</label></th>
      <th><label style="text-align:center;" class="lbl">Order No.</label></th>
      <th><label class="lbl">Sentence Title</label></th>
      <th><label style="text-align:center;" class="lbl">Implimented Date</label></th>
      <th><label style="text-align:center;" class="lbl">Duration</label></th>
      <th><label style="text-align:center;" class="lbl">Sentence Expiry Date</label></th>
      <th><label style="text-align:center;" class="lbl">Currnt Status</label></th>
      <th><label style="text-align:center;" class="lbl"></label></th>
    </tr>
  </thead>
  <tbody >

<?php $no = 1; ?>
    @foreach($prisoner->offence->sortBy('offence_status_id') as $offence)
    <tr class="@if($offence->offence_status_id == 1) active-status @elseif($offence->offence_status_id == 2) inactive-status @else hold-status @endif">
      <td><label style="text-align:center;" class="">{{$no++}}</label></td>
      <td><label style="text-align:center;" class="">{{$offence->court_order_no}}</label></td>
      <td><label class="lbl">{{$offence->offence_type->offence}}</label></td>
      <td><label style="text-align:center;" class="lbl">
        <?php $else = " - - -" ?>
        @if($offence->sentenced_implement_date != NULL)
        {{
          \carbon\carbon::createFromFormat('Y-m-d',$offence->sentenced_implement_date)->format('d-m-Y')
        }}
        @else
        {{$else}}
        @endif
      </label></td>
      <td><label style="text-align:center;" class="lbl"> {{$offence->period_year}} y {{$offence->period_month }} m {{$offence->period_day}} d </label></td>
      <td><label style="text-align:center;" class="lbl">{{\carbon\carbon::parse($offence->getExpiredDate())->format('d-m-Y')}}</label></td>
      <td><label style="text-align:center;" class="lbl">{{$offence->offence_status->offence_status}}</label></td>
      <td><a href="{{route('open.hukum',['id' => $offence->id])}}" style="color:#fff;"><i class="fa fa-pencil-square-o fa-2x" aria-hidden="true"></i></a></td>
    </tr>
    @endforeach

  </tbody>
</table>
</section>











</div>
@endsection
