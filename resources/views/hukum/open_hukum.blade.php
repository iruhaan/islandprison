@extends('layouts.myapp')
@section('content')



<div class="container">


<div class="row">
  <div class="col-md-3">
    <img class="info-profile-photo" src="{{ asset('storage/prisoner/'.$hukum->prisoner->profile_path) }}">
  </div>
  <div class="col-md-5" style="top:40px;">
    <li style="margin:6px 2px 6px 2px; font-size:30px; font-family: 'raleway'; text-align:left;">{{$hukum->prisoner->full_name}}</li>
    <li style="margin:6px 2px 6px 2px; font-size:20px; font-family: 'raleway'; text-align:left;">{{$hukum->prisoner->country->country_name}}</li>
    <li style="margin:6px 2px 6px 2px; font-size:20px; font-family: 'raleway'; text-align:left;">{{$hukum->prisoner->atoll->atoll_name}} . {{$hukum->prisoner->island->island_name}}</li>
    <li style="margin:6px 2px 6px 2px; font-size:20px; font-family: 'raleway'; text-align:left;">{{$hukum->prisoner->address}}</li>
  </div>
</div>



<br><br>
<h2 style="text-align:center" class="">{{$hukum->offence_type->offence}}<h2>
  <hr class="style-one">

  <div class="row">
    <div class="col-md-4">
      <label class="lbl">Sentence Title</label>
      <label class="text-info faruma">{{$hukum->offence_type->offence}}</label>
    </div>
    <div class="col-md-3">
      <label class="lbl">Category</label>
      <label id="offence_cat" class="text-info faruma">{{$hukum->offence_cat->offence_cat}}</label>
    </div>
    <div class="col-md-3">
      <label class="lbl">Court Order No.</label>
      <label class="text-info faruma">{{$hukum->court_order_no}}</label>
    </div>
    <div class="col-md-2">
      <label class="lbl">Ordered Court</label>
      <label class="text-info faruma">{{$hukum->court_name->court_name}}</label>
    </div>
  </div>









  <div class="row">
    <div class="col-md-2">
      <label class="lbl">Sentenced Date</label>
      <label class="text-info faruma">{{$hukum->sentenced_date}}</label>
    </div>

    <div class="col-md-2">
      <label class="lbl">Total Increment</label>
      <label class="text-info ">{{$hukum->total_increment_days}}</label>
    </div>
    <div class="col-md-2">
      <label class="lbl">Total Decrement</label>
      <label class="text-info ">{{$hukum->total_decrement_days}}</label>
    </div>
    <div class="col-md-6">
      <label class="lbl">Sentence Period</label>
      <label class="text-info ">{{$hukum->period_year." Year "}} {{$hukum->period_month." Month "}} {{$hukum->period_day." Days"}}</label>
    </div>
  </div>


  <div class="row">
    <div class="col-md-3">
      <label class="lbl">Sentence implement Date</label>
      <label class="text-info ">{{$hukum->sentenced_implement_date}}</label>
    </div>
    <div class="col-md-6">
      <label class="lbl">Current Status</label>
      <label class="text-info faruma">{{$hukum->offence_status->offence_status}}</label>
    </div>

    <div class="col-md-3">
      <label class="lbl">Court Order Copy</label>
      <label class="text-info faruma"><a target="_blank" href="{{ asset('storage/gaziyya_chit/'.$hukum->court_order_copy) }}"><img style="width:120px;" src="{{ asset('storage/gaziyya_chit/'.$hukum->gaziyya_copy) }}">
        </a>
    </label>
    </div>
  </div>

  <div class="row">
    <label class="lbl">Sentence Summery  </label>
    <label class="text-info faruma">{{$hukum->warrant_detail}}</label>
  </div>




<!-- start offence status logs  -->
<div class="outline">
  <div class="row">
    <div class="col-md-2">
    </div>
    <div class="col-md-8">
  <h2 style="text-align:center" class="">Sentence Implementation Log<h2>
    </div>
    <div class="col-md-2">
      <a href="{{route('add.offence.status',['id' => $hukum->id])}}"><button style="background:#28B463;height:40px; color:#fff;border-radius:10px;font-size:17px;width:100%;"class="faruma"type="button" name="button">Enter New Log</button></a>
    </div>
  </div>
  <hr class="style-one">
  <table class="">
    <thead style="">
      <tr style="background-color:transparent; border-bottom: 1px solid #0CAB9A;">
        <th ><label style="text-align:center;" class="lbl">Effected Date</label></th>
        <th><label style="text-align:left;" class="lbl">Status</label></th>
        <th style="width:40%;"><label style="text-align:right;" class="lbl">Detail</label></th>
        <th><label style="text-align:center;" class="lbl">Implemented Days</label></th>
        <th><label style="text-align:center;" class="lbl">Document copy</label></th>
        <th><label style="text-align:center;" class="lbl"></label></th>
      </tr>
    </thead>
    <tbody style="background:rgb(0,0,0,1%);">
      <?php $thanfeez=0;
            $thanfeezNukoh=0;
       ?>
      @foreach($hukum->offence_status_log as $index => $status)
      <tr style="color:
        @if($status->offence_status_id == 1)
        #2ECC71
        @elseif($status->offence_status_id == 2)
        #EC7063
        @else
        #F39C12
        @endif
      ;">
        <td style="border-bottom: 1px solid #0CAB9A;text-align:center;">{{\carbon\carbon::parse($status->status_date)->format('d-M-Y')}}</td>
        <td style="border-bottom: 1px solid #0CAB9A;text-align:right;">{{$status->offence_status->offence_status}}</td>
        <td style="border-bottom: 1px solid #0CAB9A;text-align:right;">{{$status->detail}}</td>

        <?php
              $date = \carbon\Carbon::parse($status->status_date);
              if (isset($hukum->offence_status_log[$index+1]->status_date)){
                        $nextDate = $hukum->offence_status_log[$index+1]->status_date;
                        }
                      else{
                        $nextDate = \carbon\carbon::now()->format('Y-m-d');
                      }
              $diff = $date->diffInDays($nextDate);

              if ($status->offence_status_id == 1) {
                  $thanfeez = $thanfeez + $diff;
                }
              elseif($status->offence_status_id != 2) {
                  $thanfeezNukoh = $thanfeezNukoh + $diff;
                }
         ?>
          <td class="" style="border-bottom: 1px solid #0CAB9A;text-align:center;">{{$diff}} Days</td>



        <td style="border-bottom: 1px solid #0CAB9A;text-align:center;" class="faruma" >
          @if($status->document_copy == "no_file.png")
          <img style="width:80px;" src="{{ asset('storage/status_log/'.$status->document_copy) }}" alt="">
          @else
          <a target="_blank" href="{{ asset('storage/status_log/'.$status->document_copy) }}" ><img style="width:80px;" src="{{ asset('storage/status_log/file.png') }}" alt=""></a>
          @endif
        </td>



        <td style="border-bottom: 1px solid #0CAB9A;text-align:center;" ><a href="{{route('edit.offence.status',['id'=>$status->id])}}" style="color:#E67E22;"><i class="fa fa-pencil-square-o fa-2x" aria-hidden="true"></i></a></td>

      </tr>
      @endforeach
    </tbody>
  </table>

  <?php
  $years = ($thanfeez / 365) ; // days / 365 days
  $years = floor($years); // Remove all decimals
  $month = ($thanfeez % 365) / 30.5; // I choose 30.5 for Month (30,31) ;)
  $month = floor($month); // Remove all decimals
  $days = ($thanfeez % 365) % 30.5; // the rest of days
  $thanfeez_long =  $years.' Year   '.$month.' Month   '.$days.' Day';

  //calculatin thanfeez nukoh huri duvas
  $nukohyears = ($thanfeezNukoh / 365) ; // days / 365 days
  $nukohyears = floor($nukohyears); // Remove all decimals
  $nukohmonth = ($thanfeezNukoh % 365) / 30.5; // I choose 30.5 for Month (30,31) ;)
  $nukohmonth = floor($nukohmonth); // Remove all decimals
  $nukohdays = ($thanfeezNukoh % 365) % 30.5; // the rest of days
  $thanfeezNukoh_long =  $nukohyears.' Year   '.$nukohmonth.' Month   '.$nukohdays.' Day';

  //calculatin total decrement
  $decrement_years = ($hukum->total_decrement_days / 365) ; // days / 365 days
  $decrement_years = floor($decrement_years); // Remove all decimals
  $decrement_month = ($hukum->total_decrement_days % 365) / 30.5; // I choose 30.5 for Month (30,31) ;)
  $decrement_month = floor($decrement_month); // Remove all decimals
  $decrement_days = ($hukum->total_decrement_days % 365) % 30.5; // the rest of days
  $decrement_year_long =  $decrement_years.' Year   '.$decrement_month.' Month   '.$decrement_days.' Day';

  //calculatin total increment
  $increment_years = ($hukum->total_increment_days / 365) ; // days / 365 days
  $increment_years = floor($increment_years); // Remove all decimals
  $increment_month = ($hukum->total_increment_days % 365) / 30.5; // I choose 30.5 for Month (30,31) ;)
  $increment_month = floor($increment_month); // Remove all decimals
  $increment_days = ($hukum->total_increment_days % 365) % 30.5; // the rest of days
  $increment_year_long =  $increment_years.' Year   '.$increment_month.' Month   '.$increment_days.' Day';

  //calculatin total increment
  $total_period_days_years = ($hukum->total_period_days / 365) ; // days / 365 days
  $total_period_days_years = floor($total_period_days_years); // Remove all decimals
  $total_period_days_month = ($hukum->total_period_days % 365) / 30.5; // I choose 30.5 for Month (30,31) ;)
  $total_period_days_month = floor($total_period_days_month); // Remove all decimals
  $total_period_days_days = ($hukum->total_period_days % 365) % 30.5; // the rest of days
  $total_period_days_year_long =  $total_period_days_years.' Year   '.$total_period_days_month.' Month   '.$total_period_days_days.' Day';

  //calculatin total increment
  $ramin_days_years = ($balance / 365) ; // days / 365 days
  $ramin_days_years = floor($ramin_days_years); // Remove all decimals
  $ramin_days_month = ($balance % 365) / 30.5; // I choose 30.5 for Month (30,31) ;)
  $ramin_days_month = floor($ramin_days_month); // Remove all decimals
  $ramin_days_days = ($balance % 365) % 30.5; // the rest of days
  $ramin_days_year_long =  $ramin_days_years.' Year   '.$ramin_days_month.' Month   '.$ramin_days_days.' Day';




   ?>
  <ul style="text-align:left;margin: 20px 20px 10px auto;font-size:20px; color:#2ECC71"colspan="7"class=""><i class="fa fa-check-circle fa-1x" aria-hidden="true"></i> &nbsp;Served Days :{{$thanfeez  }} | <b style="color:#fff;">{{$thanfeez_long}}</b></ul>
  <ul style="text-align:left;margin: 20px 20px 10px auto;font-size:20px; color: #E67E22;"colspan="7" class=""><i class="fa fa-pause-circle" aria-hidden="true"></i> &nbsp; Postponed Days : {{$thanfeezNukoh}} | <b style="color:#fff;">{{$thanfeezNukoh_long}}</b></ul>
  <ul style="text-align:left;margin: 20px 20px 10px auto;font-size:20px; color: #E74C3C;"colspan="7" class=""><i class="fa fa-minus-circle fa-1x" aria-hidden="true"></i> &nbsp; Total Decrement Days in Sentence Duration : {{$hukum->total_decrement_days}} | <b style="color:#fff;">{{$decrement_year_long}}</b></ul>
  <ul style="text-align:left;margin: 20px 20px 10px auto;font-size:20px; color: #48C9B0;"colspan="7" class=""><i class="fa fa-plus-circle fa-1x" aria-hidden="true"></i> &nbsp; Total Increment Days in Sentence Duration : {{$hukum->total_increment_days}} | <b style="color:#fff;">{{$increment_year_long}}</b></ul>
  <ul style="text-align:left;margin: 20px 20px 10px auto;font-size:20px; color: #F1C40F;"colspan="7" class=""><i class="fa fa-gavel fa-1x" aria-hidden="true"></i> &nbsp; Sentence Duration : {{$hukum->total_period_days}} | <b style="color:#fff;">{{$total_period_days_year_long}}</b></ul>
  <ul style="text-align:left;margin: 20px 20px 10px auto;font-size:20px; color: #00D9DF;"colspan="7" class=""><i class="fa fa-hourglass-end fa-1x" aria-hidden="true"></i> &nbsp;Total Days Remain in Sentence : {{$balance}} | <b style="color:#fff;">{{$ramin_days_year_long}}</b></ul>
  <ul style="text-align:left;margin: 20px 20px 10px auto;font-size:20px; color: #0FF191;"colspan="7" class=""><i class="fa fa-clock-o fa-1x" aria-hidden="true"></i> &nbsp;Sentence Expiry Date : <b style="color:#fff;">{{\carbon\carbon::parse($expire_date)->subDays(1)->format('Y-m-d')}}</b></ul>

</div>
<!-- end of offence status log -->











<!-- start offence incrment -->
<div class="outline">
  <div class="row">
    <div class="col-md-2">
    </div>
    <div class="col-md-8">
  <h2 style="text-align:center" class="">Sentence Increment Detail<h2>
    </div>
    <div class="col-md-2">
      <a href="{{route('add.offence.increment',['id' => $hukum->id])}}"><button style="background:#28B463;height:40px; color:#fff;border-radius:10px;font-size:17px;width:100%;"class=""type="button" name="button">Add Record</button></a>
    </div>
  </div>

    <hr class="style-one">

    <table class="">
      <thead style="">
        <tr style="background-color:transparent;border-bottom: 1px solid #0CAB9A;">
          <th><label style="text-align:left;" class="lbl">Reason</label></th>
          <th><label style="text-align:center;" class="lbl">Date</label></th>
          <th><label style="text-align:center;" class="lbl">Inducted Days</label></th>
          <th><label style="width:400px;text-align:left;color:#fff;" class="">Additional Info</label></th>
          <th><label style="text-align:center;" class="lbl">Document</label></th>
          <th><label style="text-align:center;" class="lbl"></label></th>
        </tr>
      </thead>

      <tbody style="background:rgb(0,0,0,5%);text-decoration: none!important;">
        @foreach($hukum->offence_increment->sortBy('increment_date') as $offence)
        <tr style="color:
          @if($offence->approval_status_id == 1)
          #E74C3C
          @elseif($offence->approval_status_id == 2)
          #E67E22
          @else
          #2ECC71
          @endif
        ;">
          <td style="text-decoration: none!important;border-bottom: 1px solid #0CAB9A;text-align:left;padding-right:20px;" class="">{{$offence->increment_type->increment_type}}</td>
          <td style="border-bottom: 1px solid #0CAB9A;text-align:center;">{{$offence->increment_date}}</td>
          <td style="border-bottom: 1px solid #0CAB9A;text-align:center;">{{$offence->days}}</td>
          <td style="border-bottom: 1px solid #0CAB9A;line-height:30px;width:400px;text-align:justify;" class="faruma">{{$offence->additional_detail}}</td>

              <td style="border-bottom: 1px solid #0CAB9A;text-align:center;" class="faruma" >
                @if($offence->document_copy == "no_file.png")
                <img style="width:80px;" src="{{ asset('storage/offence_increment/'.$offence->document_copy) }}" alt="">
                @else
                <a target="_blank" href="{{ asset('storage/offence_increment/'.$offence->document_copy) }}" ><img style="width:80px;" src="{{ asset('storage/offence_increment/file.png') }}" alt=""></a>
                @endif
              </td>

          <td style="border-bottom: 1px solid #0CAB9A;text-align:center;" ><a href="{{route('print.increment',['id'=>$offence->id])}}" style="color:#E67E22;"><i class="fa fa-pencil-square-o fa-2x" aria-hidden="true"></i></a></td>

        </tr>
      @endforeach
      </tbody>
    </table>
  </div>
  <!-- end offence increment -->






<!-- start offence decrement -->
<section class="outline">
  <div class="row">
    <div class="col-md-2">
    </div>
    <div class="col-md-8">
  <h2 style="text-align:center" class="waheedh">Sentence Decrement Detail<h2>
    </div>
    <div class="col-md-2">
      <a href="{{route('add.offence.decrement',['id' => $hukum->id])}}"><button style="background:#28B463;height:40px; color:#fff;border-radius:10px;font-size:17px;width:100%;"class=""type="button" name="button">Add Record</button></a>
    </div>
  </div>

    <hr class="style-one">

    <table class="">
      <thead style="">
        <tr style="background-color:transparent;border-bottom: 1px solid #0CAB9A;">
          <th><label style="text-align:left;" class="lbl">Reason</label></th>
          <th><label style="text-align:center;" class="lbl">Date</label></th>
          <th><label style="text-align:center;" class="lbl">Deducted Days</label></th>
          <th><label style="width:400px;text-align:Left; color:#fff;" class="">Additional Info</label></th>
          <th><label style="text-align:center;" class="lbl">Document</label></th>
          <th><label style="text-align:center;" class="lbl"></label></th>
        </tr>
      </thead>

      <tbody style="background:rgb(0,0,0,5%);">
        @foreach($hukum->offence_decrement->sortBy('decrement_date') as $offence)
        <tr style="color:
          @if($offence->approval_status_id == 1)
          #E74C3C
          @elseif($offence->approval_status_id == 2)
          #E67E22
          @else
          #2ECC71
          @endif
        ;">
          <td style="border-bottom: 1px solid #0CAB9A;text-align:left;padding-left:20px;" class="faruma">{{$offence->decrement_type->decrement_type}}</td>
          <td style="border-bottom: 1px solid #0CAB9A;text-align:center;">{{$offence->decrement_date}}</td>
          <td style="border-bottom: 1px solid #0CAB9A;text-align:center;">{{$offence->days}}</td>
          <td style="border-bottom: 1px solid #0CAB9A;line-height:30px;width:400px;text-align:justify;" class="faruma">{{$offence->additional_detail}}</td>

              <td style="border-bottom: 1px solid #0CAB9A;text-align:center;" class="faruma" >
                @if($offence->document_copy == "no_file.png")
                <img style="width:80px;" src="{{ asset('storage/offence_decrement/'.$offence->document_copy) }}" alt="">
                @else
                <a target="_blank" href="{{ asset('storage/offence_decrement/'.$offence->document_copy) }}" ><img style="width:80px;" src="{{ asset('storage/offence_decrement/file.png') }}" alt=""></a>
                @endif
              </td>

          <td style="border-bottom: 1px solid #0CAB9A;text-align:center;" ><a href="{{route('print.decrement',['id' => $offence->id])}}" style="color:#fff;"><i class="fa fa-pencil-square-o fa-2x" aria-hidden="true"></i></a></td>


      @endforeach
      </tbody>
    </table>
  </section>
<!-- end offence decrement -->










  <script>
  $(document).ready(function(){
    var dr = document.getElementById("drug_offence");
    var num = "<?php Print($hukum->id); ?>";
    if (num == 1) {
      dr.classList.remove("invisible");
    }
    else{
    }
  });
  </script>

</div>
@endsection
