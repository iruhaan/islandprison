@extends('layouts.report-header')
@section('content')

<style media="print">
@page {
  size: A4 landscape;
  margin: 0.6mm 0.6mm 0.6mm 0.6mm;
}
</style>



<div id="fill" class="container-fluid" style="background-color:#fff;">
<button onclick="window.print();" style="position:fixed;float:right;"class="print-button"><span class="print-icon"></span></button>
<img style="margin-top:10px; width:100%; display:block; margin:auto;" src="/images/mcsletterhead.png" alt="letter-head">



<h2 class="waheedh" style="color:#000; text-align:center;">ކޮށްފައިވާ ޙުކުމްތަކުގެ ތަފްސީލް</h2>
<hr class="hr-print">
  <!-- prisoner information -->
  <div class="row">
    <div class="col-print-3">
      <div style="margin-top:5px; margin-left:80px;">
        {!! QrCode::size(50)->generate(Request::url()); !!}
      </div>
    </div>
    <div class="col-print-3 faruma">
      <ul class="faruma">
        <ui><img class="barcode" src="data:image/png;base64,{{DNS1D::getBarcodePNG($prisoner->file_number, 'C39')}}" alt="barcode" /></ui>
        <li>ފައިލް ނަންބަރ : <b>{{$prisoner->file_number}}</b></li>
        <li>އައިޑި ކާޑް ނަންބަރ : <b>{{$prisoner->id_passportNo}}</b></li>
      </ul>
    </div>
    <div class="col-print-4 faruma">
      <ul class="faruma">
        <li>ފުރިހަމަ ނަން : <b>{{$prisoner->full_name_dhi}}</b></li>
        <li>އެޑްރެސް : <b>{{$prisoner->atoll_name_dhi}} . {{$prisoner->island_name_dhi}} / {{$prisoner->address_dhi}} , {{$prisoner->country->country_name_dhi}}</b></li>
        <li>އުމުރު : <b>{{$prisoner->age}}</b></li>
      </ul>
    </div>
    <div class="col-print-2" style="float:right;">
      <img src="{{ asset('storage/prisoner/'.$prisoner->profile_path) }}" class="image--cover">
    </div>
  </div>


  <hr class="table-print">
  <div class="table-responsive">
  <table class="table faruma">
    <thead>
      <tr>
        <th class="faruma">#</th>
        <th class="faruma">ޤަޟިއްޔާ ނަންބަރު</th>
        <th class="faruma" style="text-align:right;">މައްސަލަ</th>
        <th class="" style="text-align:center;">ޙުކުމްކުރި ތާރީޚް</th>
        <th class="" style="text-align:center;">ތަންފީޛް ފެށި ތާރީޚް</th>
        <th class="faruma" style="text-align:center;">މުއްދަތު</th>
        <th class=""style="text-align:center;">ޙުކުމަށް އިތުރުކުރި ދުވަސް</th>
        <th class=""style="text-align:center;">ޙުކުމުން އުނިކުރި ދުވަސް</th>
        <th class=""style="text-align:center;">މުއްދަތު ހަމަވާ ތާރީޚް</th>
        <th class="faruma" style="text-align:center;">ސްޓޭޓަސް</th>
      </tr>
    </thead>
    <tbody>
      <?php $order = 1 ?>
      @foreach($prisoner->offence as $offence)

      <tr>
        <td class="" style="text-align:center;">{{$order}}</td>
        <td class="" style="text-align:center;">{{$offence->gaziyya_no}}</td>
        <td class="faruma" style="text-align:right;">{{$offence->offence_type->offence_type_dhi}}</td>
        <td class="" style="text-align:center;">{{$offence->sentenced_date}}</td>
        <td class="" style="text-align:center;">{{$offence->sentenced_implement_date}}</td>
        <td class="" style="text-align:center;">{{$offence->period_year}} އ {{$offence->period_month }} މ {{$offence->period_day}} ދ</td>
        <td class="" style="text-align:center;">{{$offence->total_increment_days}}</td>
        <td class="" style="text-align:center;">{{$offence->total_decrement_days}}</td>
        <td class="" style="text-align:center;">{{$offence->getExpiredDate()}}</td>
        <td class="" style="text-align:center;">{{$offence->offence_status->offence_status}}</td>
      </tr>
      <?php $order++ ?>
      @endforeach
    </tbody>
  </table>
  </div>
</div>










<div class="divFooter">Printed Date/Time : {{\carbon\carbon::now()->format('d-m-Y / H:i')}}</div>
</div>
@endsection
