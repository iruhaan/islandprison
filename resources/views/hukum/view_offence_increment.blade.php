@extends('layouts.myapp')
@section('content')

<div class="container">

<div class="row">
  <div class="col-md-3">
    <img class="info-profile-photo" src="{{ asset('storage/prisoner/'.$hukum->prisoner->profile_path) }}">
  </div>
  <div class="col-md-5" style="top:40px;">
    <li style="margin:6px 2px 6px 2px; font-size:30px; font-family: 'raleway'; text-align:left;">{{$hukum->prisoner->full_name_eng}}</li>
    <li style="margin:6px 2px 6px 2px; font-size:20px; font-family: 'raleway'; text-align:left;">{{$hukum->prisoner->country->country_name_eng}}</li>
    <li style="margin:6px 2px 6px 2px; font-size:20px; font-family: 'raleway'; text-align:left;">{{$hukum->prisoner->atoll->atoll_name_eng}} . {{$hukum->prisoner->island->island_name_eng}}</li>
    <li style="margin:6px 2px 6px 2px; font-size:20px; font-family: 'raleway'; text-align:left;">{{$hukum->prisoner->address_eng}}</li>
  </div>
  <div class="col-md-4" style="top:40px;">
    <li style="margin:6px 2px 6px 2px; font-size:30px; text-align:right;" class="waheedh">{{$hukum->prisoner->full_name_dhi}}</li>
    <li style="margin:6px 2px 6px 2px; font-size:20px; text-align:right;" class="waheedh">{{$hukum->prisoner->country->country_name_dhi}}</li>
    <li style="margin:6px 2px 6px 2px; font-size:20px; text-align:right;" class="waheedh">{{$hukum->prisoner->atoll->atoll_name_dhi}} . {{$hukum->prisoner->island->island_name_dhi}}</li>
    <li style="margin:6px 2px 6px 2px; font-size:20px; text-align:right;" class="waheedh">{{$hukum->prisoner->address_dhi}}</li>
  </div>
</div>

<br><br>
<h2 style="text-align:center" class="waheedh">{{$hukum->offence_type->offence_type_dhi}} | {{$hukum->sentence_type->sentence_type_dhi}}<h2>
  <hr class="style-one">

  <div class="row">
    <div class="col-md-3">
      <label class="lbl">ކަނޑައެޅި ނިޔާ</label>
      <label class="text-info faruma">{{$hukum->sentence_type->sentence_type_dhi}}</label>
    </div>
    <div class="col-md-3">
      <label class="lbl">ސާބިތުވި ކުށް</label>
      <label class="text-info faruma">{{$hukum->offence_type->offence_type_dhi}}</label>
    </div>
    <div class="col-md-3">
      <label class="lbl">ކުށުގެ ކެޓަގަރީ</label>
      <label id="offence_cat" class="text-info faruma">{{$hukum->offence_cat->offence_cat_dhi}}</label>
    </div>
    <div class="col-md-3">
      <label class="lbl">ޤަޟިއްޔާ ނަންބަރ</label>
      <label class="text-info faruma">{{$hukum->gaziyya_no}}</label>
    </div>
  </div>




<br>
<br>
  <h2 style="text-align:center" class="waheedh">ހުކުމަށް މުއްދަތު އިތުރުކުރުން<h2>
    <hr class="style-one">

    <form class="" action="{{route('update.offence.increment',['id'=>$offence_increment->id])}}" method="post" enctype="multipart/form-data">
      {{CSRF_field()}}


    <!-- auto calculator -->

      <div class="row">
        <div class="col-md-4">

        </div>
        <div class="col-md-2">

        </div>
        <div class="col-md-3">
          <label class="lbl">ނިންމާ ތާރީޚް</label>
          <input autocomplete="off" id="e_date" type="text" value="{{$offence_increment->end_date}}" name="end_date" style="text-align:center;" class="datepicker increment">
        </div>
        <div class="col-md-3">
          <label class="lbl">ދުވަސް ގުނަން ފަށާ ތާރީޚް</label>
          <input autocomplete="off" id="s_date" type="text" value="{{$offence_increment->start_date}}" name="start_date" style="text-align:center;" class="datepicker increment">
        </div>
      </div>
      <hr class="style-one">




    <div class="row">
      <div class="col-md-3">
        <label class="lbl">ރިފަރެންސް ކަރުދާހުގެ ކޮޕީ</label>
        <input type="file" name="document_copy" value="{{$offence_increment->document_copy}}">
      </div>
      <div class="col-md-2">
        <label class="lbl">އިތުރު ކުރި ދުވަސް</label>
        <input id="tdays" style="text-align:center;" type="text" name="days" value="{{$offence_increment->days}}" required required autocomplete="off">
      </div>
      <div class="col-md-5">
        <label class="lbl">މުއްދަތު އިތުރުކުރާ ބާވަތް</label>
        <select class="faruma" name="increment_type_id" required>
          @foreach($increment_types as $type)
          @if($offence_increment->increment_type_id == $type->id)
          <option selected value="{{$type->id}}">{{$type->increment_type}}</option>
          @endif
          <option value="{{$type->id}}">{{$type->increment_type}}</option>
          @endforeach
        </select>
      </div>
      <div class="col-md-2">
        <label class="lbl">ތާރީޚް</label>
        <input class="datepicker" style="text-align:center; "type="text" value="{{$offence_increment->increment_date}}" name="increment_date" required autocomplete="off">
      </div>
    </div>
    <div class="row">
      <label class="lbl">އިތުރު ތަފްސީލް</label>
      <textarea type="text" name="additional_detail" class="faruma thaanaKeyboardInput autosize">{{$offence_increment->additional_detail}}</textarea>
    </div>

    <div class="row">
      <div class="col-md-3">
        <input type="submit" name="" value="އެޕްރޫވަލަށް ފޮނުވުމަށް" class="faruma btn" style="color:#fff;height:50px; width:100%; background:#28B463;font-size:19px;">
      </div>
      <div class="col-md-3">
        <a href="{{route('open.hukum',['id' => $offence_increment->offence_id])}}"><button type="button" name="" value="" class="faruma btn" style="color:#fff;height:50px; width:100%; background:#E74C3C;font-size:19px;">
          ކެންސަލް
        </button></a>
      </div>
      <div class="col-md-3">
      </div>
      <div class="col-md-3">
        <a href="{{route('delete.offence.increment',['id' => $offence_increment->id])}}"><button type="button" name="" value="" class="faruma btn" style="color:#fff;height:50px; width:100%; background:#BF1200;font-size:19px;">
          އުނިކުރުމަށް
        </button></a>
      </div>
    </div>

</form>












    <script type="text/javascript">
    $('.increment').on('change', function() {
      var edate = document.getElementById('e_date').value;
      var sdate = document.getElementById('s_date').value;
      var start = new Date(sdate);
      var end = new Date(edate);
      var diff =  Math.floor(( Date.parse(end) - Date.parse(start) ) / 86400000);
      document.getElementById("tdays").value = diff + 1;
      });
    </script>





    <script type="text/javascript">
    autosize();
    function autosize(){
        var text = $('.autosize');

        text.each(function(){
            $(this).attr('rows',5);
            resize($(this));
        });

        text.on('input', function(){
            resize($(this));
        });

        function resize ($text) {
            $text.css('height', 'auto');
          $text.css('width', '100%');
            $text.css('height', $text[0].scrollHeight+'px');
        }
    }
    </script>


</div>
@endsection
