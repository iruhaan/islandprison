@extends('layouts.report-header')
@section('content')

 <style media="print">
 @page {
   size: A5 landscape;
   margin: 0.6mm 0.6mm 0.6mm 0.6mm;
 }
 </style>



<div id="fill" class="container-fluid" style="background-color:#fff;">
<button onclick="window.print();" style="position:fixed;float:right;"class="print-button"><span class="print-icon"></span></button>
<img style=" display:block; margin:auto;margin-top:30px;width:9%;" src="/images/brand.png" alt="letter-head">



<h2 class="" style="color:#000; text-align:center;">Sentence Increment</h2>
<hr class="hr-print">

<!-- prisoner information -->
<div class="row">
  <div class="col-print-3">
    <div style="margin-top:50px; margin-left:20px;">
      <!-- {{QrCode::size(60)->generate($increment->offence->prisoner->file_number)}} -->
      <img src="data:image/png;base64,{{DNS1D::getBarcodePNG($increment->offence->prisoner->file_number, 'C39')}}" alt="barcode" />

    </div>
  </div>
  <div class="col-print-7" >
    <ul class="">
      <li>File Number : <b>{{$increment->offence->prisoner->file_number}}</b></li>
      <li>NID : <b>{{$increment->offence->prisoner->id_passportNo}}</b></li>
      <li>Full Name : <b>{{$increment->offence->prisoner->full_name}}</b></li>
      <li>Address : <b>{{$increment->offence->prisoner->atoll_name}} . {{$increment->offence->prisoner->island_name}} / {{$increment->offence->prisoner->address}} , {{$increment->offence->prisoner->country->country_name}}</b></li>
    </ul>
  </div>
  <div class="col-print-2">
    <img src="{{ asset('storage/prisoner/'.$increment->offence->prisoner->profile_path) }}" class="image--cover" style="float: center;">
  </div>
</div>
<hr class="hr-print">


<!-- hukum detail -->
<div class="row">

<!-- Increment detail -->

  <div class="col-print-6 " >
    <ul class="">
      <li>Date : <b>{{$increment->increment_date}}</b></li>
      <li>Reason : <b>{{$increment->increment_type->increment_type}}</b></li>
      <li>Inducted Days : <b>{{$increment->days}} Days</b></li>
    </ul>
  </div>

  <div class="col-print-6" >
    <ul class="">
      <li>Court Order No. : <b>{{$increment->offence->court_order_no}}</b></li>
      <li>Duration : <b> {{$increment->offence->period_year}} Y {{$increment->offence->period_month }} M {{$increment->offence->period_day}} D </b></li>
      <li>Offence Title : <b>{{$increment->offence->offence_type->offence}}</b></li>
      <li>Offence Category : <b>{{$increment->offence->offence_cat->offence_cat}}</b></li>
    </ul>
  </div>
</div>

<div class="row">
  <div class="col-print-12" style="padding-right:50px;padding-left:50px;">
    <p>{{$increment->additional_detail}}</p>
  </div>
</div>
<!--
<script type="text/javascript">
let perpx = 0.7/1300;
let ratio = perpx * window.screen.width;
document.getElementById('fill').style.transform = "scale(" + ratio + ")";
</script> -->




</div>

@stop
