<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>

  <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/tinymce/4.5.3/tinymce.min.js"></script> -->
<script src="/tinymce/tinymce.min.js"></script>
<script src="/tinymce/jquery.tinymce.min.js"></script>



  <!-- <script src="{!!asset('/js/tin.js')!!}"></script>
  <script src="/js/tinymce.min.js"></script> -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Kuvverin</title>


    <!-- Include stylesheet -->

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('/css/font-awesome.css') }}" rel="stylesheet">
    <link href="{{ asset('css/style.css') }}" rel="stylesheet">
    <link href="{{ asset('css/brand.scss') }}" rel="stylesheet">
    <link href="{{ asset('css/navbar.css') }}" rel="stylesheet">
    <link href="{{ asset('css/hukum.css') }}" rel="stylesheet">
    <link href="{{ asset('css/table.css') }}" rel="stylesheet">
    <link href="{{ asset('css/customerTable.css') }}" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Raleway" rel="stylesheet">
    <link href="/css/jquery-ui.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="/css/sweetalert.css">
    <link rel="stylesheet" type="text/css" href="/css/util.css">
    <link rel="stylesheet" type="text/css" href="/css/occurance.css">
    <link rel="stylesheet" type="text/css" href="{{asset('fonts/font.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('css/checkbox.css')}}">
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">

    <!-- time picker -->
    <link href="{{ asset('css/timepicker.css') }}" rel="stylesheet">
    <link href="{{ asset('css/font-awesome.css') }}" rel="stylesheet">
    <script src="{{ asset('js/timepicker.js') }}"></script>

    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/smoothness/jquery-ui.css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.min.css" rel="Stylesheet" type="text/css" />





<style media="screen">




.autocomplete-suggestions {
  background: rgb(0,0,0,70%);
  border-radius:30px;
  color:#fff;
  overflow: auto;
  text-align:right;
  font-size:17px;
  font-family: 'faruma'
}

.autocomplete-suggestion {
  padding: 10px 50px;
  white-space: nowrap;
  overflow: hidden;
}
.autocomplete-selected {
  background: #000;
  color:#fff;
}
.autocomplete-suggestions strong {
  font-weight: normal;
  color: orange;
}
.autocomplete-group {
  padding: 10px 5px;
}

.autocomplete-group strong {
  display: block;
}

.txta {
  width: 100%;
  max-width: 500px;
  min-height: 100px;
  font-family: Arial, sans-serif;
  font-size: 16px;
  overflow: hidden;
  line-height: 1.4;
}



#btn {
background: #222;
height: 50px;
border: none;
border-radius: 10px;
color: #eee;
font-size: 25px;
position: relative;
transition: 1s;
-webkit-tap-highlight-color: transparent;
display: flex;
align-items: center;
justify-content: center;
cursor: pointer;
padding-top: 5px;
width: 250px;
}

#btn #circle {
width: 5px;
height: 5px;
background: transparent;
border-radius: 50%;
position: absolute;
top: 0;
left: 50%;
overflow: hidden;
transition: 500ms;
}

.noselect {
-webkit-touch-callout: none;
  -webkit-user-select: none;
   -khtml-user-select: none;
     -moz-user-select: none;
      -ms-user-select: none;
          user-select: none;
}

#btn:hover {
background-color: #F1C40F;
color:#000;
}

#btn:hover #circle {
height: 50px;
width: 100%;
left: 0;
border-radius: 0;
border-bottom: 2px solid #eee;
}

.download-div{
  width: 95%;
  margin-left: auto;
  margin-right: auto;
  margin-top:40px;
}

  body{
    background: rgb(0,0,0);
    background: radial-gradient(circle, rgba(0,0,0,1) 0%, rgba(1,56,70,1) 0%, rgba(0,0,0,1) 100%);
    color: #fff;
    background-size: auto;
  }


        body::-webkit-scrollbar {
          width: 5px;
          background: rgb(12, 171, 154);
          background: radial-gradient(circle, rgba(12, 171, 154) 0%, transparent 100%);
        }
        body::-webkit-scrollbar-track {
          -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.3);
          border-radius: 20px;
        }

        body::-webkit-scrollbar-thumb {
            background:rgba(12, 171, 154.40%);
            -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.5);
        }
        body::-webkit-scrollbar-thumb:active {
            background:#00FFC5;
            -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.5);
        }
  html,body
{
    width: 100%;
    height: 100%;
    margin: 0px;
    padding: 0px;
    overflow-x: hidden;
}
.dropbtn {
  background-color: rgb(0,0,0,50%);
  color: white;
  padding: 15px;
  font-size: 16px;
  border: none;
  border-radius: 14px;
  width: 200px;
  z-index:100000;

}

.dropdown {
  position: relative;
  display: inline-block;
  float: right;
  margin-top:46px;
  z-index:1;
  margin-left:0px;
}

.dropdown-con {
  display: none;
  position: absolute;
  background-color: rgb(0,0,0,70%);
  min-width: 160px;
  box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.8);
  z-index: -1;
  border-radius: 20px;
  z-index:100000;
}

.dropdown-con a {
  color: #fff;
  padding: 12px 16px;
  text-decoration: none;
  display: block;
  z-index:100000;
  z-index: -1;

}

.dropdown-con a:hover {
  background-color: #006F6F!important;
  z-index:100000;
  z-index: -1;
}

.dropdown:hover .dropdown-con {
  display: block!important;
  z-index:-1;
}

.dropdown:hover .dropbtn {
  background-color: rgb(225,225,225,20%)!important;
  z-index:-1;
}

</style>

<script src="/js/sweetalert.min.js"></script>
<!-- date picker -->
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

</head>

<body onload="loadpage();">
    <div id="app">
        <nav class="navbar navbar-default navbar-static-top nav" style="z-index:2;">
            <div class="container">
                <div class="navbar-header navbar-header-cus" style="width:100%;">

                    <!-- Collapsed Hamburger -->
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                        <span class="sr-only">Toggle Navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>

                    <!-- Branding Image -->

                    <div class="row">
                      <div class="col-md-2">
                        <img class="brand" src="/images/brandWhite.png" alt="brand">
                      </div>
                      <div class="col-md-6" style="margin-top:20px;">
                        <div class="row">
                          <a style="font-family: 'Avenir', 'sans-serif';"class="navbar-brand brand-name" href="{{ url('/') }}">
                            ISLAND PRISON
                          </a>
                        </div>
                        <div class="row">
                          <h4 class="mcs-name" style="font-family: 'Avenir', 'sans-serif';">Offender Management System | OMS</h4>
                        </div>
                      </div>
                      <div class="col-md-4">
                        <!-- Right Side Of Navbar -->
                        <div class="dropdown">
                          @if (Auth::guest())
                          <a href="{{ route('login') }}"><button class="dropbtn waheedh">Login</button></a>
                          @else
                          <button class="dropbtn waheedh" style="text-transform: capitalize;">{{ Auth::user()->name }}</button>
                          <div class="dropdown-con">
                            <a href="/">Home</a>
                            @if (Auth::user()->offender == true)
                            <a href="{{route('offender')}}">Offender Panel</a>
                            @endif
                            @if (Auth::user())
                            <a href="{{route('offender.supervisor')}}">Offender Supervisor</a>
                            @endif
                            @if (Auth::user())
                            <a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">Logout</a>
                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                {{ csrf_field() }}
                            </form>
                            @endif
                            @endif
                          </div>
                        </div>
                      </div>
                    </div>




                </div>

                <div class="collapse navbar-collapse" id="app-navbar-collapse">
                    <!-- Left Side Of Navbar -->



                    <!-- <a href="#" class="notification" style="float:right;margin-top:60px; margin-right:20px;">
                        <span class="faruma">ފިޔަވަޅު އަޅަންޖެހޭ ލިޔެކިޔުން </span>
                          <span class="badge">3</span>
                    </a>
                    <a href="#" class="notification" style="float:right;margin-top:60px; margin-right:70px;">
                        <span> </span>
                          <span class="badge">3</span>
                    </a> -->
                </div>
            </div>
        </nav>



    </div>


    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}"></script>
    <script src="{{ asset('jtk-master/jtk.js') }}"></script>

    <script type="text/javascript" src="/js/datepicker.js"></script>
    <script src="/js/sweetalert.min.js"></script>
    <script src="/js/customtable.js"></script>
    <!-- date picker -->

    <script type="text/javascript" src="/js/datepicker.js"></script>
    <script type="text/javascript" src="/js/jquery-3.0.0.js"></script>
    <script type="text/javascript" src="/js/jquery-3.5.1.js"></script>

    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

    @if ($errors->any())
    <div class="alert alert-danger container faruma">
        <ul>
            @foreach ($errors->all() as $error)
                <li style="color:red; font-size:18px;" class="faruma"><i style="color:red;" class="fa fa-exclamation-triangle" aria-hidden="true"></i> {{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif

    @yield('content')





    <script>
    $( function() {
      var date = $('.datepicker').datepicker({ dateFormat: 'yy-mm-dd' }).val();
    } );
    </script>


    <script>
    function loadpage() {
      thaanaKeyboard.defaultKeyboard = 'phonetic';
    }
    </script>



    <script>
    $(document).ready(function(){
    $(':input').live('focus',function(){
    $(this).attr('autocomplete', 'off');
    });
    });
    </script>


    <script type="text/javascript">
      var timepicker = new TimePicker('time', {
        lang: 'en',
        theme: 'dark'
          });
          timepicker.on('change', function(evt) {

      var value = (evt.hour || '00') + ':' + (evt.minute || '00');
        evt.element.value = value;
      });
    </script>

        <script type="text/javascript">
        $( ".selector" ).datepicker({ dateFormat: 'yy-mm-dd' });
        </script>



          <img  style="padding-top:60px;display: block; margin:0 auto;width:100px;"src="/images/brandWhite.png" alt="">
          <hr class="style-one" style="margin:20px 0 20px 0;">
          <h2 class="" style="margin-bottom:10px;color:#fff;text-align:center;font-family:'avenir';">Offender Management System | oMs</h2>

          <h4 style="margin-bottom:10px;color:#fff;text-align:center;font-family:'avenir';">Powered by: Ahmed Iruhaan | S1900161 @2021.</h4>
          <hr class="style-one" style="margin:20px 0 30px 0;">


</body>


</html>
