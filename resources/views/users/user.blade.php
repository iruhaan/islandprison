
@extends('layouts.app')
@section('content')
<style media="screen">

</style>
<h2 style="color:#fff; text-align:center;">Manage System Users</h2>
<hr class="style-one" style="margin: 20px 0 20px 0;">
<br>
<div class="container">
  <div class="verticle-align-middle">
      <input type="text" id="search" onkeyup="myFunction()" placeholder="Search" style="float:left;width:400px;">
  </div>
</div>
<div class="container">


    <table class="table" id="myTable" style="background-color:transparent;box-shadow:3px 4px 30px #000;">
      <thead>
        <tr style="background-color:rgb(0,0,0,80%)!important; color:#fff;">
          <th style="text-align:center;font-size:13px;">Service Number</th>
          <th style="text-align:center;font-size:13px;">Name</th>
          <th style="text-align:center;font-size:13px;">Email Address</th>
          <th style="text-align:center;font-size:13px;">System Admin</th>
          <th style="text-align:center;font-size:13px;">HR User</th>
          <th style="text-align:center;font-size:13px;">Command Level</th>
          <th style="text-align:center;font-size:13px;">Supervisor Level</th>
          <th style="text-align:center;font-size:13px;">security</th>
          <th style="text-align:center;font-size:13px;">roster</th>
          <th style="text-align:center;font-size:13px;">operation</th>
          <th style="text-align:center;font-size:13px;">Officer Level</th>

        </tr>
      </thead>
      <tbody>
          @foreach($users as $user)

          <tr style="height:70px;">
          <td style="text-align:center;font-family:'avenir';background-color:rgb(0,0,0,70%);"><a style="color:#fff;cursor:hand;" href="{{action('UserController@edit',$user->id)}}">{{ $user->service_number}}</a></td>
          <td style="text-align:center;color:#fff;background-color:rgb(0,0,0,70%);" class="faruma">{{ $user->name }}</td>
          <td style="text-align:center;color:#fff;background-color:rgb(0,0,0,70%);font-family:'avenir';">{{ $user->email }}</td>
          <td style="text-align:center;color:#fff;background-color:rgb(0,0,0,70%);font-family:'avenir';@if($user->admin ==1) color:#2ECC71; @else color:#E74C3C; @endif">@if($user->admin == 1) Yes @else No @endif</td>
          <td style="text-align:center;color:#fff;background-color:rgb(0,0,0,70%);font-family:'avenir';@if($user->hr ==1) color:#2ECC71; @else color:#E74C3C; @endif">@if($user->hr == 1) Yes @else No @endif</td>
          <td style="text-align:center;color:#fff;background-color:rgb(0,0,0,70%);font-family:'avenir';@if($user->commandhead ==1) color:#2ECC71; @else color:#E74C3C; @endif">@if($user->commandhead == 1) Yes @else No @endif</td>
          <td style="text-align:center;color:#fff;background-color:rgb(0,0,0,70%);font-family:'avenir';@if($user->supervisor ==1) color:#2ECC71; @else color:#E74C3C; @endif">@if($user->supervisor ==1) Yes @else No @endif</td>
          <td style="text-align:center;color:#fff;background-color:rgb(0,0,0,70%);font-family:'avenir';@if($user->security ==1) color:#2ECC71; @else color:#E74C3C; @endif">@if($user->security ==1) Yes @else No @endif</td>
          <td style="text-align:center;color:#fff;background-color:rgb(0,0,0,70%);font-family:'avenir';@if($user->roster ==1) color:#2ECC71; @else color:#E74C3C; @endif">@if($user->roster ==1) Yes @else No @endif</td>
          <td style="text-align:center;color:#fff;background-color:rgb(0,0,0,70%);font-family:'avenir';@if($user->operation ==1) color:#2ECC71; @else color:#E74C3C; @endif">@if($user->operation ==1) Yes @else No @endif</td>
          <td style="text-align:center;color:#fff;background-color:rgb(0,0,0,70%);font-family:'avenir';@if($user->officer == true) color:#2ECC71; @else color:#E74C3C; @endif">@if($user->officer == true) Yes @else No @endif</td>

        @endforeach
      </tbody>
    </table>
    <script>
    function myFunction() {
      // Declare variables
      var input, filter, table, tr, td, i, txtValue;
      input = document.getElementById("search");
      filter = input.value.toUpperCase();
      table = document.getElementById("myTable");
      tr = table.getElementsByTagName("tr");

      // Loop through all table rows, and hide those who don't match the search query
      for (i = 0; i < tr.length; i++) {
        td = tr[i].getElementsByTagName("td")[0];
        if (td) {
          txtValue = td.textContent || td.innerText;
          if (txtValue.toUpperCase().indexOf(filter) > -1) {
            tr[i].style.display = "";
          } else {
            tr[i].style.display = "none";
          }
        }
      }
    }
  </script>
</div>
@stop
