@extends('layouts.login-header')
@section('content')

<h2 style="color:#fff; text-align:center;">Modify System Users</h2>
<hr class="style-one">
<br>
<div class="container">
<form method="POST" action="{{ action('UserController@update',$user->id) }}">
  {{ CSRF_field()}}

      <table class="table" id="myTable" style="background-color:transparent;box-shadow:3px 4px 30px #fff;">
        <thead>
          <tr style="background-color:rgb(0,0,0,80%)!important; color:#fff;">
            <th style="text-align:center;font-size:13px;">Service Number</th>
            <th style="text-align:center;font-size:13px;">Name</th>
            <th style="text-align:center;font-size:13px;">System Admin</th>
            <th style="text-align:center;font-size:13px;">HR User</th>
            <th style="text-align:center;font-size:13px;">Command Level</th>
            <th style="text-align:center;font-size:13px;">Supervisor Level</th>
            <th style="text-align:center;font-size:13px;">Security</th>
            <th style="text-align:center;font-size:13px;">Roster</th>
            <th style="text-align:center;font-size:13px;">Operation</th>
            <th style="text-align:center;font-size:13px;">Officer Level</th>
          </tr>
        </thead>
        <tbody>


            <tr style="height:70px;">
            <td style="text-align:center;font-family:'avenir';background-color:rgb(0,0,0,70%); color:#fff;">
            <!-- ServiceNumber -->
            {{$user->service_number}}

            </td>
            <td style="text-align:center;color:#fff;background-color:rgb(0,0,0,70%);color:#fff;" class="faruma">
              <!-- User Name -->
            {{$user->name}}

            </td>

            <td style="text-align:center;color:#fff;background-color:rgb(0,0,0,70%);font-family:'avenir';">
              <!-- Admin -->
              <label class="btn btn-success">
              <input style="width:40px;height:40px;" name="admin" type="checkbox" autocomplete="off"
              @if($user->admin == 1)
              checked
              @endif
              >
              <span class="glyphicon glyphicon-ok"></span>
            </label>

            </td>
            <td style="text-align:center;color:#fff;background-color:rgb(0,0,0,70%);font-family:'avenir';">
              <!-- HR -->
              <label class="btn btn-success">
              <input style="width:40px;height:40px;" name="hr" type="checkbox" autocomplete="off"
              @if($user->hr == 1)
              checked
              @endif
              >
              <span class="glyphicon glyphicon-ok"></span>
            </label>




            </td>
            <td style="text-align:center;color:#fff;background-color:rgb(0,0,0,70%);font-family:'avenir';">
              <!-- ommand Head -->
              <label class="btn btn-success">
              <input style="width:40px;height:40px;" name="commandhead" type="checkbox" autocomplete="off"
              @if($user->commandhead == 1)
              checked
              @endif
              >
              <span class="glyphicon glyphicon-ok"></span>
            </label>
            </td>

            <td style="text-align:center;color:#fff;background-color:rgb(0,0,0,70%);font-family:'avenir';">
              <!-- Supervisor-->
              <label class="btn btn-success">
              <input style="width:40px;height:40px;" name="supervisor" type="checkbox" autocomplete="off"
              @if($user->supervisor == 1)
              checked
              @endif
              >
              <span class="glyphicon glyphicon-ok"></span>
            </label>


            </td>


            </td>
            <td style="text-align:center;color:#fff;background-color:rgb(0,0,0,70%);font-family:'avenir';">
              <!-- ommand Head -->
              <label class="btn btn-success">
              <input style="width:40px;height:40px;" name="security" type="checkbox" autocomplete="off"
              @if($user->security == 1)
              checked
              @endif
              >
              <span class="glyphicon glyphicon-ok"></span>
            </label>
            </td>




            </td>
            <td style="text-align:center;color:#fff;background-color:rgb(0,0,0,70%);font-family:'avenir';">
              <!-- ommand Head -->
              <label class="btn btn-success">
              <input style="width:40px;height:40px;" name="roster" type="checkbox" autocomplete="off"
              @if($user->roster == 1)
              checked
              @endif
              >
              <span class="glyphicon glyphicon-ok"></span>
            </label>
            </td>



            <td style="text-align:center;color:#fff;background-color:rgb(0,0,0,70%);font-family:'avenir';">
              <!-- Operation-->

              <label class="btn btn-success">
              <input style="width:40px;height:40px;" name="operation" type="checkbox" autocomplete="off"
              @if($user->operation == 1)
              checked
              @endif
              >
              <span class="glyphicon glyphicon-ok"></span>
            </label>



            </td>


            <td style="text-align:center;color:#fff;background-color:rgb(0,0,0,70%);font-family:'avenir';">
              <!-- Officer-->

              <label class="btn btn-success">
              <input style="width:40px;height:40px;" name="officer" type="checkbox" autocomplete="off"
              @if($user->officer == 1)
              checked
              @endif
              >
              <span class="glyphicon glyphicon-ok"></span>
            </label>



            </td>
        </tbody>
      </table>

<div class="row">
  <div class="col-md-12">
    <label for="reset_password" style="font-family:'avenir'; color:#fff;">Want Reset Password ? then tick me</label>

      <label class="btn btn-info">
        <input type="checkbox" name="reset_password">
        <span class="glyphicon glyphicon-ok"></span>
      </label>
  </div>

</div>
<br>
<div class="row">
  <div class="col-md-4">
<input type="submit" name="" value="Save" class="btn btn-success" style="width:100%">
  </div>
  <div class="col-md-4">

  </div>
  <div class="col-md-4">
  </div>
</div>
</div>
</form>




<form class="" action="{{route('save.yaumiyya.control',['id'=>$user->id])}}" method="post">
    {{ CSRF_field()}}

<div class="container">



  <h2 style="color:#fff; text-align:center;">Yaumiyya Control</h2>
  <hr class="style-one">

  <table class="table" style="color:#fff;background:#000;">
    <thead>
      <tr>
        <th style="text-align:center;" colspan="4">General Administration</th>
      </tr>
      <tr>
        <th style="text-align:center;">Admin</th>
        <th style="text-align:center;">HR</th>
        <th style="text-align:center;">Reception</th>
        <th style="text-align:center;">Stock</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td style="text-align:center;"><input name="y_admin" @if($user->y_admin == 1) checked @endif style="width:40px;height:40px;"type="checkbox"></td>
        <td style="text-align:center;"><input name="y_hr" @if($user->y_hr == 1) checked @endif style="width:40px;height:40px;"type="checkbox"></td>
        <td style="text-align:center;"><input name="y_reception" @if($user->y_reception == 1) checked @endif style="width:40px;height:40px;"type="checkbox"></td>
        <td style="text-align:center;"><input name="y_stock" @if($user->y_stock == 1) checked @endif style="width:40px;height:40px;"type="checkbox"></td>
      </tr>
    </tbody>
  </table>

  <table class="table" style="color:#fff;background:#000;">
    <thead>
      <tr>
        <th style="text-align:center;" colspan="4">Support Service</th>
      </tr>
      <tr>
        <th style="text-align:center;">Welfare</th>
        <th style="text-align:center;">Maintenance</th>
        <th style="text-align:center;">IT</th>
        <th style="text-align:center;">Transport</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td style="text-align:center;"><input name="y_welfare" @if($user->y_welfare == 1) checked @endif style="width:40px;height:40px;"type="checkbox"></td>
        <td style="text-align:center;"><input name="y_maintenance" @if($user->y_maintenance == 1) checked @endif style="width:40px;height:40px;"type="checkbox"></td>
        <td style="text-align:center;"><input name="y_it" @if($user->y_it == 1) checked @endif style="width:40px;height:40px;"type="checkbox"></td>
        <td style="text-align:center;"><input name="y_transport" @if($user->y_transport == 1) checked @endif style="width:40px;height:40px;"type="checkbox"></td>
      </tr>
    </tbody>
  </table>

  <table class="table" style="color:#fff;background:#000;">
    <thead>
      <tr>
        <th style="text-align:center;" colspan="4">Offender Service</th>
      </tr>
      <tr>
        <th style="text-align:center;">Offender Records</th>
        <th style="text-align:center;">Canteen Service</th>
        <th style="text-align:center;">Visits</th>
        <th style="text-align:center;">Prison Labour</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td style="text-align:center;"><input name="y_offender_record" @if($user->y_offender_record == 1) checked @endif style="width:40px;height:40px;"type="checkbox"></td>
        <td style="text-align:center;"><input name="y_canteen" @if($user->y_canteen == 1) checked @endif style="width:40px;height:40px;"type="checkbox"></td>
        <td style="text-align:center;"><input name="y_visit" @if($user->y_visit == 1) checked @endif style="width:40px;height:40px;"type="checkbox"></td>
        <td style="text-align:center;"><input name="y_labour" @if($user->y_labour == 1) checked @endif style="width:40px;height:40px;"type="checkbox"></td>
      </tr>
    </tbody>
  </table>

  <table class="table" style="color:#fff;background:#000;">
    <thead>
      <tr>
        <th style="text-align:center;" colspan="4">Unit Management</th>
      </tr>
      <tr>
        <th style="text-align:center;">Orientation</th>
        <th style="text-align:center;">Procecution</th>
        <th style="text-align:center;">Complain</th>
        <th style="text-align:center;">Units</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td style="text-align:center;"><input name="y_orientation" @if($user->y_orientation == 1) checked @endif style="width:40px;height:40px;"type="checkbox"></td>
        <td style="text-align:center;"><input name="y_procecution" @if($user->y_procecution == 1) checked @endif style="width:40px;height:40px;"type="checkbox"></td>
        <td style="text-align:center;"><input name="y_complain" @if($user->y_complain == 1) checked @endif style="width:40px;height:40px;"type="checkbox"></td>
        <td style="text-align:center;"><input name="y_unit" @if($user->y_unit == 1) checked @endif style="width:40px;height:40px;"type="checkbox"></td>
      </tr>
    </tbody>
  </table>

  <table class="table" style="color:#fff;background:#000;">
    <thead>
      <tr>
        <th style="text-align:center;" colspan="3">Programs</th>
      </tr>
      <tr>
        <th style="text-align:center;">Case Management</th>
        <th style="text-align:center;">Councelling</th>
        <th style="text-align:center;">Library</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td style="text-align:center;"><input name="y_case_management" @if($user->y_case_management == 1) checked @endif style="width:40px;height:40px;"type="checkbox"></td>
        <td style="text-align:center;"><input name="y_councelling" @if($user->y_councelling == 1) checked @endif style="width:40px;height:40px;"type="checkbox"></td>
        <td style="text-align:center;"><input name="y_library" @if($user->y_library == 1) checked @endif style="width:40px;height:40px;"type="checkbox"></td>
      </tr>
    </tbody>
  </table>

  <table class="table" style="color:#fff;background:#000;">
    <thead>
      <tr>
        <th style="text-align:center;" colspan="2">Parole and Climency</th>
      </tr>
      <tr>
        <th style="text-align:center;">Interview</th>
        <th style="text-align:center;">Assessment</th>

      </tr>
    </thead>
    <tbody>
      <tr>
        <td style="text-align:center;"><input name="y_interview" @if($user->y_interview == 1) checked @endif style="width:40px;height:40px;"type="checkbox"></td>
        <td style="text-align:center;"><input name="y_assessment" @if($user->y_assessment == 1) checked @endif style="width:40px;height:40px;"type="checkbox"></td>
      </tr>
    </tbody>
  </table>

  <table class="table" style="color:#fff;background:#000;">
    <thead>
      <tr>
        <th style="text-align:center;" colspan="3">Health</th>
      </tr>
      <tr>
        <th style="text-align:center;">Medical Center</th>
        <th style="text-align:center;">Medical Record</th>
        <th style="text-align:center;">Pharmacy</th>

      </tr>
    </thead>
    <tbody>
      <tr>
        <td style="text-align:center;"><input name="y_medical_center" @if($user->y_medical_center == 1) checked @endif style="width:40px;height:40px;"type="checkbox"></td>
        <td style="text-align:center;"><input name="y_medical_record" @if($user->y_medical_record == 1) checked @endif style="width:40px;height:40px;"type="checkbox"></td>
        <td style="text-align:center;"><input name="y_pharmacy" @if($user->y_pharmacy == 1) checked @endif style="width:40px;height:40px;"type="checkbox"></td>
      </tr>
    </tbody>
  </table>

  <table class="table" style="color:#fff;background:#000;">
    <thead>
      <tr>
        <th style="text-align:center;" colspan="2">Offender Movement</th>
      </tr>
      <tr>
        <th style="text-align:center;">Escort Team</th>
        <th style="text-align:center;">Special Security</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td style="text-align:center;"><input name="y_escort" @if($user->y_escort == 1) checked @endif style="width:40px;height:40px;"type="checkbox"></td>
        <td style="text-align:center;"><input name="y_special_security" @if($user->y_special_security == 1) checked @endif style="width:40px;height:40px;"type="checkbox"></td>
      </tr>
    </tbody>
  </table>

  <table class="table" style="color:#fff;background:#000;">
    <thead>
      <tr>
        <th style="text-align:center;" colspan="2">Security</th>
      </tr>
      <tr>
        <th style="text-align:center;">Internal Investigation</th>
        <th style="text-align:center;">Internal Inspection</th>
        <th style="text-align:center;">Gate House</th>
        <th style="text-align:center;">Medical Addmission</th>
        <th style="text-align:center;">Officer Incharge</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td style="text-align:center;"><input name="y_investigation" @if($user->y_investigation == 1) checked @endif style="width:40px;height:40px;"type="checkbox"></td>
        <td style="text-align:center;"><input name="y_inspection" @if($user->y_inspection == 1) checked @endif style="width:40px;height:40px;"type="checkbox"></td>
        <td style="text-align:center;"><input name="y_gatehouse" @if($user->y_gatehouse == 1) checked @endif style="width:40px;height:40px;"type="checkbox"></td>
        <td style="text-align:center;"><input name="y_addmission" @if($user->y_addmission == 1) checked @endif style="width:40px;height:40px;"type="checkbox"></td>
        <td style="text-align:center;"><input name="y_incharge" @if($user->y_incharge == 1) checked @endif style="width:40px;height:40px;"type="checkbox"></td>
      </tr>
    </tbody>
  </table>


  <input type="submit" name="" value="Save" class="btn btn-danger">
</div>

</form>
@stop
