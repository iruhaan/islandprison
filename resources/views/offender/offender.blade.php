@extends('layouts.myapp')
@section('content')

<div class="container">



<h2 style="text-align:center;"class="waheedh">Prisners Registry</h2>
<hr class="style-one">


<div class="row">
  <div class="col-md-3">
    <a href="{{route('add.prisoner')}}"><button class="waheedh" style="border-radius:50px;height:40px; width:150px; font-size:18px; color:#000; background-color:#04B7B9">
    Add New Prisoner
    </button>
    </a>
  </div>
  <div class="col-md-3">
  </div>
  <div class="col-md-3">
  </div>
<div class="col-md-3">
<input  class="search-table faruma thaanaKeyboardInput"type="text" name="" value="" placeholder="ހޯދާ . . .">
</div>
</div>

<div id="prisoner_registry" style="overflow-y: scroll; height: 400px;">
<table class="">
  <thead>
    <tr>
      <th style="text-align:center;">File Number</th>
      <th style="text-align:left;">Full Name</th>
      <th style="text-align:left;">Address</th>
      <th style="text-align:center;">Country</th>
      <th style="text-align:center;">Security Level</th>
      <th style="text-align:center;">NID | PNo</th>
      <th style="text-align:center;">Status</th>
      <th style="text-align:center;">Photo</th>
    </tr>
  </thead>
    <tbody>
      @foreach($prisoners as $prisoner)
      <tr>
        <td id="file_number" class="@if($prisoner->status->id == 1) status-active @elseif($prisoner->status->id == 2 || $prisoner->status->id == 3) status-geybandhu @else status-inactive @endif" style="text-align:center;">
          <a style="color:#fff;" target="_blank" href="{{route('prisoner.info',['id'=>$prisoner->id])}}">{{$prisoner->file_number}}</a>
        </td>
        <td style="text-align:left;" class="faruma @if($prisoner->status->id == 1) active @elseif($prisoner->status->id == 2 || $prisoner->status->id == 3) geybandhu @else inactive @endif">
          {{$prisoner->full_name}}
        </td>
        <td style="text-align:left;" class="faruma @if($prisoner->status->id == 1) active @elseif($prisoner->status->id == 2 || $prisoner->status->id == 3) geybandhu @else inactive @endif">
          {{$prisoner->address}} , {{$prisoner->atoll->atoll_name}} . {{$prisoner->island->island_name}}
        </td>
        <td style="text-align:center;" class="faruma @if($prisoner->status->id == 1) active @elseif($prisoner->status->id == 2 || $prisoner->status->id == 3) geybandhu @else inactive @endif">
          {{$prisoner->country->country_name}}
        </td>
        <td style="text-align:center;" class="faruma @if($prisoner->status->id == 1) active @elseif($prisoner->status->id == 2 || $prisoner->status->id == 3) geybandhu @else inactive @endif">
          {{$prisoner->security_level->security_level}}
        </td>
        <td style="text-align:center;" class="@if($prisoner->status->id == 1) active @elseif($prisoner->status->id == 2 || $prisoner->status->id == 3) geybandhu @else inactive @endif">
          {{$prisoner->id_passportNo}}
        </td>
        <td id="status_code" style="text-align:center;" class="faruma @if($prisoner->status->id == 1) active @elseif($prisoner->status->id == 2 || $prisoner->status->id == 3) geybandhu @else inactive @endif">
          {{$prisoner->status->status}}
        </td>
        <td style="text-align:center;"><a class="trigger"><img class="list-profile-photo" src="{{asset('storage/prisoner/'.$prisoner->profile_path)}}"></a></td>
        <div id="pop-up">
          <img class="pop-ups" src="{{asset('storage/prisoner/'.$prisoner->profile_path)}}">
        </div>
      </tr>
      @endforeach
    </tbody>
</table>
</div>





<!-- HIDDEN / POP-UP DIV -->


<script type="text/javascript">
$(function() {
var moveLeft = 20;
var moveDown = 10;

$('a.trigger').hover(function(e) {
  $('div#pop-up').show();
  //.css('top', e.pageY + moveDown)
  //.css('left', e.pageX + moveLeft)
  //.appendTo('body');
}, function() {
  $('div#pop-up').hide();
});

$('a.trigger').mousemove(function(e) {
  $("div#pop-up").css('top', e.pageY + moveDown).css('left', e.pageX + moveLeft);
});

});
</script>


<script>
$(document).on("keyup",".search-table", function () {
            var value = $(this).val();
            $("table tr").each(function (index) {
                $row = $(this);
                $row.show();
                if (index !== 0 && value) {
                    var found = false;
                    $row.find("td").each(function () {
                        var cell = $(this).text();
                        if (cell.indexOf(value.toLowerCase()) >= 0) {
                            found = true;
                            return;
                        }
                    });
                    if (found === true) {
                        $row.show();
                    }
                    else {
                        $row.hide();
                    }
                }
      });
});
</script>



</div>
@endsection
