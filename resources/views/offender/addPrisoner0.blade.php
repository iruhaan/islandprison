@extends('layouts.myapp')
@section('content')

<div class="container">
<h3 style="text-align:center">Add Prisoner</h3>

<hr class="style-one">

<form method="post" action="{{route('saveprisoner')}}">
  {{CSRF_field()}}

<div class="row">
  <div class="col-md-2">
    <label class="lbl">File Number</label>
    <input style="text-align:center;"type="text" name="file_number" value="" autocomplete="off">
  </div>
  <div class="col-md-3">
    <label class="lbl">Full Name</label>
    <input style="text-align:center;"type="text" name="full_name" value="" autocomplete="off">
  </div>
  <div class="col-md-3">
    <label class="lbl">Address</label>
    <input style="text-align:center;"type="text" name="address" value="" autocomplete="off">
  </div>
  <div class="col-md-2">
    <label class="lbl">NID | P.NO</label>
    <input style="text-align:center;"type="text" name="id_passportNo" value="" autocomplete="off">
  </div>
  <div class="col-md-2">
    <label class="lbl">Date of Birth</label>
    <input class="datepicker" style="text-align:center;" type="text" name="DOB" value="" autocomplete="off">
  </div>
</div>


<div class="row">
  <div class="col-md-3">
    <label class="lbl">Select Country</label>
    <select style="text-align-last:center;" class="faruma" name="country_id" id="country" required>
      <option value="" disabled selected>Select</option>
      @foreach($countries as $country)
        <option value="{{$country->id}}">{{$country->country_name}}</option>
      @endforeach
    </select>
  </div>
  <div class="col-md-2">
    <label class="lbl">Select Atoll</label>
    <select style="text-align-last:center;" class="faruma" name="atoll_id" id="atoll" disabled>
      <option style="text-align-last:center;" value="" disabled selected>Please Select</option >
      @foreach($atolls as $atoll)
        <option value="{{$atoll->id}}">{{$atoll->atoll_name}}</option>
      @endforeach
    </select>
  </div>
  <div class="col-md-3">
    <label class="lbl">Select Island</label>
    <select style="text-align-last:center;" class="faruma" name="island_id" id="island" disabled>
      <option value="" disabled selected>Please Select</option>
      @foreach($islands as $island)
        <option value="{{$island->id}}">{{$island->island_name}}</option>
      @endforeach
    </select>
  </div>

  <div class="col-md-2">
    <label class="lbl">Select Gender</label>
    <select style="text-align-last:center;" name="gender_id">
      <option style="text-align-last:center;" value="" disabled selected>Please Select</option >
      @foreach($genders as $gender)
        <option value="{{$gender->id}}">{{$gender->gender}}</option>
      @endforeach
    </select>
  </div>

  <div class="col-md-2">
    <label class="lbl">Nick Name</label>
    <input class="" style="text-align:center;" type="text" name="nick_name" value="" autocomplete="off">
  </div>
</div>


<div class="row">
  <div class="col-md-2">
    <input style="width:100%;height:50px;font-size:18px; color:#fff;"type="submit" name="" class="btn btn-primary"value="Save">
  </div>
  <div class="col-md-2">
    <a href="/"><input style="width:100%;height:50px;font-size:18px; color:#fff;"type="button" name="" class="btn btn-danger"value="Cancel"></a>
  </div>
  <div class="col-md-2">
    <input style="width:100%;height:50px;font-size:18px; color:#fff;"type="reset" name="" class="btn btn-warning"value="Reset">
  </div>
</div>

<input type="hidden" name="security_level_id" value="1">
<input type="hidden" name="status_id" value="1">
<input type="hidden" name="profile_path" value="unknown.png">

</form>




<script>
    $(function() {
        $('select[id=atoll]').change(function() {
            var url = '/islandList/' + $(this).val();
            $.get(url, function(data) {
                var select = $('select[id= island]');
                $('select[id="island"]').empty();

                        $('select[id="island"]').append(
                                    '<option value="" disabled selected> Select Island </option>'
                                    )
                $.each(data,function(key, value) {
                    select.append('<option value=' + value.id + '>' + value.island_name + '</option>');
                });
            });
        });
    });
</script>

<!-- enable and disable dropdown -->
<script type="text/javascript">
    $(function () {
        $("#country").change(function () {

          //alert($(this).val());

            if ($(this).val() == 2) {
                $("#atoll").removeAttr("disabled");
                $("#island").removeAttr("disabled");

            } else {
                $("#atoll").attr("disabled", "disabled");
                $("#island").attr("disabled", "disabled");
            }
        });
    });
</script>


</div>
@endsection
