@extends('layouts.myapp')
@section('content')

<div class="container">
<div class="row">
<div class="col-md-2">

</div>
<div class="col-md-8">
<h2 class="" style="color:#fff;text-align:center;">{{$prisoner->full_name}}'s General Information </h2>
</div>

<div class="col-md-2">
  @if($prisoner->status_id === 1)
  <div class="video__icon">
    <div class="circle--outer"></div>
    <div class="circle--inner"></div>
  </div>
  @else
  <div class="video__icon-r">
    <div class="circle--outer-r"></div>
    <div class="circle--inner-r"></div>
  </div>
  @endif
</div>

</div>

<hr class="style-one">

<div class="row" style="margin-bottom:100px;">


<div class="col-md-4">

  <li class="" style="color:#fff;text-align:left;margin: 30px 0px 30px 0px;font-size:18px;">NID | Pno : <b style="color:#F1C40F;">{{$prisoner->id_passportNo}}</b></li>
  <li class="" style="color:#fff;text-align:left;margin: 30px 0px 30px 0px;font-size:18px;">DOB : <b style="color:#F1C40F;">{{$prisoner->DOB}}</b></li>
  <li class="" style="color:#fff;text-align:left;margin: 30px 0px 30px 0px;font-size:18px;">AGE : <b style="color:#F1C40F;">{{\Carbon\Carbon::parse($prisoner->DOB)->diff(\Carbon\Carbon::now())->format('%y Years %m Months and %d Days')}}</b></li>
  <li class="" style="color:#fff;text-align:left;margin: 30px 0px 30px 0px;font-size:18px;">GENDER : <b style="color:#F1C40F;">{{$prisoner->gender->gender}}</b></li>
  <li class="" style="color:#fff;text-align:left;margin: 30px 0px 30px 0px;font-size:18px;">NICK NAME : <b style="color:#F1C40F;">{{$prisoner->nick_name}}</b></li>
  <li class="" style="color:#fff;text-align:left;margin: 30px 0px 30px 0px;font-size:18px;">SECURITY LEVEL : <b style="color:#F1C40F;">{{$prisoner->security_level->security_level}}</b></li>
</div>

<div class="col-md-4">
  <li class="" style="color:#fff;text-align:left;margin: 30px 0px 30px 0px;font-size:18px;">FILE NUMBER : <b style="color:#F1C40F;">{{$prisoner->file_number}}</b></li>
  <li class="" style="color:#fff;text-align:left;margin: 30px 0px 30px 0px;font-size:18px;">FULL NAME : <b style="color:#F1C40F;">{{$prisoner->full_name}}</b></li>
  <li class="" style="color:#fff;text-align:left;margin: 30px 0px 30px 0px;font-size:18px;">COUNTRY : <b style="color:#F1C40F;">{{$prisoner->country->country_name}}</b></li>
  <li class="" style="color:#fff;text-align:left;margin: 30px 0px 30px 0px;font-size:18px;">ATOLL | ISLAND : <b style="color:#F1C40F;">{{$prisoner->atoll->atoll_name_dhi}} . {{$prisoner->island->island_name}}</b></li>
  <li class="" style="color:#fff;text-align:left;margin: 30px 0px 30px 0px;font-size:18px;">ADDRESS : <b style="color:#F1C40F;">{{$prisoner->address}}</b></li>
  <li class="" style="color:#fff;text-align:left;margin: 30px 0px 30px 0px;font-size:18px;">STATUS : <b style="color:#F1C40F;">{{$prisoner->status->status}}</b></li>
</div>

<div class="col-md-4">
  <img class="info-profile-photo" src="{{ asset('storage/prisoner/'.$prisoner->profile_path) }}">
</div>

</div>


<h2 class="" style="color:#fff;text-align:center;">Records belongs to {{$prisoner->full_name}}</h2>
<hr class="style-one">
<div class="flex-container">
<a target="_blank" href="{{route('hukum',['id' => $prisoner->id])}}"><button class="waheedh info-button" type="button" name="button">Offences</button></a>
</div>






































</div>
@endsection
