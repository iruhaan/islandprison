@extends('layouts.myapp')
@section('content')

<div class="container">

<h2 style="text-align:center;"class="waheedh">އައު ޤައިދީ / ބަންދުމީހެއް އުޅުން</h2>
<hr class="style-one">

<form method="post" action="{{route('saveprisoner')}}">
  {{CSRF_field()}}
<div class="row">
  <div class="col-md-3">
    <label class="lbl">ފުރިހަމަ ނަން | އިނގިރޭސިން</label>
    <input type="text" name="full_name_eng" value="" style="text-align:center;" required>
  </div>
  <div class="col-md-4">
    <label class="lbl">ފުރިހަމަ ނަން | ދިވެހިން</label>
    <input type="text" name="full_name_dhi" value="" class="faruma thaanaKeyboardInput" required>
  </div>
  <div class="col-md-3">
    <label class="lbl">އައިޑީ ކާޑް | ޕާސްޕޯޓް ނަންބަރ</label>
    <input type="text" name="id_passportNo" value="" style="text-align:center;" required>
  </div>
  <div class="col-md-2">
    <label class="lbl">ފައިލް ނަންބަރ</label>
    <input type="text" name="file_number" value="" style="text-align:center;" required>
  </div>
</div>

<input type="hidden" name="profile_path" value="unknown.png">

<div class="row">
  <div class="col-md-3">
    <label class="lbl" id="lbl_address_eng">އެޑްރެސް | އިނގިރޭސިން</label>
    <input type="text" name="address_eng" id="address_eng" disabled style="text-align:center;">
  </div>

  <div class="col-md-3">
    <label class="lbl">އެޑްރެސް | ދިވެހިން</label>
    <input type="text" name="address_dhi" value="" class="faruma thaanaKeyboardInput" id="address" disabled>
  </div>

  <div class="col-md-2">
    <label class="lbl" id="lbl_island">ރަށް</label>
    <select class="faruma" name="island_id" id="island" disabled>
      <option value="" disabled selected>ނަންގަވާ</option>
      @foreach($islands as $island)
        <option value="{{$island->id}}">{{$island->island_name_dhi}}</option>
      @endforeach
    </select>
  </div>
  <div class="col-md-2">
    <label class="lbl" id="lbl_atoll">އަތޮޅު</label>
    <select class="faruma" name="atoll_id" id="atoll" disabled>
      <option value="" disabled selected>ނަންގަވާ</option >
      @foreach($atolls as $atoll)
        <option value="{{$atoll->id}}">{{$atoll->atoll_name_dhi}}</option>
      @endforeach
    </select>
  </div>
  <div class="col-md-2">
    <label class="lbl">ޤައުމު</label>
    <select class="faruma" name="country_id" id="country" required>
      <option value="" disabled selected>ނަންގަވާ</option>
      @foreach($countries as $country)
        <option value="{{$country->id}}">{{$country->country_name_dhi}}</option>
      @endforeach
    </select>
  </div>
</div>



<div class="row">

 <div class="col-md-3">
   <label class="lbl">ނިސްބަތްވާ ގޭންގް ގްރޫޕް</label>
 <select class="faruma" name="gang_name_id" required>
   <option value="" disabled selected>ނަންގަވާ</option>
   @foreach($gang_names as $gang_name)
     <option value="{{$gang_name->id}}">{{$gang_name->gang_name}}</option>
   @endforeach
 </select>
 </div>
 <div class="col-md-2">
     <label class="lbl">ޖިންސް</label>
   <select class="faruma" name="gender_id"required>
     <option value="" disabled selected>ޖިންސް ނަންގަވާ</option>
     @foreach($genders as $gender)
       <option value="{{$gender->id}}">{{$gender->gender_dhi}}</option>
     @endforeach
   </select>
 </div>
  <div class="col-md-2">
      <label class="lbl">ލޭގެ ގްރޫޕް</label>
        <select class="faruma" name="blood_group_id" required>
          <option value=""disabled selected>ނަންގަވާ</option >
          @foreach($bloods as $blood)
            <option value="{{$blood->id}}">{{$blood->blood_group}}</option>
          @endforeach
        </select>
  </div>


  <div class="col-md-2">
    <label class="lbl">ބަންދުގެ ބާވަތް</label>
      <select class="faruma" name="prisoner_type_id" required>
        <option value=""disabled selected>ނަންގަވާ</option >
        @foreach($prisoner_types as $prisoner_type)
          <option value="{{$prisoner_type->id}}">{{$prisoner_type->prisoner_type_dhi}}</option>
        @endforeach
    </select>
  </div>

  <div class="col-md-3">
    <label class="lbl" for="DOB">އުފަން ތަރީޚް</label>
    <input type="date" name="DOB" style="text-align:center;" autocomplete="off" id="dates">
  </div>
</div>


<div class="row">
  <div class="col-md-3">
    <label class="lbl" style="color:transparent;">save</label>
    <input type="submit" name="" value="ރައްކާކުރުމަށް" class="waheedh" style="border-radius:50px;font-size:19px;color:#fff;background:#1D8348;width:100%;height:50px;float:bottom;">
  </div>
  <div class="col-md-3">
    <label class="lbl" style="color:transparent;">save</label>
    <a href="{{route('offender')}}"><button type="button" name="" value="ކެންސަލް" class="waheedh" style="border-radius:50px;font-size:19px;color:#fff;background:#CB4335;width:100%;height:50px;float:bottom;">ކެންސަލް</button></a>

  </div>
  <div class="col-md-1">

  </div>
  <div class="col-md-5">
    <label class="lbl">ވަނަން</label>
    <input type="text" name="nick_name" value="" style="text-align:right;" class="faruma thaanaKeyboardInput">
  </div>
</div>
<form>

<!-- enable and disable dropdown -->
<script type="text/javascript">
    $(function () {
        $("#country").change(function () {
          //alert($(this).val());
            if ($(this).val() == 1) {
                $("#atoll").removeAttr("disabled");
                $("#island").removeAttr("disabled");
                $("#address").removeAttr("disabled");
                $("#address_eng").removeAttr("disabled");


            } else {
                $("#atoll").attr("disabled", "disabled");
                $("#island").attr("disabled", "disabled");
                $("#address").attr("disabled", "disabled");
                $("#address_eng").attr("disabled", "disabled");
            }
        });
    });
</script>

<script>
    $(function() {
        $('select[id=atoll]').change(function() {
            var url = '/islandList/' + $(this).val();
            $.get(url, function(data) {
                var select = $('select[id= island]');
                $('select[id="island"]').empty();

                        $('select[id="island"]').append(
                                    '<option value="" disabled selected> ރަށް ނަންގަވާ </option>'
                                    )
                $.each(data,function(key, value) {
                    select.append('<option value=' + value.id + '>' + value.island_name_dhi + '</option>');
                });
            });
        });
    });
</script>


</div>
@endsection
