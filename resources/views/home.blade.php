@extends('layouts.myapp')

<head>
<!-- //welcome page styles and resorces -->
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
<link rel="stylesheet" href="/home/materialize/css/materialize.min.css" media="screen" />
<!-- FontAwesome Styles-->
<link href="/home/css/font-awesome.css" rel="stylesheet" />
<!-- Custom Styles-->
<link href="/home/css/custom-styles.css" rel="stylesheet" />



</head>

<style media="screen">

#graph-notrevised {
    font-size:15px;
    overflow:hidden;
    text-align:center;
    color:#fff;
    width: 546px;
}

#graph-notrevised,
#graph-notrevised .graph-section {
height: 250px;

}

#graph-notrevised .graph-section {
    display:inline-block;
    cursor:default;
    padding:0 2px;
    border:solid;

}

#graph-notrevised .graph-bar {
    margin:0 2px 0 2px;
    background:rgb(46, 204, 113);
    -webkit-box-shadow: 10px 10px 12px -8px rgba(0,0,0,1);
    -moz-box-shadow: 10px 10px 12px -8px rgba(0,0,0,1);
    box-shadow: 10px 10px 12px -8px rgba(0,0,0,1);
    border:none;
    width:30px;
    height:5px;
}

#graph-notrevised .graph-bar-salaam {
    margin:0 2px 0 2px;
    background:#1ABC9C;
    -webkit-box-shadow: 10px 10px 12px -8px rgba(0,0,0,1);
    -moz-box-shadow: 10px 10px 12px -8px rgba(0,0,0,1);
    box-shadow: 10px 10px 12px -8px rgba(0,0,0,1);
    border:none;
    width:30px;
    height:5px;
}

#graph-notrevised .graph-caption {
    margin-bottom:0px;
}





.cta {
  overflow: hidden;
  position: relative;
  display: inline-block;
  vertical-align: middle;
  margin-top: 22px;
  margin-left: 35px;
  border-radius: 18px;
  text-decoration: none;
  font-size: 20px;
  line-height: 1.42861;
  font-weight: 400;
  letter-spacing: -.016em;
  font-family: "Helvetica", sans-serif;
  line-height: 34px;
  color: #fff;
  transform: translateZ(0);

}

.cta:before {
  content: "";
  position: absolute;
  top: 50%;
  left: 50%;
  width: 100%;
  padding-top: 100%;
  background: radial-gradient(circle at 25% 30%, #f1c909, transparent 40%),
              radial-gradient(circle at 90% 10%, #1bc9e1, transparent 50%),
              radial-gradient(circle at 70% 60%, #e6327f, transparent 30%),
              linear-gradient(45deg, #a44129, #b61264, #712ead, #0fabbd);
  transform: translateX(-50%) translateY(-50%) rotate(0deg) scale(1.05);
  animation: background-animation 5s infinite linear;
}

@keyframes background-animation {
  0% {
    transform: translateX(-50%) translateY(-50%) rotate(0deg) scale(1.05)
  }

  100% {
    transform: translateX(-50%) translateY(-50%) rotate(360deg) scale(1.05)
  }
}

.cta:focus {
  box-shadow: 0 0 0 3px rgba(131,192,253,0.5);
  outline: none
}

.cta:hover {
  line-height: 32px;
  color:#2ECC71;
}

.cta:hover span {
  margin: 2px;
  padding: 0 1.1em;
}

.cta span {
  display: inline-block;
  position: relative;
  z-index: 2;
  margin: 1px;
  padding: 0 calc( 1.1em + 1px);
  border-radius: 18px;
  /* you can delete this line below to see more */
  background: transparent;
}
</style>
@section('content')
<div class="container">

  <h2 style="text-align:center;"class="">Prisoners Registry</h2>
  <hr class="style-one">


  <div class="row">
    <div class="col-md-3">
      <input  class="search-table"type="text" name="" value="" placeholder="Search . . .">
    </div>
    <div class="col-md-3">
    </div>
    <div class="col-md-3">
    </div>
  <div class="col-md-3">
  <a href="{{route('add.prisoner')}}"><button class="" style="float:right;border-radius:50px;height:40px; width:180px; font-size:18px; color:#000; background-color:#04B7B9">
  Add New Prisoner
  </button>
  </a>
  </div>
  </div>

  <div id="prisoner_registry" style="overflow-y: scroll; height: 400px;">
    <table class="">
      <thead>
        <tr>
          <th style="text-align:center;">File Number</th>
          <th style="text-align:left;">Full Name</th>
          <th style="text-align:left;">Address</th>
          <th style="text-align:left;">Security Level</th>
          <th style="text-align:center;">Country</th>
          <th style="text-align:center;">NID | PNo</th>
          <th style="text-align:center;">Status</th>
          <th style="text-align:center;">Photo</th>
        </tr>
      </thead>
        <tbody>
          @foreach($prisoners as $prisoner)
          <tr>
            <td id="file_number" class="@if($prisoner->status->id == 1) status-active @elseif($prisoner->status->id == 2 || $prisoner->status->id == 3) status-geybandhu @else status-inactive @endif" style="text-align:center;">
              <a style="color:#fff;" target="_blank" href="{{route('prisoner.info',['id'=>$prisoner->id])}}">{{$prisoner->file_number}}</a>
            </td>
            <td style="text-align:left;" class="faruma @if($prisoner->status->id == 1) active @elseif($prisoner->status->id == 2 || $prisoner->status->id == 3) geybandhu @else inactive @endif">
              {{$prisoner->full_name}}
            </td>
            <td style="text-align:left;" class="faruma @if($prisoner->status->id == 1) active @elseif($prisoner->status->id == 2 || $prisoner->status->id == 3) geybandhu @else inactive @endif">
              {{$prisoner->address}} , {{$prisoner->atoll->atoll_name}} . {{$prisoner->island->island_name}}
            </td>
            <td style="text-align:center;" class="faruma @if($prisoner->status->id == 1) active @elseif($prisoner->status->id == 2 || $prisoner->status->id == 3) geybandhu @else inactive @endif">
              {{$prisoner->security_level->security_level}}
            </td>
            <td style="text-align:center;" class="faruma @if($prisoner->status->id == 1) active @elseif($prisoner->status->id == 2 || $prisoner->status->id == 3) geybandhu @else inactive @endif">
              {{$prisoner->country->country_name}}
            </td>
            <td style="text-align:center;" class="@if($prisoner->status->id == 1) active @elseif($prisoner->status->id == 2 || $prisoner->status->id == 3) geybandhu @else inactive @endif">
              {{$prisoner->id_passportNo}}
            </td>
            <td id="status_code" style="text-align:center;" class="faruma @if($prisoner->status->id == 1) active @elseif($prisoner->status->id == 2 || $prisoner->status->id == 3) geybandhu @else inactive @endif">
              {{$prisoner->status->status}}
            </td>
            <td style="text-align:center;"><a class="trigger"><img class="list-profile-photo" src="{{asset('storage/prisoner/'.$prisoner->profile_path)}}"></a></td>
            <div id="pop-up">
              <img class="pop-ups" src="{{asset('storage/prisoner/'.$prisoner->profile_path)}}">
            </div>
          </tr>
          @endforeach
        </tbody>
    </table>
  </div>


  <a class="cta" target="_blank" href="{{route('information')}}">
    <span class="waheedh">އޮފެންޑާރ ވޭރިއަބަލް<i class="fa fa-book" aria-hidden="true"></i></span>
  </a>

<br>


<script type="text/javascript">
$(function() {
var moveRight = 10;
var moveDown = 10;

$('a.trigger').hover(function(e) {
  $('div#pop-up').show();
  //.css('top', e.pageY + moveDown)
  //.css('left', e.pageX + moveLeft)
  //.appendTo('body');
}, function() {
  $('div#pop-up').hide();
});

$('a.trigger').mousemove(function(e) {
  $("div#pop-up").css('top', e.pageY + moveDown).css('left', e.pageX + moveLeft);
});

});
</script>


<script>
$(document).on("keyup",".search-table", function () {
            var value = $(this).val();
            $("table tr").each(function (index) {
                $row = $(this);
                $row.show();
                if (index !== 0 && value) {
                    var found = false;
                    $row.find("td").each(function () {
                        var cell = $(this).text();
                        if (cell.indexOf(value.toLowerCase()) >= 0) {
                            found = true;
                            return;
                        }
                    });
                    if (found === true) {
                        $row.show();
                    }
                    else {
                        $row.hide();
                    }
                }
      });
});
</script>

</div>
@endsection
